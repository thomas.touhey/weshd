/* ****************************************************************************
 * m2m.c -- gestion du protocole M2M over TCP des serveurs WES.
 * Copyright (C) 2018 Thomas Touhey <thomas@touhey.fr>
 *
 * This project is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the
 * CeCILL license as circulated by CEA, CNRS and INRIA at the
 * following URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the project's author, the holder of the
 * economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also therefore
 * means that it is reserved for developers and experienced professionals
 * having in-depth computer knowledge. Users are therefore encouraged to load
 * and test the software's suitability as regards their requirements in
 * conditions enabling the security of their systems and/or data to be
 * ensured and, more generally, to use and operate it in the same conditions
 * as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 * ************************************************************************* */
#include "internals.h"
#define ms * 1000

/* ---
 * Utilitaires.
 * --- */

/* `get_proto_port()`: Récupérer le protocole et le port à utiliser. */

static int get_proto_port(WES *wes, int *proto, int *port)
{
	int err;
	char tcp_act[5], udp_act[5];
	char tcp_port[10], udp_port[10];
	wes_cgi_t blocks[] = {
		/* Selon si l'interface M2M via TCP est activée ou non.
		 * Vaut "c" (pour "checked") si c'est le cas, "" sinon. */
		WESCGI("Mta", "%.1s", tcp_act, 5),

		/* Le port utilisé par l'interface M2M via TCP. */
		WESCGI("Mtp", "%d", tcp_port, 10),

		/* Selon si l'interface M2M via UDP est activée ou non.
		 * Vaut "c" (pour "checked") si c'est le cas, "" sinon. */
		WESCGI("Mua", "%.1s", udp_act, 5),

		/* Le port utilisé par l'interface M2M via UDP. */
		WESCGI("Mup", "%d", udp_port, 10)
	};

	if ((err = wes_get_cgi(wes, 4, blocks)))
		return (err);

	if (blocks[0].buffer[0] == 'c' && blocks[1].lines) {
		*proto = WSK_TCP;
		*port = (int)(atoi(blocks[1].buffer) & 0x7fff);
	} else if (blocks[2].buffer[0] == 'c' && blocks[3].lines) {
		*proto = WSK_UDP;
		*port = (int)(atoi(blocks[3].buffer) & 0x7fff);
	} else
		return (wes_error_op);

	return (wes_error_ok);
}

/* ---
 * Communication en TCP sur systèmes conformes selon POSIX/SUS.
 * --- */

/* `connect_tcp()`: Lancer la connexion en TCP.
 * Définie à part car utilisée dans l'initialisation *et* dans l'envoi
 * de commande en cas de problème. */

static int connect_tcp(WES *wes)
{
	int ret;

	if (wes->sock_type != WSK_TCP)
		return (wes_error_ok);

	/* Il s'agit d'une connexion TCP, on s'occupe donc de la
	 * connecter au serveur. */

	ret = connect(wes->platform.posix.sock, wes->platform.posix.sockaddr,
		wes->platform.posix.sockaddr_size);
	if (ret < 0) switch (errno) {
		case EADDRNOTAVAIL:
		case ENETUNREACH:
		case EADDRINUSE:
		case ETIMEDOUT:
			return (wes_error_nohost);

		default:
			return (wes_error_unknown);
	}

	/* Si on a besoin de définir `TCP_NODELAY` pour une raison ou pour
	 * une autre, c'est ici. */

	return (wes_error_ok);
}

/* `connect_wes()`: Se connecter via TCP ou UDP (selon ce qui est ouvert ou
 * ce qu'on doit ouvrir) au WES sur le port utilisé pour le protocole M2M,
 * ou réutiliser le socket déjà ouvert. */

static int connect_wes(WES *wes)
{
	char ip_as_string[50];
	const unsigned char *ip;
	struct sockaddr_in *sockaddr4;
	struct sockaddr_in6 *sockaddr6;
	int sock = -1, protocol, err;
	int af, intport; in_port_t port;

	/* On voit si la socket est déjà ouverte, et si c'est le cas, on
	 * renvoie cette socket. */

	if (wes->sock_type != WSK_NONE)
		return (wes_error_ok);

	/* On vérifie si l'interface est allumée via TCP ou UDP et on récupère
	 * le port associé, le tout via l'interface CGI. */

	if ((err = get_proto_port(wes, &protocol, &intport)))
		return (err);
	port = (in_port_t)intport;

	/* On prépare l'adresse de la socket. */

	if (wes->addr.data.inet.has & WESHASIPv4) {
		ip = wes->addr.data.inet.ipv4;
		sprintf(ip_as_string, "%d.%d.%d.%d", ip[0], ip[1], ip[2], ip[3]);

		sockaddr4 = &wes->platform.posix.sock_addr.ip;
		wes->platform.posix.sockaddr = (struct sockaddr *)sockaddr4;
		wes->platform.posix.sockaddr_size = sizeof(*sockaddr4);

		sockaddr4->sin_family = AF_INET;
		inet_pton(AF_INET, ip_as_string, &sockaddr4->sin_addr);
		sockaddr4->sin_port = htons(port);
	} else if (wes->addr.data.inet.has & WESHASIPv6) {
		ip = wes->addr.data.inet.ipv6;
		sprintf(ip_as_string, "%02X%02X:%02X%02X:%02X%02X:%02X%02X"
			":%02X%02X:%02X%02X:%02X%02X:%02X%02X",
			ip[0], ip[1], ip[2], ip[3], ip[4], ip[5], ip[6], ip[7],
			ip[8], ip[9], ip[10], ip[11], ip[12], ip[13], ip[14], ip[15]);

		sockaddr6 = &wes->platform.posix.sock_addr.ip6;
		wes->platform.posix.sockaddr = (struct sockaddr *)sockaddr6;
		wes->platform.posix.sockaddr_size = sizeof(*sockaddr6);

		sockaddr6->sin6_family = AF_INET6;
		inet_pton(AF_INET6, ip_as_string, &sockaddr6->sin6_addr);
		sockaddr6->sin6_port = htons(port);
	} else
		return (wes_error_unknown);

	/* On prépare le buffer avant la création du socket et la connexion
	 * (opérations les plus lourdes) pour que tout soit lu correctement.
	 *
	 * Ce buffer doit faire suffisamment pour récupérer l'entièreté
	 * des données dans une frame TCP ou un paquet UDP. */

	if (!(wes->sock_buf = malloc(4096)))
		return (wes_error_alloc);
	wes->sock_buflen = 4096;

	/* On crée la socket avec la bonne famille. */

	af = wes->platform.posix.sockaddr->sa_family;
	if (protocol == WSK_TCP)
		sock = socket(af, SOCK_STREAM, 0);
	else
		sock = socket(af, SOCK_DGRAM, 0);

	if (sock < 0) {
		free(wes->sock_buf);
		wes->sock_buf = NULL;

		switch (errno) {
		case EMFILE: /* FALLTHRU */
		case ENFILE: /* FALLTHRU */
		case ENOMEM:
			return (wes_error_alloc);

		default:
			return (wes_error_unknown);
		}
	}

	wes->sock_type = protocol;
	wes->platform.posix.sock = sock;

	if (protocol == WSK_TCP) {
		/* S'il s'agit de TCP, on connecte dès maintenant la socket
		 * avec la fonction `connect_tcp()` qui définit des options
		 * si nécessaire. */

		if ((err = connect_tcp(wes))) {
			close(sock);
			wes->sock_type = WSK_NONE;
			wes->platform.posix.sock = -1;

			free(wes->sock_buf);
			wes->sock_buf = NULL;

			return (err);
		}
	} else if (protocol == WSK_UDP) {
		/* S'il s'agit d'UDP, on définit l'adresse par défaut d'envoi et
		 * de réception, histoire de pouvoir utiliser les fonctions `recv()`
		 * et `send()` plutôt que leurs contreparties `recvfrom()` et
		 * `sendto()`, et `select()`/`ioctl(FIONREAD)`. */

		if (connect(sock, wes->platform.posix.sockaddr,
			wes->platform.posix.sockaddr_size) < 0) {
			/* Bon, je ne vois pas pourquoi ça échouerait ici puisqu'il
			 * n'y a pas de paquets échangés (l'UDP n'est pas connecté),
			 * mais sait-on jamais : une erreur interne est si vite arrivée. */

			close(sock);
			wes->sock_type = WSK_NONE;
			wes->platform.posix.sock = -1;

			free(wes->sock_buf);
			wes->sock_buf = NULL;

			return (wes_error_unknown);
		}
	}

	/* Notre socket est prête à être utilisée par les fonctions
	 * `send_*_command()` ! */

	return (wes_error_ok);
}

/* `receive_buffer()`: Recevoir un buffer. */

static int receive_buffer(int sock, char *buf, size_t size, size_t *received)
{
	fd_set rfds;
	struct timeval tv;
	int ret, intcount;
	ssize_t count;

	/* On attend une réponse de la part du serveur WES dans les
	 * 100 millisecondes. */

	FD_ZERO(&rfds);
	FD_SET(sock, &rfds);
	tv.tv_sec = 0;
	tv.tv_usec = 100 ms;

	if ((ret = select(sock + 1, &rfds, NULL, NULL, &tv)) < 0) switch (errno) {
		case ECONNRESET:
			return (wes_error_conn);

		default:
			return (wes_error_unknown);
	} else if (!ret)
		return (wes_error_timeout);

	/* On sait qu'on a une réponse, maintenant, on veut savoir
	 * de combien d'octets est composée cette réponse. */

	if (ioctl(sock, FIONREAD, &intcount) < 0) {
		/* FIXME: si cet `ioctl()` échoue, que fait-on ? */

		return (wes_error_unknown);
	}
	count = (ssize_t)intcount;

	/* Si cette réponse fait plus que la taille de notre buffer,
	 * tant pis, même si on a essayé de ne pas faire ça, on perdra
	 * possiblement de l'information. */

	if (count > (ssize_t)size)
		count = (ssize_t)size;

	/* On sait de combien est cette réponse, on va pouvoir
	 * lire son contenu à présent. */

	*received = (size_t)count;
	if (recv(sock, buf, (size_t)count, 0) < count) {
		/* FIXME: check ERRNO? */

		return (wes_error_unknown);
	}

	return (wes_error_ok);
}

/* `send_tcp_command()`: Envoi d'une commande via TCP, et réception de la
 * commande par le même moyen. */

static int send_tcp_command(WES *wes, const char *command, size_t clen,
	char *response, size_t rlen, size_t *fullp)
{
	int sock, err, tries;
	int newline = 0, car;
	size_t full = 0, count;
	ssize_t ssret;
	char *buf;

	sock = wes->platform.posix.sock;

	/* On prépare le buffer de réponse. */

	*response = '\0';
	rlen--;

	/* On vide tout ce qui est déjà arrivé dans la socket :
	 * ça compte pour du beurre tant qu'on n'a pas envoyé la commande. */

	while (1) {
		err = receive_buffer(sock, wes->sock_buf,
			wes->sock_buflen, &count);

		if (err == wes_error_conn) {
			if ((err = connect_tcp(wes)))
				return (err);
			break;
		}
		if (err == wes_error_ok)
			continue;
		if (err != wes_error_timeout)
			return (err);
	}

	/* Toute la fonction est dans la boucle :
	 * Si on a un problème réparable, on peut recommencer au début en
	 * envoyant la commande. */

	while (1) {
		/* Tout d'abord, on envoie la commande.
		 * En cas de reset de connexion, on relance la connexion avec
		 * `connect_tcp()` (qui utilise `connect()`).
		 * Si la connexion a réussi trois fois mais que l'envoi a échoué
		 * trois fois, autant abandonner : on n'arrivera pas à se connecter
		 * correctement. */

		for (tries = 3; tries; tries--) {
			ssret = send(sock, command, clen, 0);

			/* Si `ret < 0`, il y a eu une erreur.
			 * Si `ret < clen`, tous les octets n'ont pas été envoyés.
			 * L'ensemble fait qu'une erreur au moins s'est produite sur
			 * la ligne. */

			if ((size_t)ssret == clen)
				break;

			switch (errno) {
			case ECONNRESET:
				/* La connexion a été réinitialisée, on tente de se
				 * re-connecter. Si la re-connexion réussit, on retente
				 * alors d'envoyer le message. */

				if ((err = connect_tcp(wes)))
					return (err);
				break;

			default:
				/* On ne connaît pas l'erreur ici. */

				return (wes_error_unknown);
			}
		}

		/* Ensuite, on cherche à récupérer la réponse.
		 * Pour cela, on récupère les paquets jusqu'au prochain retour à la
		 * ligne. */

		while (1) {
			/* On reçoit le buffer. */

			err = receive_buffer(sock, wes->sock_buf,
				wes->sock_buflen, &count);
			if (err == wes_error_conn) {
				if ((err = connect_tcp(wes)))
					return (err);
				break;
			}

			switch (err) {
			case wes_error_ok:
				break;
			default:
				/* TODO: traiter l'erreur correctement */
				return (wes_error_unknown);
			}

			/* On traite le buffer. */

			for (buf = wes->sock_buf; count; count--) {
				car = *buf++;

				/* Si le caractère courant est un retour à la ligne, on
				 * break et on affecte 1 à `newline`. */

				if (car == '\n') {
					newline = 1;
					break;
				}

				/* Autrement, si ce n'est pas un caractère qu'on ignore,
				 * on l'ajoute si possible au buffer de l'utilisateur. */

				if (car == '\r')
					continue;

				full++;
				if (rlen) {
					*response++ = (char)car;
					*response = '\0';
					rlen--;
				}
			}

			if (newline)
				break;
		}

		/* Si `newline` n'a pas été défini à 1, cela veut dire qu'on n'a
		 * pas rencontré de newline, juste une erreur : on recommence donc. */

		if (!newline)
			continue;

		/* Notre buffer a bien été rempli, on peut quitter ! */

		break;
	}

	*fullp = full;
	return (wes_error_ok);
}

/* `send_udp_command()`: Envoi d'une commande via UDP, et réception de
 * la commande par le même moyen. */

static int send_udp_command(WES *wes, const char *command, size_t clen,
	char *response, size_t rlen, size_t *fullp)
{
	int sock, err, newline = 0, car;
	size_t full = 0, count;
	char *buf;
	ssize_t ssret;

	sock = wes->platform.posix.sock;

	/* On prépare le buffer de réponse. */

	*response = '\0';
	rlen--;

	/* On vide tout ce qui est déjà arrivé dans la socket :
	 * ça compte pour du beurre tant qu'on n'a pas envoyé la commande. */

	while (1) {
		err = receive_buffer(sock, wes->sock_buf,
			wes->sock_buflen, &count);

		if (err == wes_error_conn) {
			if ((err = connect_tcp(wes)))
				return (err);
			break;
		}
		if (err == wes_error_ok)
			continue;
		if (err != wes_error_timeout)
			return (err);
	}

	/* On envoie la commande.
	 * Si `ret < 0`, il y a eu une erreur.
	 * Si `ret < clen`, tous les octets n'ont pas été envoyés.
	 * L'ensemble fait qu'une erreur au moins s'est produite sur
	 * la ligne. */

	ssret = send(wes->platform.posix.sock, command, clen, 0);
	if ((size_t)ssret < clen) switch (errno) {
		default:
			/* On ne connaît pas l'erreur ici. */

			return (wes_error_unknown);
	}

	/* Ensuite, on cherche à récupérer la réponse.
	 * Pour cela, on récupère les paquets jusqu'au prochain retour à la
	 * ligne. */

	while (1) {
		/* On reçoit le buffer. */

		err = receive_buffer(sock, wes->sock_buf,
			wes->sock_buflen, &count);
		switch (err) {
		case wes_error_ok:
			break;
		default:
			/* TODO: traiter l'erreur correctement */
			return (wes_error_unknown);
		}

		/* On traite le buffer. */

		for (buf = wes->sock_buf; count; count--) {
			car = *buf++;

			/* Si le caractère courant est un retour à la ligne, on
			 * break et on affecte 1 à `newline`. */

			if (car == '\n') {
				newline = 1;
				break;
			}

			/* Autrement, si ce n'est pas un caractère qu'on ignore,
			 * on l'ajoute si possible au buffer de l'utilisateur. */

			if (car == '\r')
				continue;

			full++;
			if (rlen) {
				*response++ = (char)car;
				*response = '\0';
				rlen--;
			}
		}

		if (newline)
			break;
	}

	*fullp = full;
	return (wes_error_ok);
}

/* ---
 * Interface utilisateur.
 * --- */

/* `wes_init_m2m()`: Initialiser l'aspect M2M d'un serveur. */

int wes_init_m2m(WES *wes)
{
	wes->sock_type = WSK_NONE;
	wes->platform.posix.sock = -1;

	wes->sock_buf = NULL;

	return (wes_error_ok);
}

/* `wes_deinit_m2m()`: Déinitialiser l'aspect M2M d'un serveur. */

int wes_deinit_m2m(WES *wes)
{
	if (wes->platform.posix.sock >= 0) {
		close(wes->platform.posix.sock);
		wes->sock_type = WSK_NONE;
		wes->platform.posix.sock = -1;
	}

	if (wes->sock_buf) {
		free(wes->sock_buf);
		wes->sock_buf = NULL;
	}

	wes->sock_type = WSK_NONE;
	return (wes_error_ok);
}

/* `wes_send_command()`: Envoyer une commande via le protocole M2M, et
 * recevoir la réponse. */

int wes_send_command(WES *wes, const char *command, char *response,
	size_t *lenp)
{
	size_t commandlen;
	char shortcommand[256], *p;
	int err;

	p = memchr(command, '\0', 254);
	commandlen = p ? (size_t)(p - command) : 254;
	memcpy(shortcommand, command, commandlen);
	shortcommand[commandlen] = '\r';
	shortcommand[commandlen + 1] = '\n';
	commandlen += 2;

	/* On ouvre le socket s'il n'a pas été ouvert. */

	if ((err = connect_wes(wes)))
		return (err);

	/* On utilise l'envoi avec le protocole de transport approprié.
	 * On s'est occupés de traiter la commande et d'ajouter un newline
	 * dedans nous-mêmes, histoire que ça n'aie pas à être fait dans
	 * la sous-commande. */

	switch (wes->sock_type) {
	case WSK_TCP:
		return (send_tcp_command(wes, shortcommand, commandlen,
			response, *lenp, lenp));
	case WSK_UDP:
		return (send_udp_command(wes, shortcommand, commandlen,
			response, *lenp, lenp));
	}

	return (wes_error_unknown);
}
