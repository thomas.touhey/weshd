/* ****************************************************************************
 * internals.h -- mécanismes internes de la libwes.
 * Copyright (C) 2018 Thomas Touhey <thomas@touhey.fr>
 *
 * This project is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the
 * CeCILL license as circulated by CEA, CNRS and INRIA at the
 * following URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the project's author, the holder of the
 * economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also therefore
 * means that it is reserved for developers and experienced professionals
 * having in-depth computer knowledge. Users are therefore encouraged to load
 * and test the software's suitability as regards their requirements in
 * conditions enabling the security of their systems and/or data to be
 * ensured and, more generally, to use and operate it in the same conditions
 * as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 * ************************************************************************* */
#ifndef  LOCAL_INTERNALS_H
# define LOCAL_INTERNALS_H 20180611
# include <stdlib.h>
# include <string.h>
# include <errno.h>
# include <libwes.h>
# include <curl/curl.h>

/* En-têtes spécifiques POSIX. */

# include <unistd.h>
# include <arpa/inet.h>
# include <netinet/in.h>
# include <netinet/tcp.h>
# include <sys/socket.h>
# include <sys/ioctl.h>

/* ---
 * Logging.
 * --- */

/* Obtention du nom d'une fonction. */

# if defined(__STDC_VERSION__) && __STDC_VERSION__ >= 199901L
#  define LOGFUNC __func__
# elif !defined(__STRICT_ANSI__) && GNUC_PREREQ(2, 0)
#  define LOGFUNC __FUNCTION__
# else
#  define LOGFUNC NULL
# endif

/* Non seulement les versions en minuscules sont plus faciles à taper, mais
 * elles sont faites pour être utilisées avec les macros `msg` et `mem`.
 *
 * (et parce que les majuscules c'est un peu lourd à taper, je fais aussi
 * la version en minuscules. en fait depuis ça a pris un rôle différent,
 * mais bon) */

# define wldebug   0, LOGFUNC
# define wlinfo    0, LOGFUNC
# define wlwarn   10, LOGFUNC
# define wlerror  20, LOGFUNC
# define wlfatal  30, LOGFUNC
# define wlnotice 40, LOGFUNC

/* Fonctions principales.
 * Elles sont cachées derrière les macros en-dessous. */

extern void wes_log_msg(wes_loglevel_t loglevel, const char *func,
	const char *format, ...);
extern void wes_log_mem(wes_loglevel_t loglevel, const char *func,
	const void *m, size_t n);

/* Macros principales, qui redirigent vers les fonctions de log.
 * Utilisez les doubles parenthèses de la façon suivante :
 *
 *	msg((wldebug, "ma %schaîne de formattage %d", "merveilleuse ", 1));
 *	mem((wlwarn, ma_zone_de_memoire, la_taille_de_ma_zone_de_memoire));
 *
 * Il s'agit de macros variadiques, mais compatibles K&R/ANSI C. */

# define msg(ARGS) wes_log_msg ARGS
# define mem(ARGS) wes_log_mem ARGS

/* ---
 * Déclaration de la structure WES.
 * --- */

# define WSK_NONE 0
# define WSK_TCP  1
# define WSK_UDP  2

struct WES {
	/* Identifiants pour les serveurs HTTP et FTP. */

	char *http_name;
	char *http_pass;

	char  *ftp_name;
	char  *ftp_pass;

	/* Canaux (handles de la libcurl) pour le HTTP et FTP. */

	CURL *http_handle;
	CURL  *ftp_handle;

#if 0
	/* Éléments pour la gestion de la configuration.
	 * Il s'agit d'une liste chaînée de sections. */

	wes_cfg_sec_t *sections;
#endif

	/* Éléments pour la gestion des données.
	 * Il s'agit d'une liste chaînée de données regroupées par date. */

	wes_data_span_t *data;

	/* Adresse IP cross-platform pour joindre le serveur WES. */

	wes_addr_t addr;

	/* Éléments pour le protocole M2M.
	 * `sock_type` représente le type de la socket parmi les constantes
	 * en `WSK_*` précisées ci-dessus.
	 * `sock_buf` est le buffer de réception, alloué dynamiquement à
	 * `WSBUFSIZE` lorsque l'on se sert de la socket.
	 * La socket et ses données, qui dépendent de la plateforme, sont
	 * stockées dans l'union `platform`. */

	int sock_type;
	char *sock_buf;
	size_t sock_buflen;

	/* Éléments spécifiques à des platformes données. */

	union {
		struct {
			/* Sockets BSD.
			 * On garde le file descriptor et l'adresse (pour l'UDP et
			 * sa commande associée `sendto()`). */

			int sock;

			int wes_posix__align;

			struct sockaddr *sockaddr;
			socklen_t sockaddr_size;

			union {
				struct sockaddr_in  ip;
				struct sockaddr_in6 ip6;
			} sock_addr;
		} posix;
	} platform;
};

/* ---
 * Décodage de fichiers CSV.
 * --- */

/* Cookie et type du callback.
 * Le callback est appelé pour chaque ligne qui a été lue.
 * Le `fdc` passé (field count) ne donne que le nombre de valeurs remplies,
 * pas le nombre total de champs qu'il y avait dans la ligne. */

typedef int wes_csv_entry_t (void *cookie, int line, int fdc,
	char * const fdv[]);

typedef struct wes_csv {
	int num, id, line, err;
	char * const *buffers;
	size_t const *lens;
	wes_csv_entry_t *callback;
	void *cbcookie;

	size_t off;
	int separator;

	int wes_csv__align;
} wes_csv_t;

/* `wes_init_csv()` permet d'initialiser le cookie avec les buffers, quelques
 * tweaks de lecture et le callback à utiliser. */

extern int wes_init_csv(wes_csv_t *cookie, int separator,
	int numfields, char * const buffers[], size_t const lens[],
	wes_csv_entry_t *callback, void *cbcookie);

/* `wes_read_csv()` sert de callback à la libcurl par exemple (ressemble donc
 * à `fwrite()`), pour lire du contenu ;
 * `wes_end_csv()` termine la lecture du fichier en appelant si besoin une
 * dernière fois le callback. */

extern size_t wes_read_csv(char *buf, size_t size, size_t nmemb, void *cookie);
extern int wes_end_csv(wes_csv_t *cookie);

/* ---
 * Autres utilitaires.
 * --- */

/* Ouverture des canaux sur lesquels les requêtes sont faites. */

extern int wes_open_http_handle(WES *wes, CURL **handlep,
	wes_http_method_t method, char const *path, int argc,
	wes_http_arg_t *args);
extern int wes_open_ftp_handle(WES *wes, CURL **handlep, char const *path);

#if 0 /* INDEV */
/* Récupérer et définir la valeur d'une option de configuration sur le
 * serveur WES. La « section » correspond au fichier dans le dossier `CFG/`
 * accessible sur le serveur FTP du serveur WES. */

extern int wes_get_config(WES *wes, const char *section, const char *key,
	char *buf, size_t len);
extern int wes_set_config(WES *wes, const char *section, const char *key,
	const char *value);

/* Appliquer et retirer l'intégralité de la copie locale de
 * la configuration. */

extern int wes_apply_config(WES *wes);
extern int wes_clear_config(WES *wes);
#endif

#endif /* LOCAL_INTERNALS_H */
