/* ****************************************************************************
 * data.c -- gestion des données par FTP des serveurs WES.
 * Copyright (C) 2018 Thomas Touhey <thomas@touhey.fr>
 *
 * This project is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the
 * CeCILL license as circulated by CEA, CNRS and INRIA at the
 * following URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the project's author, the holder of the
 * economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also therefore
 * means that it is reserved for developers and experienced professionals
 * having in-depth computer knowledge. Users are therefore encouraged to load
 * and test the software's suitability as regards their requirements in
 * conditions enabling the security of their systems and/or data to be
 * ensured and, more generally, to use and operate it in the same conditions
 * as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 * ************************************************************************* */
#include "internals.h"

/* ---
 * Utilitaires.
 * --- */

#define KIND_PCE 0 /* pinces */
#define KIND_PLS 1 /* impulsions */
#define KIND_TLI 2 /* teleinfo */

/* `get_request()`: Préparation d'une requête, avec URL et identifiants.
 * Il y a plusieurs URLs différentes, se ressemblant mais dont le format
 * varie légèrement parfois ; voir `DATA.rst` dans `doc/`.
 *
 * `kind` contient le type de données (pinces, impulsions, teleinfo) à
 * récupérer, sous la forme de l'une des constantes `KIND_*` présentées
 * ci-dessus.
 * `alt` est une valeur d'URL alternative, 0 s'il s'agit de l'ancien format,
 * 1 s'il s'agit du nouveau. */

static int get_request(WES *wes, CURL **handlep, int kind, int alt,
	int year, int mon, int dom)
{
	const char *alts[3][4] = {{
		"/PCE/%04d/PC-%02d-%02d.csv",
		"/PCE/PC%04d-%02d-%02d.csv",
		NULL
	}, {
		"/PLS/%04d/PL-%02d-%02d.csv",
		"/PLS/PL%04d-%02d-%02d.csv",
		"/PLS/PC%04d-%02d-%02d.csv",
		NULL
	}, {
		"/teleinfo/%04d/TI-%02d-%02d.csv",
		"/teleinfo/TI%04d-%02d-%02d.csv",
		NULL
	}};
	char path[50];

	/* Création de l'URL. */

	if (!alts[kind][alt])
		return (wes_error_iter);
	sprintf(path, alts[kind][alt], year, mon, dom);

	return (wes_open_ftp_handle(wes, handlep, path));
}

/* `gethourmin()`: récupérer l'heure et la minute à partir d'un champ
 * de type 'HH:MM'. */

static int gethourmin(const char *from, int *hourp, int *minp)
{
	int hour, min;

	if (!isdigit(from[0]) || !isdigit(from[1]) || from[2] != ':'
	 || !isdigit(from[3]) || !isdigit(from[4]) || from[5] != '\0')
		return (wes_error_unknown);

	hour = (from[0] - '0') * 10 + (from[1] - '0');
	min = (from[3] - '0') * 10 + (from[4] - '0');
	if (hour >= 24 || min >= 60)
		return (wes_error_unknown);

	*hourp = hour;
	*minp = min;
	return (wes_error_ok);
}

/* `getentry()`: Récupération d'une entrée de donnée correspondant à
 * un couple (heures, minutes) de la journée. Si l'entrée n'existe pas,
 * elle est créée et initialisée. */

static int getentry(wes_data_span_t *span, wes_data_t **dataresp,
	int hour, int min)
{
	wes_data_t **datap, *data;

	for (datap = &span->data; (data = *datap); datap = &data->next) {
		if (data->hour > hour || (data->hour == hour && data->min >= min))
			break;
	}
	if (data && data->hour == hour && data->min == min) {
		*dataresp = data;
		return (wes_error_ok);
	}

	data = malloc(sizeof(wes_data_t));
	if (!data)
		return (wes_error_alloc);

	data->next = *datap;
	data->hour = hour;
	data->min = min;
	data->flags = 0;

	*datap = data;
	*dataresp = data;
	return (wes_error_ok);
}

/* ---
 * Callbacks pour récupération des données.
 * --- */

/* `gatherspanpl_add()`: Callback de `getcsv()` pour `gatherspandata()`
 * appliqué aux données d'impulsions. Récupère les buffers et la section
 * et ajoute l'entrée à la section. */

static int gatherspanpl_add(wes_data_span_t *span, int line,
	int fdc, char **fdv)
{
	int err, i;
	wes_data_t *ent;
	int hour, min;

	/* On ignore la première ligne, qui ne contient que les noms des champs. */

	if (line == 0)
		return (wes_error_ok);

	/* On obtient la date et l'heure, et on obtient l'entrée correspondante
	 * dans la liste de données. Si la date et l'heure sont invalides,
	 * on se contente d'ignorer l'entrée courante. */

	if (!fdc || (err = gethourmin(fdv[0], &hour, &min)))
		return (wes_error_ok);

	/* On récupère un pointeur sur l'entrée. */

	if ((err = getentry(span, &ent, hour, min)))
		return (err);

	/* On récupère ensuite les données en elles-mêmes. */

	ent->flags |= WDFPLEXST;

	for (i = 0; i < 4; i++) {
		if (i >= fdc || !strcmp(fdv[1 + i], "nan"))
			ent->flags |= WDFPLNAN0 << i;
		else {
			ent->flags &= ~(WDFPLNAN0 << i);
			sscanf(fdv[1 + i], "%lf", &ent->pl[i]);
		}
	}

	return (wes_error_ok);
}

/* `gatherspanpc_add()`: Callback de `getcsv()` pour `gatherspandata()`
 * appliqué aux données des pinces ampèremétriques. Récupère les buffers
 * et la section et ajoute l'entrée à la section. */

static int gatherspanpc_add(wes_data_span_t *span, int line,
	int fdc, char **fdv)
{
	int err, i;
	wes_data_t *ent;
	int hour, min;

	/* On ignore la première ligne, qui ne contient que les noms des champs. */

	if (line == 0)
		return (wes_error_ok);

	/* On obtient la date et l'heure, et on obtient l'entrée correspondante
	 * dans la liste de données. Si la date et l'heure sont invalides,
	 * on se contente d'ignorer l'entrée courante. */

	if (!fdc || (err = gethourmin(fdv[0], &hour, &min)))
		return (wes_error_ok);

	/* On récupère un pointeur sur l'entrée. */

	if ((err = getentry(span, &ent, hour, min)))
		return (err);

	/* On récupère ensuite les données en elles-mêmes. */

	ent->flags |= WDFPCEXST;

	for (i = 0; i < 4; i++) {
		if (i >= fdc || !strcmp(fdv[1 + i], "nan"))
			ent->flags |= WDFPCNAN0 << i;
		else {
			ent->flags &= ~(WDFPCNAN0 << i);
			sscanf(fdv[1 + i], "%lf", &ent->pc[i]);
		}
	}

	return (wes_error_ok);
}

/* `gatherspanti_add()`: Callback de `getcsv()` pour `gatherspandata()`
 * appliqué aux données de téléinfo. Récupère les buffers et la section
 * et ajoute l'entrée à la section. */

static int gatherspanti_add(wes_data_span_t *span, int line,
	int fdc, char **fdv)
{
	int err;
	wes_data_t *ent;
	int hour, min;

	/* On ignore la première ligne, qui ne contient que les noms des champs. */

	if (line == 0)
		return (wes_error_ok);

	/* On obtient la date et l'heure, et on obtient l'entrée correspondante
	 * dans la liste de données. Si la date et l'heure sont invalides,
	 * on se contente d'ignorer l'entrée courante. */

	if (!fdc || (err = gethourmin(fdv[0], &hour, &min)))
		return (wes_error_ok);

	/* On récupère un pointeur sur l'entrée. */

	if ((err = getentry(span, &ent, hour, min)))
		return (err);

	/* On récupère ensuite les données en elles-mêmes. */

	ent->flags |= WDFTIEXST;

	if (fdc <= 2 || !strcmp(fdv[1], "PAS de teleinfo")) {
		ent->flags &= ~WDFTIPLUG;
		return (wes_error_ok);
	}

	ent->flags |= WDFTIPLUG;

	/* Récupération de l'énergie consommée lors des heures pleines et
	 * des heures creuses, en Wh. */

	if (fdc >= 2)
		sscanf(fdv[1], "%ld", &ent->hp);
	else
		ent->hp = 0;

	if (fdc >= 3)
		sscanf(fdv[2], "%ld", &ent->hc);
	else
		ent->hc = 0;

	/* Récupération de l'intensité instantanée. */

	if (fdc >= 4)
		sscanf(fdv[3], "%ld", &ent->ii);
	else
		ent->ii = 0;

	/* Récupération de la puissance apparente. */

	if (fdc >= 5)
		sscanf(fdv[4], "%ld", &ent->pa);
	else
		ent->pa = 0;

	return (wes_error_ok);
}

/* ---
 * Gestion des sections.
 * --- */

/* `emptyspan()`: Vidage d'une journée de données en local. */

static int emptyspan(wes_data_span_t *span)
{
	wes_data_t *data;

	while ((data = span->data)) {
		span->data = data->next;

		free(data);
	}

	return (wes_error_ok);
}

/* `gatherspandata()`: Récupération des données concernant une section
 * pour une période. */

static int gatherspandata(WES *wes, wes_data_span_t *span,
	int kind, wes_csv_entry_t *cb, int numfields,
	char * const buffers[], size_t const lens[])
{
	CURL *curl; CURLcode cres;
	wes_csv_t csv;
	int alt, err;

	for (alt = 0;; alt++) {
		/* On initialise la requête. */

		if ((err = get_request(wes, &curl, kind, alt, span->year,
			span->mon, span->dom))) {
			if (err == wes_error_iter)
				break;
			return (err);
		}

		wes_init_csv(&csv, ',', numfields, buffers, lens, cb, span);

		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &wes_read_csv);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &csv);

		cres = curl_easy_perform(curl);

		if (cres == CURLE_OK)
			break;
		else switch (cres) {
			case CURLE_REMOTE_FILE_NOT_FOUND:
				/* On ignore le fait qu'on n'aie pas trouvé le fichier avec
				 * ce format, et on essaye le suivant. */

				break;

			case CURLE_WRITE_ERROR:
				/* Une erreur s'est sans doute produite lors du décodage. */

				return (csv.err);

			default:
				return (wes_error_unknown);
		}
	}

	/* Si on est arrivés ici, c'est qu'on a essayé toutes les alternatives.
	 * Cela veut dire que l'on n'a trouvé aucun fichier CSV correspondant
	 * à la période. Cela ne veut pour autant pas dire qu'il y a une erreur,
	 * puisque la période peut ne pas avoir été prise.
	 * On se contente donc de renvoyer OK. */

	return (wes_error_ok);
}

/* `gatherspan()`: Récupération de ce qu'il faut pour une période.
 * Cette fonction doit récupérer trois différents fichiers, soit dans le
 * format moderne, soit dans l'ancien format (ou format « alternatif ») si
 * le premier n'existe pas. */

static int gatherspan(WES *wes, wes_data_span_t *span)
{
	char timebuf[6], fl[4][10], ti[4][20];
	char * const pl_buffers[5] = {timebuf, fl[0], fl[1], fl[2], fl[3]};
	size_t const pl_lens[5] = {6, 10, 10, 10, 10};
	char * const pc_buffers[5] = {timebuf, fl[0], fl[1], fl[2], fl[3]};
	size_t const pc_lens[5] = {6, 10, 10, 10, 10};
	char * const ti_buffers[5] = {timebuf, ti[0], ti[1], ti[2], ti[3]};
	size_t const ti_lens[5] = {6, 20, 20, 20, 20};
	int err;

	/* On commence par vider les données présentes dans la zone. */

	if ((err = emptyspan(span)))
		return (err);

	/* Ensuite, on récupère les données concernant les impulsions. */

	if ((err = gatherspandata(wes, span, KIND_PLS,
		(wes_csv_entry_t*)&gatherspanpl_add, 5, pl_buffers, pl_lens)))
		return (err);

	/* On enchaîne avec les données concernant les pinces ampèremétriques. */

	if ((err = gatherspandata(wes, span, KIND_PCE,
		(wes_csv_entry_t*)&gatherspanpc_add, 5, pc_buffers, pc_lens)))
		return (err);

	/* Puis on récupère les données concernant les compteurs via Téléinfo. */

	if ((err = gatherspandata(wes, span, KIND_TLI,
		(wes_csv_entry_t*)&gatherspanti_add, 5, ti_buffers, ti_lens)))
		return (err);

	/* On est bons ! */

	return (wes_error_ok);
}

/* `getspan()`: Récupération du set de données pour un jour donné. */

static int getspan(WES *wes, wes_data_span_t **spanresp, int year,
	int mon, int dom)
{
	wes_data_span_t **spanp, *span;
	int err;

	/* Partons à la recherche de l'entrée. */

	for (spanp = &wes->data; (span = *spanp); spanp = &span->next) {
		if (span->year > year || (span->year == year &&
			(span->mon  > mon  || (span->mon  == mon  &&
			(span->dom  >= dom)))))
			break;
	}
	if (span && span->year == year && span->mon == mon && span->dom == dom) {
		/* TODO: si c'est à la date d'aujourd'hui, l'actualiser ? */
		*spanresp = span;
		return (wes_error_ok);
	}

	/* L'entrée n'existe pas, mais on sait où la créer, alors créons-la. */

	span = malloc(sizeof(*span));
	if (!span)
		return (wes_error_alloc);

	span->next = *spanp;
	span->data = NULL;
	span->year = year;
	span->mon = mon;
	span->dom = dom;

	/* On la remplit. */

	if ((err = gatherspan(wes, span))) {
		free(span);
		return (err);
	}

	/* Elle est bonne, on l'ajoute à la liste et on termine le boulot ! */

	*spanp = span;
	*spanresp = span;
	return (wes_error_ok);
}

/* ---
 * Fonction de l'interface utilisateur.
 * --- */

int wes_get_data(WES *wes, wes_data_t *data, int year, int mon, int dom,
	int hour, int min)
{
	wes_data_span_t *span;
	wes_data_t *resp;
	int err;

	/* Récupération des données pour la date. */

	if ((err = getspan(wes, &span, year, mon, dom)))
		return (err);

	/* Récupération d'un pointeur vers l'entrée. */

	if ((err = getentry(span, &resp, hour, min))) {
		/* TODO: only copy that there is no data? */
		return (err);
	}

	/* TODO: do this properly */
	memcpy(data, resp, sizeof(wes_data_t));
	return (wes_error_ok);
}
