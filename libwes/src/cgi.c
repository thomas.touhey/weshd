/* ****************************************************************************
 * cgi.c -- téléversement et exécution de scripts CGI sur le serveur WES.
 * Copyright (C) 2018 Thomas Touhey <thomas@touhey.fr>
 *
 * This project is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the
 * CeCILL license as circulated by CEA, CNRS and INRIA at the
 * following URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the project's author, the holder of the
 * economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also therefore
 * means that it is reserved for developers and experienced professionals
 * having in-depth computer knowledge. Users are therefore encouraged to load
 * and test the software's suitability as regards their requirements in
 * conditions enabling the security of their systems and/or data to be
 * ensured and, more generally, to use and operate it in the same conditions
 * as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 * ************************************************************************* */
#include "internals.h"

/* Histoire de ne pas confondre la réponse d'une ligne avec la réponse
 * d'une autre, on introduit un séparateur. */

#define VARLEN 4
#define REQSEP "--- REQUEST SEPARATOR ---"
#define ENDL "\r\n"

/* ---
 * Téléversement (upload) d'un script CGI correspondant à une suite
 * de requêtes.
 * --- */

#define UST_REQ 0 /* 'c <CMD ><FORMAT> */
#define UST_END 1 /* '. <CRLF>' de fin */
#define UST_MAX 2

#define USR_CMD 0 /* 'c ' au début de la requête */
#define USR_VAR 1 /* nom de la variable */
#define USR_FMT 2 /* format */
#define USR_SEP 3 /* CRLF, 't ' et séparateur de requête, CRLF */
#define USR_MAX 4

#define USE_CMD 0 /* '. ' au début de la requête */
#define USE_MAX 1

struct ucookie {
	int state, step;
	int id, num;
	int err;

	int ucookie__align;

	const wes_cgi_t *blocks;
	size_t off, current_fmt_len;
	char command[VARLEN];

	int ucookie__align2;
};

/* `init_cookie_block()`: Initialisation du cookie avec un bloc, et
 * initialisation du bloc s'il y a lieu. */

static inline void init_cookie_block(struct ucookie *cookie,
	const wes_cgi_t *block)
{
	char *var = cookie->command;
	size_t len;

	len = strlen(block->var);
	len = len > VARLEN ? VARLEN : len;
	memcpy(var, block->var, len);
	memset(&var[len], ' ', VARLEN - len);

	cookie->current_fmt_len = block->format ? strlen(block->format) : 0;
}

/* `sendscript_read()`: Callback de lecture de la fonction d'envoi
 * de script. */

static size_t sendscript_read(char *buffer, size_t blksize, size_t nmemb,
	struct ucookie *cookie)
{
	size_t left = blksize * nmemb;
	const char *buf; size_t len, elen, off;

	if (cookie->err != wes_error_ok || cookie->id >= cookie->num)
		return (0);

	while (left) {
		/* On sélectionne le buffer en fonction de l'étape qu'on est
		 * en train d'écrire. */

		switch (cookie->state) {
		case UST_REQ:
			switch (cookie->step) {
			case USR_CMD:
				buf = "c ";
				len = 2;
				break;
			case USR_VAR:
				buf = cookie->command;
				len = VARLEN;
				break;
			case USR_FMT:
				buf = cookie->blocks[cookie->id].format;
				len = cookie->current_fmt_len;
				break;
			case USR_SEP:
				buf = ENDL "t " REQSEP ENDL;
				len = sizeof(ENDL) + 2 + sizeof(REQSEP) + sizeof(ENDL) - 3;
				break;
			default: /* so the compiler can stop yellin' */
				buf = "";
				len = 0;
				break;
			}
			break;
		case UST_END:
			buf = ". " ENDL;
			len = 2 + sizeof(ENDL) - 1;
			break;
		default: /* so the compiler can stop yellin' */
			buf = "";
			len = 0;
			break;
		}

		/* On extrait ce qu'il faut. */

		off = cookie->off;
		elen = len - off;
		if (elen > left)
			elen = left;
		memcpy(buffer, &buf[off], elen);
		buffer += elen;
		left -= elen;
		cookie->off += elen;

		/* On vérifie si on doit changer d'étape ou carrément de
		 * bloc. */

		if (cookie->off < len)
			continue;

		/* On change d'étape. */

		cookie->off = 0;
		cookie->step += 1;

		if ((cookie->state == UST_REQ && cookie->step < USR_MAX)
		 || (cookie->state == UST_END && cookie->step < USE_MAX))
			continue;

		if (cookie->state == UST_REQ) {
			/* On change de bloc. */

			cookie->step = 0;
			cookie->id++;

			/* On vérifie bien qu'on n'a pas dépassé. */

			if (cookie->id < cookie->num) {
				/* On n'oublie pas de calculer la taille courante. */

				init_cookie_block(cookie, &cookie->blocks[cookie->id]);

				continue;
			}
		}

		/* On change de statut. */
		cookie->step = 0;
		cookie->state++;

		if (cookie->state == UST_MAX)
			break;
	}

	return ((blksize * nmemb - left) / blksize);
}

/* `estimatescriptsize()`: Estimation de la taille du fichier qu'on va
 * créer. */

static curl_off_t estimatescriptsize(int num, const wes_cgi_t *blocks)
{
	curl_off_t filesize = 0;
	size_t fmtsize;
	int i;

	for (i = 0; i < num; i++) {
		fmtsize = blocks[i].format ? strlen(blocks[i].format) : 0;
		filesize += 2 + 4 + fmtsize + sizeof(ENDL)
			+ 2 + sizeof(REQSEP) + sizeof(ENDL) - 3;
	}

	filesize += 2 + sizeof(ENDL) - 1;

	return (filesize);
}

/* `sendscript()`: Envoi d'un script CGI. */

static int sendscript(WES *wes, int num, const wes_cgi_t *blocks)
{
	CURL *curl; CURLcode cres;
	char path[50];
	struct ucookie cookie;
	int err;

	/* Initialisation du handle de la libcurl. */

	sprintf(path, "/whscript.cgi");
	if ((err = wes_open_ftp_handle(wes, &curl, path)))
		return (err);

	/* Initialisation du cookie. */

	cookie.state = 0;
	cookie.step = UST_REQ;
	cookie.id = 0;
	cookie.err = wes_error_ok;
	cookie.off = 0;
	cookie.num = num;
	cookie.blocks = blocks;

	if (num > 0)
		init_cookie_block(&cookie, &blocks[0]);

	/* Définition des méta-informations. */

	curl_easy_setopt(curl, CURLOPT_VERBOSE, 0L);
	curl_easy_setopt(curl, CURLOPT_UPLOAD, 1L);

	/* Définition du fichier à envoyer. */

	curl_easy_setopt(curl, CURLOPT_READDATA, &cookie);
	curl_easy_setopt(curl, CURLOPT_READFUNCTION, sendscript_read);
	curl_easy_setopt(curl, CURLOPT_INFILESIZE_LARGE,
		estimatescriptsize(num, blocks));

	/* Exécution de la requête. */

	cres = curl_easy_perform(curl);

	if (cres != CURLE_OK) {
		msg((wlinfo, "curl error: %s", curl_easy_strerror(cres)));

		switch (cres) {
		case CURLE_READ_ERROR:
		case CURLE_WRITE_ERROR:
			/* Le callback de lecture a fait s'arrêter le transfert. */

			return (cookie.err);

		case CURLE_OUT_OF_MEMORY:
			/* Une allocation a échoué. */

			return (wes_error_alloc);

		case CURLE_COULDNT_RESOLVE_HOST: /* FALLTHRU */
		case CURLE_COULDNT_CONNECT: /* FALLTHRU */
		case CURLE_FTP_CANT_GET_HOST: /* FALLTHRU */
		case CURLE_SEND_ERROR: /* FALLTHRU */
		case CURLE_RECV_ERROR:
			/* Quelque chose s'est mal passé sur la transmission. */

			return (wes_error_nohost);

		case CURLE_LOGIN_DENIED:
			return (wes_error_auth);

		default:
			return (wes_error_unknown);
		}
	}

	return (wes_error_ok);
}

/* ---
 * Suppression d'un script CGI correspondant à une suite de requêtes.
 * --- */

#if DELETE_SCRIPTS

/* `delscript_write()`: Sample write to ignore the content. */

static size_t delscript_write(char *buffer, size_t blksize, size_t nmemb,
	void *cookie)
{
	(void)buffer;
	(void)cookie;
	(void)blksize;

	return (nmemb);
}

/* `delscript()`: Suppression d'un script CGI. */

static int delscript(WES *wes)
{
	char hdr[20];
	CURL *curl; CURLcode cres;
	struct curl_slist *headerlist = NULL;
	int err;

	/* Initialisation du handle de la libcurl. */

	if ((err = wes_open_ftp_handle(wes, &curl, "/")))
		return (err);

	/* Préparation des méta-informations. */

	sprintf(hdr, "DELE whscript.cgi");

	headerlist = curl_slist_append(headerlist, hdr);

	curl_easy_setopt(curl, CURLOPT_QUOTE, headerlist);

	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, delscript_write);

	/* Exécution de la requête. */

	cres = curl_easy_perform(curl);
	curl_slist_free_all(headerlist);

	if (cres != CURLE_OK && cres != CURLE_REMOTE_FILE_NOT_FOUND) {
		msg((wlinfo, "curl error: %s", curl_easy_strerror(cres)));

		switch (cres) {
		case CURLE_OUT_OF_MEMORY:
			/* Une allocation a échoué. */

			return (wes_error_alloc);

		case CURLE_COULDNT_RESOLVE_HOST: /* FALLTHRU */
		case CURLE_COULDNT_CONNECT: /* FALLTHRU */
		case CURLE_FTP_CANT_GET_HOST: /* FALLTHRU */
		case CURLE_SEND_ERROR: /* FALLTHRU */
		case CURLE_RECV_ERROR:
			/* Quelque chose s'est mal passé sur la transmission. */

			return (wes_error_nohost);

		case CURLE_LOGIN_DENIED:
			return (wes_error_auth);

		default:
			return (wes_error_unknown);
		}
	}

	return (wes_error_ok);
}

#endif

/* ---
 * Récupération des valeurs du script par HTTP.
 * --- */

/* `open_http_exec_handle()`: ouverture d'un handle de récupération de
 * script CGI, avec arguments */

static int open_http_exec_handle(WES *wes, CURL **curlp,
	wes_cgi_method_t method, int argc, wes_cgi_arg_t *args)
{
	char path[20];

	sprintf(path, "/whscript.cgi");
	return (wes_open_http_handle(wes, curlp, method, path, argc, args));
}

/* Cookie d'écriture (`ecookie` comme `execution cookie`).
 * Le but du callback qui utilisera ce cookie est de lire chaque ligne et de
 * voir s'il s'agit d'une ligne de fin ou d'un contenu normal.
 *
 * - `id`: identifiant numérique du bloc courant.
 * - `num`: nombre de blocs.
 * - `err`: s'il y a eu une erreur.
 *
 * - `buffers`: tableau des buffers à remplir (0: final, 1: sép. temp.)
 * - `lens`: tableau des longueurs (pareil)
 * - `lefts`: tableau des longueurs restantes (pareil)
 * - `bufid`: numéro du buffer à utiliser pour `buffers` et `lefts`.
 *
 * Il y a sans doute une meilleure manière d'implémenter ça, mais là,
 * je cherche juste à ce que ça marche. */

struct ecookie {
	wes_cgi_t *blocks;
	char *sepbuf;

	int id, num, err, bufid;

	char *buffers[2];
	size_t lefts[2];
};

/* `execscript_write()`: Callback de récupération de la réponse du script
 * CGI exécuté. */

static size_t execscript_write(char *buffer, size_t blksize, size_t nmemb,
	struct ecookie *cookie)
{
	wes_cgi_t *block;
	int car;
	size_t left, len;

	if (cookie->err)
		return (0);

	left = blksize * nmemb;
	if (cookie->id >= cookie->num)
		left = 0;

	for (; left; left--) {
		car = *buffer++;

		switch (car) {
		case '\r':
			/* Ignorer, même s'il ne fait pas partie du CRLF de fin */
			break;

		case '\n':
			/* Vérifions que nous avons bien le bon buffer. */

			if (cookie->bufid) {
				/* Vérifions s'il s'agit d'un séparateur. */

				if (!cookie->lefts[1]) {
					/* C'est une ligne de séparateur !
					 * Ça veut soit dire qu'on n'a pas de contenu,
					 * soit qu'on change tout simplement pour le contenu
					 * suivant. */

					cookie->id++;
					if (cookie->id >= cookie->num) {
						/* Attention, à 1 et non à 0 : il faut jouer avec
						 * la décrémentation apportée par la boucle `for` ;) */

						left = 1;
						break;
					}

					/* ATTENTION: à ne pas mettre avant l'incrémentation de
					 * l'ID dans le cookie! */

					block = &cookie->blocks[cookie->id];

					cookie->buffers[0] = block->buffer;
					cookie->lefts[0] = block->len - 1;
					cookie->buffers[1] = cookie->sepbuf;
					cookie->lefts[1] = sizeof(REQSEP) - 1;

					*cookie->buffers[0] = '\0';
					block->full = 1; /* NUL character */
					block->lines = 0;

					break;
				}

				block = &cookie->blocks[cookie->id];

				/* Incrémentons le nombre de lignes.
				 * Si ce n'est pas la première ligne, mettons un retour à
				 * la ligne. */

				if (cookie->lefts[0])
					cookie->blocks[cookie->id].lines++;

				if (cookie->lefts[0] + 1 < block->len) {
					cookie->blocks[cookie->id].full++;

					if (cookie->lefts[0]) {
						*cookie->buffers[0]++ = '\n';
						*cookie->buffers[0] = '\0';
						cookie->lefts[0]--;
					}
				}

				/* Puis copions dans le buffer final, autant de bytes que
				 * possible. */

				len = sizeof(REQSEP) - 1 - cookie->lefts[1];
				cookie->blocks[cookie->id].full += len;
				cookie->buffers[1] -= len;
				if (len > cookie->lefts[0])
					len = cookie->lefts[0];

				memcpy(cookie->buffers[0], cookie->buffers[1], len);
				cookie->buffers[0] += len;
				*cookie->buffers[0] = '\0';
			}

			cookie->buffers[1] = cookie->sepbuf;
			cookie->lefts[1] = sizeof(REQSEP) - 1;
			cookie->bufid = 1;
			break;

		default:
			/* TODO? On vérifie si le caractère est bon. */

			/* On regarde si le cookie est encore sur le buffer
			 * relatif au séparateur. */

			if (cookie->bufid) {
				block = &cookie->blocks[cookie->id];

				/* Si ça fait partie du séparateur, on continue de remplir
				 * le buffer associé au séparateur. */

				if (cookie->bufid && cookie->lefts[1]
				 && *cookie->buffers[1] == car) {
					cookie->buffers[1]++;
					cookie->lefts[1]--;
					break;
				}

				/* Incrémentons le nombre de lignes.
				 * Si ce n'est pas la première ligne, mettons un retour à
				 * la ligne. */

				if (cookie->lefts[0])
					cookie->blocks[cookie->id].lines++;

				if (cookie->lefts[0] + 1 < block->len) {
					cookie->blocks[cookie->id].full++;

					if (cookie->lefts[0]) {
						*cookie->buffers[0]++ = '\n';
						*cookie->buffers[0] = '\0';
						cookie->lefts[0]--;
					}
				}

				/* Puis copions dans le buffer final, autant de bytes que
				 * possible. */

				len = sizeof(REQSEP) - 1 - cookie->lefts[1];
				cookie->blocks[cookie->id].full += len;
				cookie->buffers[1] -= len;
				if (len > cookie->lefts[0])
					len = cookie->lefts[0];

				memcpy(cookie->buffers[0], cookie->buffers[1], len);
				cookie->buffers[0] += len;
				*cookie->buffers[0] = '\0';

				/* On est bien dans le buffer de l'utilisateur ! */

				cookie->bufid = 0;
			}

			/* On copie le caractère si on peut. */

			cookie->blocks[cookie->id].full++;
			if (cookie->lefts[0]) {
				*cookie->buffers[0]++ = (char)car;
				*cookie->buffers[0] = '\0';
				cookie->lefts[0]--;
			}

			break;
		}
	}

	return ((blksize * nmemb - left) / blksize);
}

/* `execscript()`: Exécution d'un script CGI via HTTP. */

static int execscript(WES *wes, wes_cgi_method_t method,
	int argc, wes_cgi_arg_t *args, int blkc, wes_cgi_t *blocks)
{
	CURL *curl; CURLcode cres;
	char sepbuf[sizeof(REQSEP) + 1];
	struct ecookie cookie;
	int err;

	/* Récupération d'un handle de la libcurl pour exécution. */

	if ((err = open_http_exec_handle(wes, &curl, method, argc, args)))
		return (err);

	/* Initialisation du cookie. */

	cookie.id = 0;
	cookie.num = blkc;
	cookie.blocks = blocks;
	cookie.err = wes_error_ok;
	cookie.sepbuf = sepbuf;
	cookie.buffers[1] = sepbuf;
	memcpy(cookie.buffers[1], REQSEP, sizeof(REQSEP));
	cookie.lefts[1] = sizeof(REQSEP) - 1;
	cookie.bufid = 1;

	if (blkc) {
		cookie.buffers[0] = blocks[0].buffer;
		cookie.lefts[0] = blocks[0].len - 1;

		*cookie.buffers[0] = '\0';
		blocks[0].lines = 0;
		blocks[0].full = 1; /* NUL character */
	}

	/* Définition du fichier à envoyer. */

	curl_easy_setopt(curl, CURLOPT_WRITEDATA, &cookie);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, execscript_write);

	/* Exécution de la requête. */

	cres = curl_easy_perform(curl);

	if (cres != CURLE_OK) {
		msg((wlinfo, "curl error: %s", curl_easy_strerror(cres)));

		switch (cres) {
		case CURLE_OK:
			/* Tout s'est bien passé. */
			break;

		case CURLE_READ_ERROR:
		case CURLE_WRITE_ERROR:
			/* Le callback de lecture a fait s'arrêter le transfert. */

			return (cookie.err);

		case CURLE_OUT_OF_MEMORY:
			/* Une allocation a échoué. */

			return (wes_error_alloc);

		case CURLE_COULDNT_RESOLVE_HOST: /* FALLTHRU */
		case CURLE_COULDNT_CONNECT: /* FALLTHRU */
		case CURLE_FTP_CANT_GET_HOST: /* FALLTHRU */
		case CURLE_SEND_ERROR: /* FALLTHRU */
		case CURLE_RECV_ERROR:
			/* Quelque chose s'est mal passé sur la transmission. */

			return (wes_error_nohost);

		case CURLE_LOGIN_DENIED:
			return (wes_error_auth);

		default:
			return (wes_error_unknown);
		}
	}

	return (wes_error_ok);
}

/* ---
 * Interface utilisateur.
 * --- */

/* `wes_get_cgi_with_args()`: Téléversement et exécution de scripts CGI sur le
 * serveur WES, avec gestion d'arguments GET et POST. */

int wes_get_cgi_with_args(WES *wes, wes_cgi_method_t method,
	int argc, wes_cgi_arg_t *args,
	int blkc, wes_cgi_t *blocks)
{
	int err, nerr = 0;
#if DELETE_SCRIPTS
	int was_sent = 0;
#endif

	/* On vérifie les arguments histoire de ne pas aller trop loin
	 * pour rien. */

	if (!argc && !blkc)
		return (wes_error_ok);

	/* Génération et téléversement du script. */

	if (blkc) {
		msg((wlinfo, "script generation and upload"));
		if ((err = sendscript(wes, blkc, blocks)))
			return (err);

#if DELETE_SCRIPTS
		was_sent = 1;
#endif
	}

	/* Exécution du script. */

	msg((wlinfo, "script execution through HTTP"));
	err = execscript(wes, method, argc, args, blkc, blocks);

	/* Suppression du script. */

#if DELETE_SCRIPTS
	if (was_sent) {
		msg((wlinfo, "script deletion"));
		nerr = delscript(wes);
	}
#endif

	return (err ? err : nerr);
}

/* `wes_get_form()`: Exécution d'une requête avec arguments via GET. */

int wes_get_form(WES *wes, int argc, wes_cgi_arg_t *args)
{
	return (wes_get_cgi_with_args(wes, WESCGIMETHOD_GET, argc, args,
		0, NULL));
}

/* `wes_post_form()`: Exécution d'une requête avec arguments via POST. */

int wes_post_form(WES *wes, int argc, wes_cgi_arg_t *args)
{
	return (wes_get_cgi_with_args(wes, WESCGIMETHOD_POST, argc, args,
		0, NULL));
}

/* `wes_get_cgi()`: Téléversement et exécution de scripts CGI sur le
 * serveur WES. */

int wes_get_cgi(WES *wes, int blkc, wes_cgi_t *blocks)
{
	return (wes_get_cgi_with_args(wes, WESCGIMETHOD_GET, 0, NULL,
		blkc, blocks));
}

/* `wes_get_cgi_var()`: Récupération de valeurs par CGI. */

int wes_get_cgi_var(WES *wes, const char *var, const char *format,
	char *buffer, size_t *len)
{
	wes_cgi_t cgi;
	int err;

	cgi.var = var;
	cgi.format = format;
	cgi.buffer = buffer;
	cgi.len = *len;

	if ((err = wes_get_cgi(wes, 1, &cgi)))
		return (err);

	*len = cgi.full;
	if (cgi.full > cgi.len)
		return (wes_error_space);
	return (wes_error_ok);
}
