/* ****************************************************************************
 * ip/csv.c -- gestion des données en CSV par FTP des serveurs WES.
 * Copyright (C) 2018 Thomas Touhey <thomas@touhey.fr>
 *
 * This project is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the
 * CeCILL license as circulated by CEA, CNRS and INRIA at the
 * following URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the project's author, the holder of the
 * economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also therefore
 * means that it is reserved for developers and experienced professionals
 * having in-depth computer knowledge. Users are therefore encouraged to load
 * and test the software's suitability as regards their requirements in
 * conditions enabling the security of their systems and/or data to be
 * ensured and, more generally, to use and operate it in the same conditions
 * as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 * ************************************************************************* */
#include "internals.h"
#define validfieldcar(CAR) (isascii(CAR) && (CAR) != '\r')

/* `wes_init_csv()`: Initialise le cookie de récupération de fichiers CSV.
 * Prend le nombre de champs à remplir, les buffers et leur longueur (qui
 * _DOIT_ faire au moins un caractère pour contenir celui de terminaison '\0'),
 * ainsi que le callback à appeler en cas d'entrée lue (y compris la
 * première qui donne le nom des champs, au callback de détecter ça). */

int wes_init_csv(wes_csv_t *cookie, int separator,
	int numfields, char * const buffers[], size_t const lens[],
	wes_csv_entry_t *callback, void *cbcookie)
{
	cookie->num = numfields;
	cookie->buffers = buffers;
	cookie->lens = lens;
	cookie->callback = callback;
	cookie->cbcookie = cbcookie;
	cookie->separator = separator;

	cookie->line = 0;
	cookie->id = 0;
	cookie->off = 0;
	cookie->err = 0;

	return (wes_error_ok);
}

/* `wes_read_csv()`: Callback de contribution à la lecture de CSV. */

size_t wes_read_csv(char *buf, size_t size, size_t nmemb, void *rawcookie)
{
	wes_csv_t *cookie = (void*)rawcookie;
	size_t left = size * nmemb, inisz = left;
	int car, err;

	if (cookie->err)
		return (0);

	for (; left; left--) {
		car = *buf++;

		if (car == cookie->separator) {
			if (cookie->id < cookie->num) {
				cookie->buffers[cookie->id][cookie->off] = '\0';
				cookie->off = 0;

				/* On pourrait incrémenter l'id hors de la condition, mais
				 * ça veut dire que s'il y a suffisamment de champs dans
				 * le fichier (ce qui fait un bon paquet, mais tout de même),
				 * il pourrait y avoir un overflow, donc autant s'arrêter
				 * de compter à la première valeur invalide, `cookie->num`. */

				cookie->id++;
			}
		} else if (car == '\n') {
			if (cookie->id < cookie->num)
				cookie->buffers[cookie->id][cookie->off] = '\0';

			/* Les données de la ligne sont prêtes, on n'a plus qu'à appeler
			 * le callback avec ce qu'on a ! */

			err = (*cookie->callback)(cookie->cbcookie, cookie->line,
				cookie->id >= cookie->num ? cookie->num : cookie->id + 1,
				cookie->buffers);
			if (err) {
				cookie->err = err;
				return ((inisz - left) / size);
			}

			cookie->id = 0;
			cookie->line++;
			cookie->off = 0;
		} else if (cookie->id < cookie->num
		 && cookie->off < cookie->lens[cookie->id] - 1
		 && validfieldcar(car))
			cookie->buffers[cookie->id][cookie->off++] = (char)car;
	}

	return (nmemb);
}

/* `wes_end_csv()`: Termine le traitement d'un CSV. */

int wes_end_csv(wes_csv_t *cookie)
{
	int err;

	if (!cookie->id && !cookie->off)
		return (wes_error_ok);

	if (cookie->id < cookie->num)
		cookie->buffers[cookie->id][cookie->off] = '\0';

	/* Les données de la dernière ligne sont prêtes, on n'a plus qu'à appeler
	 * le callback avec ce qu'on a ! */

	err = (*cookie->callback)(cookie->cbcookie, cookie->line,
		cookie->id >= cookie->num ? cookie->num : cookie->id + 1,
		cookie->buffers);
	if (err) {
		cookie->err = err;
		return (err);
	}

	cookie->id = 0;
	cookie->off = 0;

	return (wes_error_ok);
}
