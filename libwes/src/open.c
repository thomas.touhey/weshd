/* ****************************************************************************
 * open.c -- ouvrir et fermer un handle de WES.
 * Copyright (C) 2018 Thomas Touhey <thomas@touhey.fr>
 *
 * This project is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the
 * CeCILL license as circulated by CEA, CNRS and INRIA at the
 * following URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the project's author, the holder of the
 * economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also therefore
 * means that it is reserved for developers and experienced professionals
 * having in-depth computer knowledge. Users are therefore encouraged to load
 * and test the software's suitability as regards their requirements in
 * conditions enabling the security of their systems and/or data to be
 * ensured and, more generally, to use and operate it in the same conditions
 * as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 * ************************************************************************* */
#include "internals.h"

/* `wes_open()`: créer une ressource. */

int wes_open(WES **resourcep, wes_addr_t const *addr)
{
	WES *res;

	if (!addr || addr->type != WESINET || ~addr->data.inet.has & WESHASIPv4)
		return (wes_error_val);
	if (!(res = (*resourcep = malloc(sizeof(*res)))))
		return (wes_error_alloc);

	res->http_name = NULL;
	res->http_pass = NULL;
	res->ftp_name  = NULL;
	res->ftp_pass  = NULL;
	res->http_handle = NULL;
	res->ftp_handle = NULL;
	res->data = NULL;
	res->addr.type = WESINET;
	res->addr.data.inet.has = WESHASIPv4;
	memcpy(&res->addr.data.inet.ipv4, &addr->data.inet.ipv4, 4);
	wes_init_m2m(res);

	return (wes_error_ok);
}

/* `wes_close()`: fermer une ressource. */

void wes_close(WES *res)
{
	if (!res)
		return ;

	if (res->http_handle) {
		curl_easy_cleanup(res->http_handle);
		res->http_handle = NULL;
	}
	if (res->ftp_handle) {
		curl_easy_cleanup(res->ftp_handle);
		res->ftp_handle = NULL;
	}

	wes_deinit_m2m(res);

#if 0
	clear_config(res);
#endif
	/* FIXME: clear_data(res); */

	wes_set_http_ident(res, NULL, NULL);
	wes_set_ftp_ident(res, NULL, NULL);
	free(res);
}
