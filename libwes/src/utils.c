/* ****************************************************************************
 * utils.c -- utilitaires.
 * Copyright (C) 2018 Thomas Touhey <thomas@touhey.fr>
 *
 * This project is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the
 * CeCILL license as circulated by CEA, CNRS and INRIA at the
 * following URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the project's author, the holder of the
 * economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also therefore
 * means that it is reserved for developers and experienced professionals
 * having in-depth computer knowledge. Users are therefore encouraged to load
 * and test the software's suitability as regards their requirements in
 * conditions enabling the security of their systems and/or data to be
 * ensured and, more generally, to use and operate it in the same conditions
 * as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 * ************************************************************************* */
#include "internals.h"

/* ---
 * Définitions des identifiants HTTP et FTP à utiliser.
 * --- */

static char *default_http_name = "admin";
static char *default_http_pass = "wes";
static char  *default_ftp_name = "adminftp";
static char  *default_ftp_pass = "wesftp";

/* `set_http_ident()`: Définir les identifiants HTTP du serveur WES. */

int wes_set_http_ident(WES *wes, const char *namep, const char *passp)
{
	char *name = NULL, *pass = NULL;
	size_t sz;

	/* Nouveau nom d'utilisateur. */

	if (!namep)
		name = default_http_name;
	else if (!strcmp(namep, wes->http_name))
		name = wes->http_name;
	else {
		sz = strlen(namep) + 1;
		if (!(name = malloc(sz)))
			return (wes_error_alloc);

		memcpy(name, namep, sz);
	}

	/* Nouveau mot de passe. */

	if (!passp)
		pass = default_http_pass;
	else if (!strcmp(passp, wes->http_pass))
		pass = wes->http_pass;
	else {
		sz = strlen(passp) + 1;
		if (!(pass = malloc(sz))) {
			free(name);
			return (wes_error_alloc);
		}

		memcpy(pass, passp, sz);
	}

	/* Changements à effectuer. */

	if (wes->http_name != default_http_name)
		free(wes->http_name);
	if (wes->http_pass != default_http_pass)
		free(wes->http_pass);

	wes->http_name = name;
	wes->http_pass = pass;

	return (wes_error_ok);
}

/* `wes_set_ftp_ident()`: Définir les identifiants FTP du serveur WES. */

int wes_set_ftp_ident(WES *wes, const char *namep, const char *passp)
{
	char *name = NULL, *pass = NULL;
	size_t sz;

	/* Nouveau nom d'utilisateur. */

	if (!namep)
		name = default_ftp_name;
	else if (!strcmp(namep, wes->ftp_name))
		name = wes->ftp_name;
	else {
		sz = strlen(namep) + 1;
		if (!(name = malloc(sz)))
			return (wes_error_alloc);

		memcpy(name, namep, sz);
	}

	/* Nouveau mot de passe. */

	if (!passp)
		pass = default_ftp_pass;
	else if (!strcmp(passp, wes->ftp_pass))
		pass = wes->ftp_pass;
	else {
		sz = strlen(passp) + 1;
		if (!(pass = malloc(sz))) {
			free(name);
			return (wes_error_alloc);
		}

		memcpy(pass, passp, sz);
	}

	/* Changements à effectuer. */

	if (wes->ftp_name != default_ftp_name)
		free(wes->ftp_name);
	if (wes->ftp_pass != default_ftp_pass)
		free(wes->ftp_pass);

	wes->ftp_name = name;
	wes->ftp_pass = pass;

	return (wes_error_ok);
}

/* ---
 * Ouverture d'un canal HTTP.
 * --- */

/* `strlen_encoded()`: taille de la chaîne après encodage.
 * Gère les caractères spéciaux type `&`, `?`, `=`, …
 * Doit _impérativement_ évoluer en même temps que `strcpy_encoded()`. */

static size_t strlen_encoded(char const *src)
{
	int car;
	size_t len = 0;

	for (; (car = *src); src++) switch (car) {
		case '?':
			/* percent encoding. */
			return (3);
		default:
			len++;
			break;
	}

	return (len);
}

/* `strcpy_encoded()`: copie de la chaîne en s'occupant des caractères
 * spéciaux à échapper, e.g. `&`, `?`, `=`, …
 * Doit _impérativement_ évoluer en même temps que `strlen_encoded()`. */

static void strcpy_encoded(char *dest, const char *src)
{
	int car;
	char buf[4] = "%  ";
	const char *esc = buf;
	size_t len = 3;

	for (; (car = *src); src++) {
		switch (car) {
		case '?':
			buf[1] = (car >> 4) & 15;
			buf[2] = car & 15;
			break;
		default:
			*dest++ = (char)car;
			continue;
		}

		memcpy(dest, esc, len);
		dest += len;
	}
}

/* `make_qs()`: création de la query string sous la forme
 * `arg1=val1&arg2=val2`, pour requêtes GET et POST avec arguments.
 * La fonction renvoie la taille qu'elle aurait dû avoir pour pouvoir
 * tout stocker. */

static size_t make_qs(int argc, wes_cgi_arg_t *args, char *buf, size_t size)
{
	int i;
	size_t notfirst = 0;
	size_t lenkey, lenval, lenarg, full = 1; /* caractère NUL */
	wes_cgi_arg_t *arg;

	if (size)
		size--; /* caractère NUL à la fin du buffer. */

	for (i = 0; i < argc; i++) {
		arg = &args[i];

		/* Calcul des longueurs, puis on voit si ça rentre. */

		lenarg = notfirst; /* '&' au début */
		lenkey = strlen_encoded(arg->name);
		lenval = arg->value ? 1 /* '=' */ + strlen_encoded(arg->value) : 0;
		lenarg += lenkey + lenval;
		full += lenarg;
		if (lenarg > size)
			continue ;

		/* '&' au début. */
		if (notfirst)
			*buf++ = '&';

		/* Clé. */
		strcpy_encoded(buf, arg->name);
		buf += lenkey;

		/* '=' puis valeur. */
		if (arg->value) {
			*buf++ = '=';
			strcpy_encoded(buf, arg->value);
			buf += lenval - 1;
		}

		size -= lenarg;
		notfirst = 1;
	}

	*buf = '\0';
	return (full);
}

/* `wes_open_http_handle()`: Ouverture d'un canal HTTP pour un chemin HTTP.
 * Ce canal n'a point besoin d'être fermé. */

int wes_open_http_handle(WES *wes, CURL **handlep, wes_http_method_t method,
	const char *path, int argc, wes_http_arg_t *args)
{
	CURL *handle;
	const unsigned char *ip;
	char url[50 + 200 + 200 + 1], postdata[201], *p;
	size_t left, sz;

	if (method != WESHTTPMETHOD_GET && method != WESHTTPMETHOD_POST)
		return (wes_error_op);

	/* On prépare l'URL complète, avec protocole et interface du WES. */

	if (wes->addr.data.inet.has & WESHASIPv4) {
		ip = wes->addr.data.inet.ipv4;
		sprintf(url, "http://%d.%d.%d.%d", ip[0], ip[1], ip[2], ip[3]);
	} else if (wes->addr.data.inet.has & WESHASIPv6) {
		ip = wes->addr.data.inet.ipv4;
		sprintf(url, "http://[%02X%02X:%02X%02X:%02X%02X:%02X%02X"
			":%02X%02X:%02X%02X:%02X%02X:%02X%02X]",
			ip[0], ip[1], ip[2], ip[3], ip[4], ip[5], ip[6], ip[7],
			ip[8], ip[9], ip[10], ip[11], ip[12], ip[13], ip[14], ip[15]);
	} else
		return (wes_error_op);

	sz = strlen(url);
	p = &url[sz];
	left = sizeof(url) - sz;

	sz = strlen(path);
	if (sz > left)
		return (wes_error_op);
	memcpy(p, path, sz);
	p += sz;
	left -= sz;
	*p = '\0';

	if (method == WESHTTPMETHOD_GET && argc) {
		if (left <= 1)
			return (wes_error_op);
		*p++ = '?';
		*p = '\0';
		left--;

		if (make_qs(argc, args, p, left) > left)
			return (wes_error_op);
	}

	/* On prépare le postdata si nécessaire. */

	if (method == WESHTTPMETHOD_POST
	 && (sz = make_qs(argc, args, postdata, 201)) > 201)
		return (wes_error_op);

	/* On ouvre le handle si nécessaire. */

	if (wes->http_handle)
		handle = wes->http_handle;
	else {
		handle = curl_easy_init();
		if (!handle)
			return (wes_error_unknown);

		wes->http_handle = handle;
	}

	/* On prépare le handle à ce qui l'attend. */

	curl_easy_setopt(handle, CURLOPT_URL, url);
	curl_easy_setopt(handle, CURLOPT_USERNAME, wes->http_name);
	curl_easy_setopt(handle, CURLOPT_PASSWORD, wes->http_pass);
	curl_easy_setopt(handle, CURLOPT_UPLOAD, 0L);

	if (method == WESHTTPMETHOD_GET)
		curl_easy_setopt(handle, CURLOPT_POST, 0L);
	else {
		curl_easy_setopt(handle, CURLOPT_POST, 1L);
		curl_easy_setopt(handle, CURLOPT_POSTFIELDS, postdata);
		curl_easy_setopt(handle, CURLOPT_POSTFIELDSIZE, sz - 1);
	}

	curl_easy_setopt(handle, CURLOPT_VERBOSE, CURLHTTPVERBOSE);
	curl_easy_setopt(handle, CURLOPT_QUOTE, NULL);
	curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, NULL);
	curl_easy_setopt(handle, CURLOPT_WRITEDATA, stdout);
	curl_easy_setopt(handle, CURLOPT_READFUNCTION, NULL);
	curl_easy_setopt(handle, CURLOPT_READDATA, stdin);
	curl_easy_setopt(handle, CURLOPT_USERAGENT, "weshd/" WESHD_VERSION);

	*handlep = handle;
	return (wes_error_ok);
}

/* ---
 * Ouverture d'un canal FTP.
 * --- */

/* `wes_open_ftp_handle()`: Ouverture d'un canal FTP pour un chemin FTP.
 * Ce canal n'a point besoin d'être fermé. */

int wes_open_ftp_handle(WES *wes, CURL **handlep, char const *path)
{
	CURL *handle;
	const unsigned char *ip;
	char url[50 + 200 + 1];

	/* On prépare l'URL complète, avec protocole et interface du WES. */

	if (wes->addr.data.inet.has & WESHASIPv4) {
		ip = wes->addr.data.inet.ipv4;
		sprintf(url, "ftp://%d.%d.%d.%d%.200s", ip[0], ip[1], ip[2], ip[3],
			path);
	} else if (wes->addr.data.inet.has & WESHASIPv6) {
		ip = wes->addr.data.inet.ipv4;
		sprintf(url, "ftp://[%02X%02X:%02X%02X:%02X%02X:%02X%02X"
			":%02X%02X:%02X%02X:%02X%02X:%02X%02X]%.200s",
			ip[0], ip[1], ip[2], ip[3], ip[4], ip[5], ip[6], ip[7],
			ip[8], ip[9], ip[10], ip[11], ip[12], ip[13], ip[14], ip[15],
			path);
	} else
		return (wes_error_unknown);

	/* On ouvre le handle si nécessaire. */

	if (wes->ftp_handle)
		handle = wes->ftp_handle;
	else {
		handle = curl_easy_init();
		if (!handle)
			return (wes_error_unknown);

		wes->ftp_handle = handle;
	}

	/* On prépare le handle à ce qui l'attend. */

	curl_easy_setopt(handle, CURLOPT_URL, url);
	curl_easy_setopt(handle, CURLOPT_USERNAME, wes->ftp_name);
	curl_easy_setopt(handle, CURLOPT_PASSWORD, wes->ftp_pass);
	curl_easy_setopt(handle, CURLOPT_UPLOAD, 0L);
	curl_easy_setopt(handle, CURLOPT_VERBOSE, CURLFTPVERBOSE);
	curl_easy_setopt(handle, CURLOPT_QUOTE, NULL);
	curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, stdout);
	curl_easy_setopt(handle, CURLOPT_WRITEDATA, NULL);
	curl_easy_setopt(handle, CURLOPT_READFUNCTION, stdin);
	curl_easy_setopt(handle, CURLOPT_READDATA, NULL);

	*handlep = handle;
	return (wes_error_ok);
}
