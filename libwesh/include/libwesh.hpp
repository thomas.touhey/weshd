/* ****************************************************************************
 * libwesh.hpp -- bibliothèque d'interaction avec le WES Handling Daemon.
 * Copyright (C) 2018 Thomas Touhey <thomas@touhey.fr>
 *
 * This project is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the
 * CeCILL license as circulated by CEA, CNRS and INRIA at the
 * following URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the project's author, the holder of the
 * economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also therefore
 * means that it is reserved for developers and experienced professionals
 * having in-depth computer knowledge. Users are therefore encouraged to load
 * and test the software's suitability as regards their requirements in
 * conditions enabling the security of their systems and/or data to be
 * ensured and, more generally, to use and operate it in the same conditions
 * as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 * ************************************************************************* */
#ifndef  LIBWESH_HPP
# define LIBWESH_HPP 20180311
# include <stdexcept>
# include <ostream>
# include <string>
# include <list>
# include <chrono>
# include <ctime>
# define BEGIN_NAMESPACE_WESH namespace wesh {
# define   END_NAMESPACE_WESH }

BEGIN_NAMESPACE_WESH

class meter_base;
class wes_base;
class interface;

/* L'interface SunRPC n'est pas inclue publiquement, donc le type CLIENT
 * n'est pas disponible en dehors des fichiers d'en-tête internes.
 * Seulement, on en a besoin pour définir les classes, donc on dira que
 * c'est un pointeur opaque à l'utilisateur. */

# ifndef  WESH_CLIENT_TYPE
#  define WESH_CLIENT_TYPE void
# endif

/* ---
 * Définition des exceptions utilisables par la classe WES.
 * --- */

/* La différence entre `already_created_exception` et
 * `already_exists_exception` est la suivante :
 * - `already_created_exception`: l'objet a déjà été initialisé avec un
 *   identifiant valide, il faut donc créer une autre instance de `wes`
 *   pour pouvoir créer un objet.
 * - `already_exists_exception`: un objet correspondant aux données
 *   d'entrées existe déjà du côté du démon. */

# if __cplusplus >= 201103L
#  define WESH_NOEXCEPT noexcept
# else
#  define WESH_NOEXCEPT throw()
# endif

# define WESH_WHAT_IS_REAL \
	virtual char const *what() const WESH_NOEXCEPT = 0;
# define WESH_WHAT(MSG) \
	virtual char const *what() const WESH_NOEXCEPT { \
		return (MSG); \
	}

# define WESH_EXC(NAME, MSG) \
class NAME: public std::exception { \
	WESH_WHAT(MSG) \
};

WESH_EXC(invalid_argument,
	"L'un des arguments fournis était invalide.")

WESH_EXC(connexion_exception,
	"La connexion au démon a échoué.")
WESH_EXC(daemon_exception,
	"Le démon a connu une erreur interne.")
WESH_EXC(not_implemented_exception,
	"L'appel correspondant n'est pas implémenté.")

WESH_EXC(no_parent_exception,
	"L'objet requiert un parent.")

WESH_EXC(no_wes_exception,
	"Il n'y a pas de serveur WES avec cet identifiant.")
WESH_EXC(unavailable_wes_exception,
	"Le serveur WES est injoignable.")

WESH_EXC(no_meter_exception,
	"Il n'y a pas de compteur avec cet identifiant.")
WESH_EXC(invalid_meter_type_exception,
	"Le compteur distant n'est pas du bon type.")

WESH_EXC(no_sensor_exception,
	"Il n'y a pas de capteur avec cet identifiant.")

# undef WESH_EXC

/* ---
 * Définitions des compteurs.
 * --- */

/* Mode de consommation/production d'un compteur.
 * `conso`: consommation.
 * `prod`: production (panneaux solaires, …). */

enum meter_mode {
	mm_conso,
	mm_prod
};

/* Grandeur mesurée par le compteur.
 * `water`: eau froide/chaude.
 * `gas`: gaz.
 * `electricity`: electricité.
 * `fuel`: fioul. */

enum meter_what {
	mw_water,
	mw_gas,
	mw_elec,
	mw_fuel
};

/* Unité des valeurs pour la grandeur mesurée par le compteur.
 * `wh`: watts par heure (Wh).
 * `kwh`: kilo-watts par heure (kWh).
 * `liter`: litres (L).
 * `cubicmeter`: mètres cube (m³). */

enum meter_unit {
	mu_wh,
	mu_kwh,
	mu_liter,
	mu_cubicmeter
};

/* Type de tarif pour le protocole télé-informatique d'ERDF.
 * `all`:   tous forfaits.
 * `base`:  forfait Base (toutes heures), le prix du kWh ne varie pas en
 *          fonction de la période de consommation ;
 * `hchp`:  forfait Heures Pleines Heures Creuses, le prix du kWh varie
 *          en fonction de l'heure de la journée ;
 * `tempo`: forfait Temporaire, le prix du kWh varie en fonction de l'heure
 *          de la journée, ainsi que de la couleur de la journée ;
 * `ejp`:   forfait Effacement de Jour de Pointe, le prix du kWh varie en
 *          fonction de la journée. */

enum teleinfo_meter_subscription_type {
	ticmst_all,
	ticmst_base,
	ticmst_hchp,
	ticmst_ejp,
	ticmst_tempo
};

/* Période tarifaire pour le protocole télé-informatique d'ERDF.
 * La signification des valeurs dans cette énumération dépend du forfait
 * souscrit pour ce compteur.
 *
 * Tous forfaits ou forfait BASE :
 * `th`: toutes heures.
 *
 * Forfait HCHP :
 * `hc`: heures creuses.
 * `hp`: heures pleines.
 *
 * Forfait TEMPO :
 * `hcjb`: heures creuses en jours bleus.
 * `hpjb`: heures pleines en jours bleus.
 * `hcjw`: heures creuses en jours blancs.
 * `hpjw`: heures pleines en jours blancs.
 * `hcjr`: heures creuses en jours rouges.
 * `hpjr`: heures pleines en jours rouges.
 *
 * Forfait EJP :
 * `hn`: heures normales.
 * `pm`: pointes mobiles. */

enum teleinfo_meter_subscription_period {
	ticmsp_th   =  0,
	ticmsp_hn   =  1,
	ticmsp_hc   =  2,
	ticmsp_hp   =  3,
	ticmsp_pm   =  4,

	ticmsp_hcjb =  2,
	ticmsp_hpjb =  3,
	ticmsp_hcjw = 10,
	ticmsp_hpjw = 11,
	ticmsp_hcjr = 18,
	ticmsp_hpjr = 19
};

/* Structure concernant le forfait actuel d'un compteur électrique
 * branché via télé-information. */

struct teleinfo_meter_subscription {
	teleinfo_meter_subscription_type type;
	teleinfo_meter_subscription_period period;
};

/* Méthodes d'impulsions pour un compteur d'impulsions.
 * `ils`: impulsions mécaniques.
 * `elec`: impulsions électroniques. */

enum pulse_meter_method {
	pmm_ils,
	pmm_elec
};

/* Classe de base pour un compteur.
 * Les autres classes représentant des compteurs sont toutes héritées de
 * celle-ci. */

class meter_base {
protected:
	wes_base *parent;
	int id;

public:
	meter_base(void);
	meter_base(wes_base *parent, int num);
	meter_base(meter_base const &original);
	virtual ~meter_base(void);

	WESH_WHAT_IS_REAL

	void orphan(void);

	/* Récupération de données depuis le démon. */

	int get_id(void);
	WESH_CLIENT_TYPE *get_clnt(void);

	std::string get_name(void);
	void set_name(std::string name);
	void set_name(char const *name);

	bool enabled(void);
	void enable(void);
	void disable(void);

	meter_mode get_mode(void);
	void set_mode(meter_mode mode);

	meter_what get_what(void);
	void set_what(meter_what what);

	double get_fixed_costs(void);
	void set_fixed_costs(double cost);

	/* Récupération de données générique. */

	double get_count(int year, int month, int month_day);
};

/* Compteur particulier : pince ampèremétrique. */

class clamp: public meter_base {
public:
	clamp(void) : meter_base() {}
	clamp(wes_base *parent, int id) : meter_base(parent, id) {}
	clamp(clamp const &original) : meter_base(original) {}

	WESH_WHAT("clamp")

	int get_voltage(void);
	void set_voltage(int voltage);

	double get_unit_cost(void);
	void set_unit_cost(double cost);
};

/* Compteur d'impulsions. */

class pulse_meter: public meter_base {
public:
	pulse_meter(void) : meter_base() {}
	pulse_meter(wes_base *parent, int id) : meter_base(parent, id) {}
	pulse_meter(pulse_meter const &original) : meter_base(original) {}

	WESH_WHAT("pulse")

	pulse_meter_method get_method(void);
	void set_method(pulse_meter_method method);

	int get_pulses_per_unit(void);
	void set_pulses_per_unit(int number);

	double get_unit_cost(void);
	void set_unit_cost(double cost);

	/* Récupération de données. */

	double get_units(int year, int month, int mday);
};

/* Compteur relié via téléinfo. */

class teleinfo_meter: public meter_base {
public:
	teleinfo_meter(void) : meter_base() {}
	teleinfo_meter(wes_base *parent, int id) : meter_base(parent, id) {}
	teleinfo_meter(teleinfo_meter const &original) : meter_base(original) {}

	WESH_WHAT("teleinfo")

	double get_unit_cost(teleinfo_meter_subscription sub);
	void set_unit_cost(teleinfo_meter_subscription sub, double cost);

	bool bdpv_enabled(void);
	void enable_bdpv(void);
	void disable_bdpv(void);

	std::pair<int, int> is_sent_to_bdpv_at(void);
	void send_to_bdpv_at(int hour, int min);

	std::pair<std::string, std::string> get_bdpv_ids(void);
	void set_bdpv_ids(std::string username, std::string password);

	/* Récupération de données. */

	double get_kwh(int year, int month, int month_day,
		teleinfo_meter_subscription_type sub_type,
		teleinfo_meter_subscription_period sub_period);
};

/* ---
 * Définition des ressources de type capteur.
 * --- */

/* Classe de base pour un capteur. */

class sensor_base {
protected:
	wes_base *parent;
	int id;

public:
	sensor_base(void);
	sensor_base(wes_base *parent, int id);
	sensor_base(sensor_base const &original);
	virtual ~sensor_base(void);

	WESH_WHAT_IS_REAL

	void orphan(void);

	/* Récupération de données depuis le démon. */

	int get_id(void);
	WESH_CLIENT_TYPE *get_clnt(void);

	std::string get_name(void);
	void set_name(std::string name);
	void set_name(char const *name);

	double get_min(int year, int month, int month_day);
	double get_avg(int year, int month, int month_day);
	double get_max(int year, int month, int month_day);
};

/* Classe représentant un capteur relié par le bus 1-Wire. */

typedef unsigned char onewire_id_t[8];

class onewire_sensor: public sensor_base {
public:
	onewire_sensor(void) : sensor_base() {}
	onewire_sensor(wes_base *parent, int id) : sensor_base(parent, id) {}
	onewire_sensor(onewire_sensor const &orig) : sensor_base(orig) {}

	/* Récupération de données depuis le démon. */

	onewire_id_t get_onewire_id(void);
};

/* ---
 * Définition des ressources de type serveur WES.
 * --- */

/* Classe de base, pour toutes les interfaces derrière. */

class wes_base {
protected:
	interface *parent;
	int id;

	std::set<meter_base *> meter_list;
	std::set<sensor_base *> sensor_list;

public:
	wes_base(void);
	wes_base(interface *parent, int id);
	wes_base(wes_base const &original);
	virtual ~wes_base(void);

	WESH_WHAT_IS_REAL

	/* Manage the resource list. */

	void add_meter_ref(meter_base *resource);
	void del_meter_ref(meter_base *resource);

	void add_sensor_ref(sensor_base *resource);
	void del_sensor_ref(sensor_base *resource);

	void orphan(void);

	/* Getters et setters pour les propriétés communes. */

	int get_id(void);

	std::string get_name(void);
	void set_name(std::string name);
	void set_name(char const *name);

	int get_year(void);
	int get_month(void);
	int get_month_day(void);
	int get_hour(void);
	int get_min(void);
	int get_sec(void);
	int get_dow(void);
	bool get_dst(void);

	void set_year(int val);
	void set_month(int val);
	void set_month_day(int val);
	void set_hour(int val);
	void set_min(int val);
	void set_sec(int val);
	void set_dow(int val);
	void set_dst(bool val);

	/* Récupération de ressources.
	 * Le compteur peut être récupéré par son ID absolu ou le numéro des
	 * compteurs de ce type. */

	clamp get_clamp(int id);
	clamp get_clamp_no(int no);
	pulse_meter get_pulse_meter(int id);
	pulse_meter get_pulse_meter_no(int no);
	teleinfo_meter get_teleinfo_meter(int id);
	teleinfo_meter get_teleinfo_meter_no(int no);

	onewire_sensor get_onewire_sensor(int id);
	onewire_sensor get_onewire_sensor_no(int no);

	/* Destiné aux classes ayant cette interface comme référent. */

	WESH_CLIENT_TYPE *get_clnt(void);
};

/* Classe fille pour le serveur WES de base connecté via IP. */

class wes: public wes_base {
public:
	wes(void) : wes_base() {}
	wes(interface *parent, int id) : wes_base(parent, id) {}
	wes(wes const &original) : wes_base(original) {}

	WESH_WHAT("ip")
};

/* Classe fille pour le serveur WES factice. */

class dummy_wes: public wes_base {
public:
	dummy_wes(void) : wes_base() {}
	dummy_wes(interface *parent, int id) : wes_base(parent, id) {}
	dummy_wes(dummy_wes const &original) : wes_base(original) {}

	WESH_WHAT("dummy")
};

/* ---
 * Définition de l'interface.
 * --- */

class interface {
private:
	WESH_CLIENT_TYPE *client;
	std::set<wes_base *> wes_list;

public:
	interface(void);
	~interface(void);

	/* Manage the WES list. */

	void add_wes_ref(wes_base *resource);
	void del_wes_ref(wes_base *resource);

	/* Récupérer un WES connecté par IP. */

	wes get_wes(unsigned char const ip[4]);
	wes get_wes(char const *ip);
	wes get_wes(std::string const ip);

	wes get_wes(unsigned char const ip[4],
		char const *username, char const *password);
	wes get_wes(unsigned char const ip[4],
		std::string const username, std::string const password);
	wes get_wes(char const *ip,
		char const *username, char const *password);
	wes get_wes(char const *ip,
		std::string const username, std::string const password);
	wes get_wes(std::string const ip,
		char const *username, char const *password);
	wes get_wes(std::string const ip,
		std::string const username, std::string const password);

	wes get_wes(unsigned char const ip[4],
		char const *http_username, char const *http_password,
		char const *ftp_username, char const *ftp_password);
	wes get_wes(unsigned char const ip[4],
		std::string const http_username, std::string const http_password,
		std::string const ftp_username, std::string const ftp_password);
	wes get_wes(char const *ip,
		char const *http_username, char const *http_password,
		char const *ftp_username, char const *ftp_password);
	wes get_wes(char const *ip,
		std::string const http_username, std::string const http_password,
		std::string const ftp_username, std::string const ftp_password);
	wes get_wes(std::string const ip,
		char const *http_username, char const *http_password,
		char const *ftp_username, char const *ftp_password);
	wes get_wes(std::string const ip,
		std::string const http_username, std::string const http_password,
		std::string const ftp_username, std::string const ftp_password);

	/* Récupérer un WES factice. */

	dummy_wes get_dummy_wes(void);

	/* Supprimer un WES. */

	void remove(int id);
	void remove(wes_base const &resource);
	void remove(wes_base const *resource);

	/* Destiné aux classes ayant cette interface comme référent. */

	WESH_CLIENT_TYPE *get_clnt(void);
};

END_NAMESPACE_WESH

/* Quelques utilitaires hors du namespace pour afficher ce qu'il faut
 * de façon relativement agréable. */

extern std::ostream& operator<<(std::ostream& os,
	wesh::wes_base& wes);

extern std::ostream& operator<<(std::ostream& os,
	wesh::meter_mode md);
extern std::ostream& operator<<(std::ostream& os,
	wesh::meter_what wh);
extern std::ostream& operator<<(std::ostream& os,
	wesh::meter_unit un);
extern std::ostream& operator<<(std::ostream& os,
	wesh::teleinfo_meter_subscription_type st);
extern std::ostream& operator<<(std::ostream& os,
	wesh::teleinfo_meter_subscription_period pd);
extern std::ostream& operator<<(std::ostream& os,
	wesh::teleinfo_meter_subscription sb);
extern std::ostream& operator<<(std::ostream& os,
	wesh::pulse_meter_method pl);

#endif /* LIBWESH_HPP */
