/* ****************************************************************************
 * wes.cpp -- méthodes de la classe `wes_base` et dérivées.
 * Copyright (C) 2018 Thomas Touhey <thomas@touhey.fr>
 *
 * This project is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the
 * CeCILL license as circulated by CEA, CNRS and INRIA at the
 * following URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the project's author, the holder of the
 * economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also therefore
 * means that it is reserved for developers and experienced professionals
 * having in-depth computer knowledge. Users are therefore encouraged to load
 * and test the software's suitability as regards their requirements in
 * conditions enabling the security of their systems and/or data to be
 * ensured and, more generally, to use and operate it in the same conditions
 * as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 * ************************************************************************* */
#include "internals.hpp"

using namespace wesh;

/* ---
 * Constructeur et destructeur de la classe de base.
 * --- */

wes_base::wes_base(interface *given_parent, int given_id)
{
	if (given_id <= 0 || given_id > 32766 || !given_parent)
		throw no_wes_exception();

	given_parent->add_wes_ref(this);

	parent = given_parent;
	id = given_id;
}

wes_base::wes_base(wes_base const &original)
{
	/* Les données ont déjà été copiées. */

	if (parent)
		parent->add_wes_ref(this);
}

wes_base::~wes_base(void)
{
	/* Prévenons le parent pour ne pas qu'il nous informe quand il meurt,
	 * puisque nous serons morts nous-mêmes. */

	if (parent)
		parent->del_wes_ref(this);

	/* Prévenons également les enfants qu'ils sont orphelins avant de
	 * mourir. */

	for (wes_base *p : meter_list)
		p->orphan();
	for (sensor_base *p : sensor_list)
		p->orphan();
}

int get_id(void)
{
	return (id);
}

CLIENT *wes_base::get_clnt(void)
{
	return (parent->get_clnt());
}

void wes_base::add_meter_ref(wes_base *resource)
{
	if (parent)
		meter_list.insert(resource);
}

void wes_base::del_meter_ref(wes_base *resource)
{
	auto it = meter_list.find(resource);
	if (resource != std::set::end)
		meter_list.erase(resource);
}

void wes_base::add_sensor_ref(wes_base *resource)
{
	if (parent)
		sensor_list.insert(resource);
}

void wes_base::del_sensor_ref(wes_base *resource)
{
	auto it = sensor_list.find(resource);
	if (resource != std::set::end)
		sensor_list.erase(resource);
}

void wes_base::orphan(void)
{
	if (!parent)
		return ;
	parent = nullptr;
}

/* ---
 * Récupération et définition du nom.
 * --- */

std::string wes_base::get_name(void)
{
	wespid_with_wes_flags_t args;
	wespret_with_wes_t *resp;

	args.id = static_cast<wespid_t>(this->id);
	args.to_get = WESP_NAME;
	resp = get_1(&args, get_clnt());
	if (!resp)
		throw connexion_exception();

	if (resp->ret != WESPRET_OK)
		throw_exception_using_ret(resp->ret);

	return (std::string(resp->wes.name));
}

void wes_base::set_name(char const *name_to_set)
{
	wespid_with_wes_t args;
	wespret_t ret, *retp;
	char namebuf[33];
	const char *nm;
	size_t len;

	nm = static_cast<const char*>(memchr(name_to_set, '\0', 33));
	len = nm ? static_cast<size_t>(nm - name_to_set) : 32;
	memcpy(namebuf, name_to_set, len);
	namebuf[len] = '\0';

	args.id = static_cast<wespid_t>(this->id);
	args.wes.name = namebuf;
	args.wes.to_set = WESP_NAME;

	retp = set_1(&args, get_clnt());
	if (!retp)
		throw connexion_exception();

	ret = *retp;
	if (ret != WESPRET_OK)
		throw_exception_using_ret(ret);
}

void wes_base::set_name(std::string name_to_set)
{
	set_name(name_to_set.c_str());
}

/* ---
 * Récupération de la date.
 * --- */

struct tm wes_base::get_date(void)
{
	wespid_with_wes_flags_t args;
	wespret_with_wes_t *resp;
	wespdt_t *dt;
	struct tm tm;

	args.id = static_cast<wespid_t>(this->id);
	args.to_get = WESP_TIME;
	resp = get_1(&args, get_clnt());
	if (!resp)
		throw connexion_exception();

	if (resp->ret != WESPRET_OK)
		throw_exception_using_ret(resp->ret);

	dt = &resp->wes.current_time;
	tm.tm_year = dt->year - 1900;
	tm.tm_mon = dt->mon - 1;
	tm.tm_mday = dt->dom;
	tm.tm_hour = dt->hour;
	tm.tm_min = dt->min;
	tm.tm_sec = dt->sec;

	mktime(&tm);
	return (tm);
}

/* ---
 * Récupération des compteurs.
 * --- */

#if 0
/* Code provenant de l'ancienne méthode `gather()` de `meter_base`.
 * TODO: la réutiliser? */

void meter_base::gather(void)
{
	wespid_with_meter_id_t args = {
		static_cast<wespid_t>(this->parent->get_id()),
		this->num};
	wespret_with_tic_t *resp;

	if (this->num < 0)
		throw not_loaded_exception();

	/* Requête au démon. */

	resp = get_tic_1(&args, clnt(this->parent));
	if (!resp)
		throw connexion_exception();

	/* TODO: other values */
}
#endif

clamp wes_base::get_clamp(int cl_id)
{
	clamp cl(this, cl_id);

	return (cl);
}

clamp wes_base::get_clamp_no(int cl_no)
{
	wespid_with_meter_query_t args;
	wespret_with_meter_list_t *resp;
	int cl_id;



	return (clamp(this, cl_id));
}

pulse_meter wes_base::get_pulse_meter(int pl_id)
{
	pulse_meter pm(this, pl_id);

	/* TODO: préférer la recherche */

	return (pm);
}

teleinfo_meter wes_base::get_teleinfo_meter(int tic_id)
{
	teleinfo_meter tic(this, tic_id);

	/* TODO: préférer la recherche */

	return (tic);
}
