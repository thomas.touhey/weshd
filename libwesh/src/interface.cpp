/* ****************************************************************************
 * interface.cpp -- interface principale de la libwesh.
 * Copyright (C) 2018 Thomas Touhey <thomas@touhey.fr>
 *
 * This project is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the
 * CeCILL license as circulated by CEA, CNRS and INRIA at the
 * following URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the project's author, the holder of the
 * economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also therefore
 * means that it is reserved for developers and experienced professionals
 * having in-depth computer knowledge. Users are therefore encouraged to load
 * and test the software's suitability as regards their requirements in
 * conditions enabling the security of their systems and/or data to be
 * ensured and, more generally, to use and operate it in the same conditions
 * as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 * ************************************************************************* */
#include "internals.hpp"

using namespace wesh;

/* ---
 * Constructeur, destructeur et méthodes de base.
 * --- */

interface::interface(void)
{
	client = nullptr;
}

interface::~interface(void)
{
	if (client)
		clnt_destroy(client);
	for (wes_base *w : wes_list)
		w->orphan();
}

CLIENT *interface::get_clnt(void)
{
	if (client)
		return (client);

	client = clnt_create("localhost", WESHD_PROG, WESHD_VERS1, "tcp");
	if (!client)
		throw connexion_exception();

	return (client);
}

void add_wes_ref(wes_base *resource)
{
	wes_list.insert(resource);
}

void del_wes_ref(wes_base *resource)
{
	auto it = wes_list.find(resource);
	if (resource != std::set::end)
		wes_list.erase(resource);
}

/* ---
 * Récupération d'un WES connecté par IP.
 * --- */

wes interface::get_wes(unsigned char const ip[4],
	char const *http_username, char const *http_password,
	char const *ftp_username, char const *ftp_password)
{
	wespoptions_t args;
	wespret_with_id_t *resp;
	wespret_t ret;

	args.type = WESPTYPE_IP;
	args.wespoptions_t_u.ip.addr.type = WESPIPTYPE_4;
	args.wespoptions_t_u.ip.addr.wespip_t_u.addr4[0] = ip[0];
	args.wespoptions_t_u.ip.addr.wespip_t_u.addr4[1] = ip[1];
	args.wespoptions_t_u.ip.addr.wespip_t_u.addr4[2] = ip[2];
	args.wespoptions_t_u.ip.addr.wespip_t_u.addr4[3] = ip[3];
	args.wespoptions_t_u.ip.http_username = const_cast<char *>(http_username);
	args.wespoptions_t_u.ip.http_password = const_cast<char *>(http_password);
	args.wespoptions_t_u.ip.ftp_username = const_cast<char *>(ftp_username);
	args.wespoptions_t_u.ip.ftp_password = const_cast<char *>(ftp_password);

	resp = gather_1(&args, get_clnt());
	if (!resp)
		throw connexion_exception();

	ret = resp->ret;
	if (ret != WESPRET_OK)
		throw_exception_using_ret(ret);

	return (wes(this, static_cast<int>(resp->id)));
}

wes interface::get_wes(unsigned char const ip[4],
	std::string const http_username, std::string const http_password,
	std::string const ftp_username, std::string const ftp_password)
{
	return (get_wes(ip, http_username.c_str(), http_password.c_str(),
		ftp_username.c_str(), ftp_password.c_str()));
}

wes interface::get_wes(char const *ip_string,
	char const *http_username, char const *http_password,
	char const *ftp_username, char const *ftp_password)
{
	unsigned char ip[4];

	if (decode_ipv4(ip_string, ip))
		throw invalid_argument();
	return (get_wes(ip, http_username, http_password,
		ftp_username, ftp_password));
}

wes interface::get_wes(char const *ip_string,
	std::string const http_username, std::string const http_password,
	std::string const ftp_username, std::string const ftp_password)
{
	return (get_wes(ip_string, http_username.c_str(), http_password.c_str(),
		ftp_username.c_str(), ftp_password.c_str()));
}

wes interface::get_wes(std::string const ip,
	char const *http_username, char const *http_password,
	char const *ftp_username, char const *ftp_password)
{
	return (get_wes(ip.c_str(), http_username, http_password,
		ftp_username, ftp_password));
}

wes interface::get_wes(std::string const ip,
	std::string const http_username, std::string const http_password,
	std::string const ftp_username, std::string const ftp_password)
{
	return (get_wes(ip, http_username.c_str(), http_password.c_str(),
		ftp_username.c_str(), ftp_password.c_str()));
}

wes interface::get_wes(unsigned char const ip[4],
	char const *username, char const *password)
{
	return (get_wes(ip, username, password, username, password));
}

wes interface::get_wes(unsigned char const ip[4],
	std::string const username, std::string const password)
{
	return (get_wes(ip, username.c_str(), password.c_str()));
}

wes interface::get_wes(char const *ip_string,
	char const *username, char const *password)
{
	unsigned char ip[4];

	if (decode_ipv4(ip_string, ip))
		throw invalid_argument();
	return (get_wes(ip, username, password));
}

wes interface::get_wes(char const *ip_string,
	std::string const username, std::string const password)
{
	return (get_wes(ip_string, username.c_str(), password.c_str()));
}

wes interface::get_wes(std::string const ip,
	char const *username, char const *password)
{
	return (get_wes(ip.c_str(), username, password));
}

wes interface::get_wes(std::string const ip,
	std::string const username, std::string const password)
{
	return (get_wes(ip, username.c_str(), password.c_str()));
}

wes interface::get_wes(unsigned char const ip[4])
{
	return (get_wes(ip, "admin", "wes", "adminftp", "wesftp"));
}

wes interface::get_wes(char const *ip_string)
{
	unsigned char ip[4];

	if (decode_ipv4(ip_string, ip))
		throw invalid_argument();
	return (get_wes(ip));
}

wes interface::get_wes(std::string const ip)
{
	return (get_wes(ip.c_str()));
}

/* ---
 * Récupération d'un WES factice.
 * --- */

dummy_wes interface::get_dummy_wes(void)
{
	wespoptions_t args;
	wespret_with_id_t *resp;
	wespret_t ret;

	args.type = WESPTYPE_DUMMY;

	resp = gather_1(&args, get_clnt());
	if (!resp)
		throw connexion_exception();

	ret = resp->ret;
	if (ret != WESPRET_OK)
		throw_exception_using_ret(ret);

	return (dummy_wes(this, static_cast<int>(resp->id)));
}

/* ---
 * Suppression d'une ressource de type serveur WES.
 * --- */

void interface::remove(int given_id)
{
	wespid_t id;
	wespret_t *ret;

	id = static_cast<wespid_t>(given_id);
	ret = unregister_1(&id, get_clnt());
	if (!ret)
		throw connexion_exception();

	if (*ret != WESPRET_OK)
		throw_exception_using_ret(*ret);
}

void interface::remove(wes_base const &resource)
{
	remove(resource.get_id());
}

void interface::remove(wes_base const *resource)
{
	if (!resource)
		return ;
	remove(resource->get_id());
}
