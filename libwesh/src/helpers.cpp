/* ****************************************************************************
 * helpers.cpp -- utilitaires pour la libwesh.
 * Copyright (C) 2018 Thomas Touhey <thomas@touhey.fr>
 *
 * This project is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the
 * CeCILL license as circulated by CEA, CNRS and INRIA at the
 * following URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the project's author, the holder of the
 * economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also therefore
 * means that it is reserved for developers and experienced professionals
 * having in-depth computer knowledge. Users are therefore encouraged to load
 * and test the software's suitability as regards their requirements in
 * conditions enabling the security of their systems and/or data to be
 * ensured and, more generally, to use and operate it in the same conditions
 * as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 * ************************************************************************* */
#include "internals.hpp"

using namespace wesh;

/* ---
 * Utilitaires par rapport aux adresses IP.
 * --- */

/* `decode_ipv4()`: décodage d'une IPv4 à partir d'une chaîne de caractères,
 * avec des extensions cool comme :
 *
 * - si on ne précise pas tous les champs, ça extrapole, e.g.
 *   192.168.4 -> 192.168.0.4.
 * - on peut utiliser `0` et `0o` pour préciser un numéro en octal,
 *   e.g. 192.0250.4 -> 192.168.0.4
 * - on peut utiliser `0x` pour préciser un numéro en hexadécimal,
 *   e.g. 0xc0.0o250.4 -> 192.168.0.4. */

int wesh::decode_ipv4(char const *s, unsigned char ip[4])
{
	/* mode: 10 pour décimal, 8 pour octal, 16 pour hexadécimal
	 * oct: si 'o' a déjà été lu dans le préfixe.
	 * started: si le décodage des données d'un nombre a commencé. */

	int mode = 10;
	int oct = 0;
	int started = 0;
	int i;

	ip[0] = 0;
	ip[1] = 0;
	ip[2] = 0;
	ip[3] = 0;

	for (i = 0; *s; s++) switch (*s) {
		case '0':
			if (started)
				ip[i] = static_cast<unsigned char>(ip[i] * mode);
			else if (mode == 10)
				mode = 8;
			else
				started = 1;
			break;

		case '1': case '2': case '3':
		case '4': case '5': case '6':
		case '7':
			ip[i] = static_cast<unsigned char>(ip[i] * mode + (*s - '0'));
			started = 1;
			break;

		case '8': case '9':
			if (mode == 8)
				return (1);
			ip[i] = static_cast<unsigned char>(ip[i] * mode + (*s - '0'));
			started = 1;
			break;

		case 'A': case 'B': case 'C':
		case 'D': case 'E': case 'F':
			if (mode != 16)
				return (1);
			ip[i] = static_cast<unsigned char>(ip[i] * mode + (*s - 'A' + 10));
			started = 1;
			break;

		case 'a': case 'b': case 'c':
		case 'd': case 'e': case 'f':
			if (mode != 16)
				return (1);
			ip[i] = static_cast<unsigned char>(ip[i] * mode + (*s - 'a' + 10));
			started = 1;
			break;

		case 'o':
			if (mode != 8 || started || oct)
				return (1);
			oct = 1;
			break;

		case 'x':
			if (started || mode != 8)
				return (1);
			mode = 16;
			break;

		case '.':
			if (++i >= 4)
				return (1);
			mode = 10;
			started = 0;
			oct = 0;
			break;

		default:
			return (1);
	}

	if (i < 3) {
		ip[3] = ip[i];
		ip[i] = 0;
	}

	return (0);
}

/* ---
 * Utilitaires concernant les exceptions.
 * --- */

/* `throw_exception_using_ret()`: utilisation du code de retour pour savoir
 * quelle exception soulever. */

void wesh::throw_exception_using_ret(wespret_t ret)
{
	switch (ret) {
	case WESPRET_OK:
		break;
	case WESPRET_IMP:
		throw not_implemented_exception();
	case WESPRET_NOW:
		throw no_exists_exception();
	case WESPRET_CON:
		throw unavailable_exception();
	default:
		throw daemon_exception();
	}

	throw std::exception();
}

/* ---
 * Utilitaires d'affichage (mise sur un flux de sortie).
 * --- */

std::ostream& operator<<(std::ostream& os, wes_base& wes)
{
	os << "<wes"
		<< " name=\"" << wes.get_name() << "\""
		<< " iface=\"" << wes.what() << "\""
		<< ">";

	return (os);
}

std::ostream& operator<<(std::ostream& os, meter_mode md)
{
	switch (md) {
	case mm_conso:
		os << "CONSO";
		break;
	case mm_prod:
		os << "PROD";
		break;
	default:
		os << "(unknown)";
		break;
	}

	return (os);
}

std::ostream& operator<<(std::ostream& os, meter_what wh)
{
	switch (wh) {
	case mw_water:
		os << "Eau";
		break;
	case mw_gas:
		os << "Gaz";
		break;
	case mw_elec:
		os << "Électricité";
		break;
	case mw_fuel:
		os << "Fioul";
		break;
	default:
		os << "(unknown)";
		break;
	}

	return (os);
}

std::ostream& operator<<(std::ostream& os, meter_unit un)
{
	switch (un) {
	case mu_wh:
		os << "Wh";
		break;
	case mu_kwh:
		os << "kWh";
		break;
	case mu_liter:
		os << "L";
		break;
	case mu_cubicmeter:
		os << "m³";
		break;
	default:
		os << "(unknown)";
		break;
	}

	return (os);
}

std::ostream& operator<<(std::ostream& os,
	teleinfo_meter_subscription_type st)
{
	switch (st) {
	case ticmst_all:
		os << "TOUS";
		break;
	case ticmst_base:
		os << "BASE";
		break;
	case ticmst_hchp:
		os << "HCHP";
		break;
	case ticmst_ejp:
		os << "EJP";
		break;
	case ticmst_tempo:
		os << "TEMPO";
		break;
	default:
		os << "(unknown)";
		break;
	}

	return (os);
}

std::ostream& operator<<(std::ostream& os,
	teleinfo_meter_subscription_period pd)
{
	switch (pd) {
	case ticmsp_th:
		os << "TH";
		break;
	case ticmsp_hn:
		os << "HN";
		break;
	case ticmsp_hc:
		os << "HC(JB)";
		break;
	case ticmsp_hp:
		os << "HP(JB)";
		break;
	case ticmsp_pm:
		os << "PM";
		break;
	case ticmsp_hcjw:
		os << "HCJW";
		break;
	case ticmsp_hpjw:
		os << "HPJW";
		break;
	case ticmsp_hcjr:
		os << "HCJR";
		break;
	case ticmsp_hpjr:
		os << "HPJR";
		break;
	default:
		os << "(unknown)";
		break;
	}

	return (os);
}

std::ostream& operator<<(std::ostream& os,
	teleinfo_meter_subscription sb)
{
	switch (sb.type) {
	case ticmst_base: switch (sb.period) {
		case ticmsp_hn:
			os << "BASE";
			break;
		default:
			os << "(unknown)";
		} break;
	case ticmst_hchp: switch (sb.period) {
		case ticmsp_hc:
			os << "HCHC";
			break;
		case ticmsp_hp:
			os << "HCHP";
			break;
		default:
			os << "(unknown)";
		} break;
	case ticmst_ejp: switch (sb.period) {
		case ticmsp_hn:
			os << "EJPHN";
			break;
		case ticmsp_pm:
			os << "EJPPM";
			break;
		default:
			os << "(unknown)";
		} break;
	case ticmst_tempo: switch (sb.period) {
		case ticmsp_hcjb:
			os << "HCJB";
			break;
		case ticmsp_hpjb:
			os << "HPJB";
			break;
		case ticmsp_hcjw:
			os << "HCJW";
			break;
		case ticmsp_hpjw:
			os << "HPJW";
			break;
		case ticmsp_hcjr:
			os << "HCJR";
			break;
		case ticmsp_hpjr:
			os << "HPJR";
			break;
		default:
			os << "(unknown)";
		} break;
	default:
		os << "(unknown)";
	}

	return (os);
}

std::ostream& operator<<(std::ostream& os, pulse_meter_method pl)
{
	switch (pl) {
	case pmm_ils:
		os << "ILS (mécanique)";
		break;
	case pmm_elec:
		os << "Électronique";
		break;
	default:
		os << "(unknown)";
		break;
	}

	return (os);
}
