/* ****************************************************************************
 * sensor.cpp -- méthodes de la classe `sensor_base` et dérivées.
 * Copyright (C) 2018 Thomas Touhey <thomas@touhey.fr>
 *
 * This project is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the
 * CeCILL license as circulated by CEA, CNRS and INRIA at the
 * following URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the project's author, the holder of the
 * economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also therefore
 * means that it is reserved for developers and experienced professionals
 * having in-depth computer knowledge. Users are therefore encouraged to load
 * and test the software's suitability as regards their requirements in
 * conditions enabling the security of their systems and/or data to be
 * ensured and, more generally, to use and operate it in the same conditions
 * as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 * ************************************************************************* */
#include "internals.hpp"

using namespace wesh;

/* ---
 * Constructeurs, destructeur, méthodes de base.
 * --- */

sensor_base::sensor_base(wes_base *given_parent, int given_id)
{
	if (id_to_use <= 0 || id_to_use > 32766 || !given_parent)
		throw no_sensor_exception();

	given_parent->add_sensor_ref(this);

	parent = given_parent;
	id = given_id;
}

sensor_base::sensor_base(sensor_base const &original)
{
	/* Les données ont déjà été copiées. */

	if (parent)
		parent->add_sensor_ref(this);
}

sensor_base::~sensor_base(void)
{
	if (parent)
		given_parent->del_sensor_ref(this);
}

void sensor_base::orphan(void)
{
	parent = nullptr;
}

int sensor_base::get_id(void)
{
	return (id);
}

CLIENT *sensor_base::get_clnt(void)
{
	if (!parent)
		throw no_parent_exception();
	return (parent->get_clnt());
}

/* TODO: getters, setters */
