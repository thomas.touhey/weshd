/* ****************************************************************************
 * args.cpp -- interface en ligne de commande, décodage des arguments.
 * Copyright (C) 2018 Thomas Touhey <thomas@touhey.fr>
 *
 * This project is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the
 * CeCILL license as circulated by CEA, CNRS and INRIA at the
 * following URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the project's author, the holder of the
 * economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also therefore
 * means that it is reserved for developers and experienced professionals
 * having in-depth computer knowledge. Users are therefore encouraged to load
 * and test the software's suitability as regards their requirements in
 * conditions enabling the security of their systems and/or data to be
 * ensured and, more generally, to use and operate it in the same conditions
 * as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 * ************************************************************************* */
#include "internals.hpp"
#include <boost/program_options.hpp>

namespace po = boost::program_options;

/* ---
 * Décodage des arguments de la ligne de commande.
 * --- */

int parse_args(int ac, const char *av[], args_t &args)
{
	po::variables_map vm;
	po::options_description opts{"Options"};
	po::positional_options_description params;

	args.menu = MENU_QUIT;

	/* Ajout des paramètres et options. */

	opts.add_options()
		("help,h", "display this help message")
		("version,v", "display the version message");

	/* Décodage et interprétation de la ligne de commande. */

	try {
		po::store(po::command_line_parser(ac, av)
			.options(opts).positional(params).run(), vm);
		po::notify(vm);
	} catch (const std::exception &ex) {
		std::cerr << "weshclient: " << ex.what() << std::endl;
		return (1);
	}

	if (vm.count("version")) {
		std::cerr << "weshclient v" WESHD_VERSION
			<< " (licensed under CeCILL 2.1)" << std::endl
			<< "Maintained by " << WESHD_MAINT << "." << std::endl << std::endl
			<< "This is free software; see he source for copying conditions."
			<< std::endl << "There is NO warranty; not even for "
			<< "MERCHANTABILITY" << std::endl << "or FITNESS FOR A "
			<< "PARTICULAR PURPOSE." << std::endl;

		return (0);
	}

	if (vm.count("help")) {
		std::cerr << "syntax: weshclient [OPTIONS..]" << std::endl << std::endl
			<< opts << std::endl
			<< "Report bugs to " WESHD_MAINT "." << std::endl;

		return (0);
	}

	/* TODO */

	args.menu = MENU_TEST;

	return (0);
}
