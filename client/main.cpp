/* ****************************************************************************
 * main.cpp -- fonction principale du client par défaut de weshd.
 * Copyright (C) 2018 Thomas Touhey <thomas@touhey.fr>
 *
 * This project is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the
 * CeCILL license as circulated by CEA, CNRS and INRIA at the
 * following URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the project's author, the holder of the
 * economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also therefore
 * means that it is reserved for developers and experienced professionals
 * having in-depth computer knowledge. Users are therefore encouraged to load
 * and test the software's suitability as regards their requirements in
 * conditions enabling the security of their systems and/or data to be
 * ensured and, more generally, to use and operate it in the same conditions
 * as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 * ************************************************************************* */
#include "internals.hpp"

static void test_things(void)
{
	wesh::interface iface;
	wesh::dummy_wes wes = iface.get_dummy_wes();

	wes.set_name("hello-world");
	std::cout << wes << std::endl;

	iface.remove(wes);
}

/* ---
 * Point d'entrée du programme.
 * --- */

int main(int ac, const char *av[])
{
	args_t args;

	if (parse_args(ac, av, args))
		return (1);

	switch (args.menu) {
	case MENU_QUIT:
		break;

	case MENU_TEST:
		test_things();
		break;
	}

	return (0);
}
