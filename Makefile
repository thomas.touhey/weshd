#!/usr/bin/make -f
include Makefile.vars Makefile.msg

# ---
# Targets générales.
# ---
# Une target pour tout construire.

all: all-libw all-daemon all-lib all-client

# Une target pour tout nettoyer.

clean:
	$(call rmsg,Removing built files.)
	$(call qcmd,$(RM) -r build)
	$(call qcmd,$(RM) common/weshd.h testclient)

# Une target pour remettre à zéro.

distclean: clean
	$(call rmsg,Removing configuration and tarballs.)
	$(call qcmd,$(RM) Makefile.cfg)
	$(call qcmd,$(RM) $(NAME)-*.tar*)

 mrproper: distclean

# Une target pour tout refaire.

re: clean all

# Installation de tout.

install: install-libw install-daemon install-lib install-client

# Une target pour ne rien faire, histoire de tester des choses avec
# des `$(info <contenu>)` principalement.

 nothing:

.PHONY: all clean distclean mrproper re nothing

# ---
# Dépendances de vérifications de la configuration.
# ---
# Définissons les dépendances.

 CHECKCFG := $(if $(shell test -f Makefile.cfg || echo y),check-config, \
	$(if $(shell [ "$(VERSION)" = "$(CONFIG_VERSION)" ] || echo y), \
	check-config-version))

# Définissons les targets.

 check-config:
	@echo -e "\033[1;31mNo configuration file found!"
	@echo -e "You should configure before re-running this target.\033[0m"
	@false
 check-config-version:
	@echo -e "\033[1;31mConfiguration version is incorrect!"
	@echo -e "You should re-configure before re-running this target.\033[0m"
	@false

.PHONY: check-config check-config-version

# ---
# Récupération d'informations depuis le Makefile.
# ---

 getname:
	@echo $(NAME)
 getversion:
	@echo $(VERSION)
 getmaintainer:
	@echo "$(MAINTAINER)"

 getlibdir: $(CHECKCFG)
	@echo "$(OLIBDIR)"
 getincdir: $(CHECKCFG)
	@echo "$(OINCDIR)"

.PHONY: getname getversion getmaintainer getlibdir getincdir

# ---
# Création des dossiers.
# ---
# Dossiers de construction.
# XXX: Temporaire, merci à moi du futur de refaire ça proprement un jour.

 ./build/ ./build/rpc/ ./build/daemon/ ./build/libw/ \
 ./build/lib/ ./build/lib/wes/ ./build/lib/meter/ \
 ./build/daemon/ip/ ./build/daemon/dummy/ ./build/client/:
	$(call bcmd,mkdir,$@,$(MD) "$@")

# Dossiers d'installation.

 $(IBINDIR) $(ILIBDIR) $(IPKGDIR) \
 $(sort $(foreach x,$(L_INCp),$(dir $(IINCDIR)/lib$(LIB)-$(VERSION)/$(x)))) \
 $(sort $(foreach x,$(W_INCp),$(dir $(IINCDIR)/lib$(LIBW)-$(VERSION)/$(x)))):
	$(call qcmd,$(MD) "$@")

# ---
# Définition des fichiers générés par `rpcgen`.
# Ces targets ne sont destinées qu'à être mises en dépendances.
# ---

# Copie du fichier dans le dossier de construction.

 $(S_OBJDIR)/$(S_NAME).x: $(S_FROM) | $(S_OBJDIR)/
	$(call bcmd,cp,$(S_NAME).x,$(CP) "$<" "$@")

# Création du header et des sources.

 $(S_OBJDIR)/$(S_NAME).h: $(S_OBJDIR)/$(S_NAME).x | $(S_OBJDIR)/
	$(call bcmd,rpcgen -h,$(S_NAME).h,cd $(S_OBJDIR) \
		&& $(RM) $(S_NAME).h \
		&& $(RPCGEN) -C -h -o $(S_NAME).h $(S_NAME).x)

 $(S_OBJDIR)/$(S_NAME)_svc.c: $(S_OBJDIR)/$(S_NAME).x | $(S_OBJDIR)/
	$(call bcmd,rpcgen -m,$(S_NAME)_svc.c,cd $(S_OBJDIR) \
		&& $(RM) $(S_NAME)_svc.c \
		&& $(RPCGEN) -C -m -o $(S_NAME)_svc.c $(S_NAME).x)

 $(S_OBJDIR)/$(S_NAME)_xdr.c: $(S_OBJDIR)/$(S_NAME).x | $(S_OBJDIR)/
	$(call bcmd,rpcgen -c,$(S_NAME)_xdr.c,cd $(S_OBJDIR) \
		&& $(RM) $(S_NAME)_xdr.c \
		&& $(RPCGEN) -C -c -o $(S_NAME)_xdr.c $(S_NAME).x)

 $(S_OBJDIR)/$(S_NAME)_clnt.c: $(S_OBJDIR)/$(S_NAME).x | $(S_OBJDIR)/
	$(call bcmd,rpcgen -l,$(S_NAME)_clnt.c,cd $(S_OBJDIR) \
		&& $(RM) $(S_NAME)_clnt.c \
		&& $(RPCGEN) -C -l -o $(S_NAME)_clnt.c $(S_NAME).x)

# Création des objets.

 $(S_OBJDIR)/%.c.o: $(S_OBJDIR)/%.c $(S_OBJDIR)/$(S_NAME).h | $(S_OBJDIR)/
	$(call bcmd,cc,$@,$(CC) -c -o $@ $< $(S_CFLAGS))

# ---
# Définition des targets pour la libwes.
# ---
# Création de la bibliothèque.

 all-libw: $(CHECKCFG) $(W_DEST)

 $(W_DEST): $(W_DEST).$(MAJOR) | $(dir $(W_DEST))
	$(call bcmd,ln,$@,$(LN) $(notdir $(W_DEST).$(MAJOR)) $(W_DEST))
 $(W_DEST).$(MAJOR): $(W_OBJ) | $(dir $(W_DEST))
	$(call bcmd,ld,$@,$(LD) -o $@ $(W_OBJ) $(W_LDFLAGS))

define make-lib-obj-rule
 $(W_OBJDIR)/$1.c.o: $(W_SRCDIR)/$1.c $(W_INC) | $(dir $(W_OBJDIR)/$1)
	$(call bcmd,cc,$$@,$(CC) -c -o $$@ $$< $(W_CFLAGS))
endef
$(foreach src,$(basename $(W_SRC)),\
$(eval $(call make-lib-obj-rule,$(src))))

# Installation de la bibliothèque.

 install-libw: $(CHECKCFG) $(W_DEST) | $(ILIBDIR) $(IPKGDIR) $(sort \
 $(foreach x,$(W_INCp),$(dir $(IINCDIR)/lib$(LIBW)-$(VERSION)/$(x))))
	$(call imsg,Installation de la bibliothèque.)
	$(call qcmd,$(INST) -m 755 $(W_DEST) \
		$(ILIBDIR)/$(notdir $(W_DEST).$(MAJOR)))
	$(call qcmd,$(LN) $(notdir $(W_DEST).$(MAJOR)) \
		$(ILIBDIR)/$(notdir $(W_DEST)))
	$(call imsg,Installation du paquet pkg-config.)
	$(call qcmd,tools/write-wpkg.sh >$(IPKGDIR)/lib$(LIBW).pc)
	$(call imsg,Installation des headers.)
	$(foreach i,$(W_INCp),$(call qcmd,$(INST) -m 644 $(W_INCDIR)/$(i) \
		"$(IINCDIR)/lib$(LIBW)-$(VERSION)/$(i)"$(\n)))

.PHONY: all-libw install-libw

# ---
# Définition des targets pour le démon.
# ---
# Création du démon.

 all-daemon: $(CHECKCFG) $(D_DEST)

 $(D_DEST): $(D_OBJ) | $(dir $(D_DEST))
	$(call bcmd,ld,$@,$(LD) -o $@ $(D_OBJ) $(D_LIBS))

define make-daemon-obj-rule
 $(D_OBJDIR)/$1.c.o: $(D_SRCDIR)/$1.c $(D_INC) | $(dir $(D_OBJDIR)/$1)
	$(call bcmd,cc,$$@,$(CC) -c -o $$@ $$< $(D_CFLAGS))
endef
$(foreach src,$(basename $(D_SRC)),\
$(eval $(call make-daemon-obj-rule,$(src))))

# Installation du démon.

 install-daemon: $(CHECKCFG) $(D_DEST) | $(IBINDIR)
	$(call imsg,Installation du démon.)
	$(call qcmd,$(INST) -m 755 $(D_DEST) $(IBINDIR)/$(notdir $(D_DEST)))

.PHONY: all-daemon install-daemon

# ---
# Définition des targets pour la bibliothèque client.
# ---
# Création de la bibliothèque.

 all-lib: $(CHECKCFG) $(L_DEST)

 $(L_DEST): $(L_DEST).$(MAJOR) | $(dir $(L_DEST))
	$(call bcmd,ln,$@,$(LN) $(notdir $(L_DEST).$(MAJOR)) $(L_DEST))
 $(L_DEST).$(MAJOR): $(L_OBJ) | $(dir $(L_DEST))
	$(call bcmd,ld++,$@,$(LDXX) -o $@ $(L_OBJ) $(L_LDFLAGS))

define make-lib-obj-rule
 $(L_OBJDIR)/$1.cpp.o: $(L_SRCDIR)/$1.cpp $(L_INC) | $(dir $(L_OBJDIR)/$1)
	$(call bcmd,cxx,$$@,$(CXX) -c -o $$@ $$< $(L_CXXFLAGS))
endef
$(foreach src,$(basename $(L_SRC)),\
$(eval $(call make-lib-obj-rule,$(src))))

# Installation de la bibliothèque.

 install-lib: $(CHECKCFG) $(L_DEST) | $(ILIBDIR) $(IPKGDIR) $(sort \
 $(foreach x,$(L_INCp),$(dir $(IINCDIR)/lib$(LIB)-$(VERSION)/$(x))))
	$(call imsg,Installation de la bibliothèque.)
	$(call qcmd,$(INST) -m 755 $(L_DEST) \
		$(ILIBDIR)/$(notdir $(L_DEST).$(MAJOR)))
	$(call qcmd,$(LN) $(notdir $(L_DEST).$(MAJOR)) \
		$(ILIBDIR)/$(notdir $(L_DEST)))
	$(call imsg,Installation du paquet pkg-config.)
	$(call qcmd,tools/write-pkg.sh >$(IPKGDIR)/lib$(LIB).pc)
	$(call imsg,Installation des headers.)
	$(foreach i,$(L_INCp),$(call qcmd,$(INST) -m 644 $(L_INCDIR)/$(i) \
		"$(IINCDIR)/lib$(LIB)-$(VERSION)/$(i)"$(\n)))

.PHONY: all-lib install-lib

# ---
# Définition des targets pour le client.
# ---
# Construction du client.

 all-client: $(CHECKCFG) $(C_DEST)

 $(C_DEST): $(L_DEST) $(C_OBJ) | $(dir $(C_DEST))
	$(call bcmd,ld++,$@,$(LDXX) -o $@ $(C_OBJ) $(C_LDFLAGS))

 $(C_OBJDIR)/%.cpp.o: $(C_SRCDIR)/%.cpp $(C_INC) | $(C_OBJDIR)/
	$(call bcmd,cxx,$@,$(CXX) -c -o $@ $< $(C_CXXFLAGS))

# Installation du client.

 install-client: $(CHECKCFG) $(C_DEST) | $(IBINDIR)
	$(call imsg,Installation du client.)
	$(call qcmd,$(INST) -m 755 $(C_DEST) \
		$(IBINDIR)/$(notdir $(C_DEST)))

.PHONY: all-client install-client

# ---
# Création et téléversement de l'archive de distribution (tarball).
# ---

dist: mrproper
	$(call bcmd,mkdir,$(NAME)-$(VERSION),$(MD) .dist)
	$(call bcmd,cp,* $(NAME)-$(VERSION),$(CP) -R * .dist)
	$(call qcmd,$(MV) .dist $(NAME)-$(VERSION))
	$(call bcmd,tarball,$(NAME)-$(VERSION),\
		tar czf $(NAME)-$(VERSION).tar.gz --exclude .git $(NAME)-$(VERSION))
	$(call qcmd,$(RM) -r $(NAME)-$(VERSION))

 tarball: dist

 upload-tarball: tarball
	$(call msg,Uploading the tarball to the web server.)
	$(call qcmd,scp $(NAME)-$(VERSION).tar.gz usimarit.touhey.org:pub)

.PHONY: dist tarball upload-tarball

# ---
# Test du couple client/démon.
# ---

 test: all-daemon all-client
	$(call msg,Testing the daemon with the default client.)
	$(call qcmd,LIB=$(L_DEST) DAEMON=$(D_DEST) CLT=$(C_DEST) \
		tools/testclient.sh)

.PHONY: test

# End of file.
