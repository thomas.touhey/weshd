/* ****************************************************************************
 * weshd.x -- définition du protocole over Sun RPC pour le démon weshd.
 * Copyright (C) 2018 Thomas Touhey <thomas@touhey.fr>
 *
 * This project is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the
 * CeCILL license as circulated by CEA, CNRS and INRIA at the
 * following URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the project's author, the holder of the
 * economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also therefore
 * means that it is reserved for developers and experienced professionals
 * having in-depth computer knowledge. Users are therefore encouraged to load
 * and test the software's suitability as regards their requirements in
 * conditions enabling the security of their systems and/or data to be
 * ensured and, more generally, to use and operate it in the same conditions
 * as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 * ************************************************************************* */
/* Ce fichier est une évolution du protocole vers une utilisation des
 * `union switch()` pour regrouper des objets entre eux. Puisque le protocole,
 * de pair avec le logiciel, est en cours de développement, celui-ci utilise
 * toujours la version 1 (WESHD_VERS1), puisque celle-ci n'est pas encore
 * posée (et ne le sera pas avant la version 1.0).
 *
 * Pour retrouver d'anciennes versions du protocole, reculez dans l'historique
 * du dépôt git de développement, disponible via l'URL suivante :
 * <https://forge.touhey.fr/usi/weshd.git>.
 *
 * À noter que les unions et structures dans les unions ont été principalement
 * faites à cause des limitations de `rpcgen` : en effet, les cas dans
 * une union ne peuvent contenir que zéro ou un membre, et les unions
 * ne peuvent pas être définies anonymement.
 *
 * Notez également que contrairement au C standard, les `typedef` sont
 * absents : cela est dû au fait que `rpcgen` les ajoute automatiquement
 * dans les fichiers source et en-tête générés pour tout garder dans le
 * même espace de noms. */

/* ---
 * Utilitaires.
 * --- */

/* Horodate, la plus complète possible pour tout couvrir.
 * `dt` pour `datetime`, « date et heure ». */

struct wespdt_t {
	/* Date dans le calendrier grégorien.
	 * `year`: année, e.g. 2018.
	 * `mon`: mois de l'année, de 1 à 12.
	 * `dom`: jour du mois, de 1 à 31.
	 * `hour`: heure du jour, de 0 à 23.
	 * `min`: minute de l'heure, de 0 à 59.
	 * `sec`: seconde de la minute, de 0 à 60 (leap seconds). */

	int year;
	int mon;
	int dom;
	int hour;
	int min;
	int sec;

	/* Jour de la semaine, de 0 à 6 pour représenter lundi à dimanche. */

	int dow;

	/* Fuseau horaire et option d'heure d'été.
	 * Le fuseau horaire est défini par l'heure et la minute de décalage
	 * par rapport à l'heure dans le fuseau GMT.
	 * Si le fuseau est négatif, alors l'heure sera négative.
	 * Si `dst` est vrai, alors l'heure d'été (daylight savings flag)
	 * est en vigueur. */

	int tzhour;
	int tzmin;
	bool dst;
};

/* Adresse IP (avec version). */

enum wespaddrtype_t {
	WESPADDRTYPE_IP  = 1,
	WESPADDRTYPE_IP6 = 2
};

union wespaddr_t switch (wespaddrtype_t type) {
case WESPADDRTYPE_IP:
	unsigned char ip[4];
case WESPADDRTYPE_IP6:
	unsigned char ip6[16];
};

/* ---
 * Codes relatifs au protocole.
 * --- */

/* Les codes de retour (ou d'erreur) sont les suivants :
 *
 * - `OK`:  aucune erreur ;
 * - `INT`: une erreur interne s'est produite ;
 * - `IMP`: cette fonctionnalité n'est pas (encore) implémentée.
 * - `VAL`: arguments invalides pour cette fonctionnalité.
 * - `MIS`: mismatch entre ce qu'on a et ce qui est attendu par le client,
 *          utilisé pour les compteurs.
 *
 * - `NOR`: la ressource n'existe pas.
 * - `CON`: le serveur où est hébergée la ressource n'a pas pu être joint.
 * - `LRG`: la période pour la récupération de données est trop large,
 *          le statut de la (sous-)ressource a changé entretemps (e.g.
 *          tarif appliqué à un compteur via téléinfo, BASE puis HCHP). */

enum wespret_t {
	WESPRET_OK  =  0,
	WESPRET_INT =  1,
	WESPRET_IMP =  2,
	WESPRET_VAL =  3,
	WESPRET_MIS =  4,

	WESPRET_NOR = 10,
	WESPRET_CON = 11,
	WESPRET_LRG = 12
};

/* ---
 * Gestion des objets.
 * --- */

/* Identifiant d'un objet (serveur WES, compteur, …).
 * Tous les objets sont dans un même espace d'identifiants, où chacun
 * peut aller de 1 à 32766 inclus. */

typedef int wespid_t;

/* Type de l'objet. */

enum wesptype_t {
	WESPTYPE_NONE   = 0,
	WESPTYPE_WES    = 1,
	WESPTYPE_METER  = 2,
	WESPTYPE_SENSOR = 3
};

/* Sous-type de l'objet pour un objet de type `WESTYPE_WES`.
 * `NONE` : type générique.
 * `DUMMY`: serveur WES factice.
 * `IP`: serveur WES connecté par IP. */

enum wesptype_wes_t {
	WESPTYPE_WES_NONE  = 0,
	WESPTYPE_WES_DUMMY = 1,
	WESPTYPE_WES_IP    = 2
};

/* Sous-type de l'objet pour un objet de type `WESTYPE_METER`.
 * `NONE`: type générique.
 * `TELEINFO`: compteur branché via le protocole télé-information d'Enedis.
 * `AMPER`: pinces ampèremétriques.
 * `PULSE`: compteur d'impulsions. */

enum wesptype_meter_t {
	WESPTYPE_METER_NONE     = 0,
	WESPTYPE_METER_AMPER    = 1,
	WESPTYPE_METER_PULSE    = 2,
	WESPTYPE_METER_TELEINFO = 3
};

/* Sous-type de l'objet pour un objet de type `WESTYPE_SENSOR`.
 * `NONE`: type générique.
 * `ONEWIRE`: capteur branché via le bus 1-Wire. */

enum wesptype_sensor_t {
	WESPTYPE_SENSOR_NONE    = 0,
	WESPTYPE_SENSOR_ONEWIRE = 1
};

/* ---
 * Objets de type serveurs WES.
 * --- */

/* Propriétés supplémentaires concernant les serveurs WES connectés via IP. */

struct wespcfg_wes_ip_t {
	/* Heure locale du WES. */

	wespdt_t current_time;

	/* Propriétés réseau. */

	bool dhcp_enabled;
	unsigned char ip[4];
	unsigned char mask[4];
	unsigned char gw[4];
	unsigned char dns1[4];
	unsigned char dns2[4];
	unsigned char mac[6]; /* read-only! */
};

/* Propriétés supplémentaires concernant les serveurs WES en général, inclus
 * dans la définition de l'objet général. */

union wespcfg_wesmore_t switch (wesptype_wes_t type) {
case WESPTYPE_WES_NONE:
	/* Propriétés génériques uniquement. */
case WESPTYPE_WES_DUMMY:
	/* Rien pour le moment, mais l'on pourrait imaginer des tweaks
	 * de simulation. */
case WESPTYPE_WES_IP:
	wespcfg_wes_ip_t ip;
};

struct wespcfg_wes_t {
	wespcfg_wesmore_t more;
};

/* ---
 * Objets de type compteur.
 * --- */

/* Type de tarif pour le protocole télé-informatique d'ERDF.
 * `BASE`:  forfait Base (toutes heures), le prix du kWh ne varie pas en
 *          fonction de la période de consommation ;
 * `HCHP`:  forfait Heures Pleines Heures Creuses, le prix du kWh varie
 *          en fonction de l'heure de la journée ;
 * `TEMPO`: forfait Temporaire, le prix du kWh varie en fonction de l'heure
 *          de la journée, ainsi que de la couleur de la journée ;
 * `EJP`:   forfait Effacement de Jour de Pointe, le prix du kWh varie en
 *          fonction de la journée. */

enum wesptictarif_t {
	WESPTICTARIF_BASE  = 0,
	WESPTICTARIF_HCHP  = 1,
	WESPTICTARIF_TEMPO = 2,
	WESPTICTARIF_EJP   = 3
};

/* Période tarifaire pour le protocole télé-informatique d'ERDF.
 * La signification des valeurs dans cette énumération dépend du forfait
 * souscrit pour ce compteur.
 * Cette structure est pensée comme un champ de bits puisque les
 * valeurs peuvent couvrir plusieurs périodes.
 *
 * Forfait BASE :
 * `TH`: toutes heures.
 *
 * Forfait HCHP :
 * `HC`: heures creuses.
 * `HP`: heures pleines.
 *
 * Forfait TEMPO :
 * `HC`: heures creuses.
 * `HP`: heures pleines.
 * `JB`: jours bleus.
 * `JW`: jours blancs.
 * `JR`: jours rouges.
 *
 * Forfait EJP :
 * `HN`: heures normales.
 * `PM`: pointes mobiles. */

enum wespticperiod_t {
	WESPTICPERIOD_TH =  1,

	WESPTICPERIOD_HC =  1,
	WESPTICPERIOD_HP =  2,

	WESPTICPERIOD_JB =  4,
	WESPTICPERIOD_JW =  8,
	WESPTICPERIOD_JR = 16,

	WESPTICPERIOD_HN =  1,
	WESPTICPERIOD_PM =  2
};

/* Propriétés supplémentaires pour un compteur relié via téléinfo. */

struct wespcfg_meter_teleinfo_t {
	/* Propriétés relatives aux compteurs branchés via
	 * le protocole télé-information défini par Enedis.
	 * `cost_<abo>_<période>`: coût du kWh pendant une période lorsqu'un
	 *                          forfait est actuellement en vigueur.
	 * `bdpv_enabled`: envoyer (ou non) les données via BDPV.
	 * `bdpv_hour`: heure d'envoi via BDPV.
	 * `bdpv_min`: minutes d'envoi via BDPV. */

	double cost_base_th;
	double cost_hchp_hc;
	double cost_hchp_hp;
	double cost_tempo_hcjb;
	double cost_tempo_hpjb;
	double cost_tempo_hcjw;
	double cost_tempo_hpjw;
	double cost_tempo_hcjr;
	double cost_tempo_hpjr;
	double cost_ejp_hn;
	double cost_ejp_pm;

	bool bdpv_enabled;
	int bdpv_hour;
	int bdpv_min;
	string bdpv_username<>;
	string bdpv_password<>;
};

/* Propriétés supplémentaires concernant un compteur de type
 * pinces ampèremétriques. */

struct wespcfg_meter_amper_t {
	/* `voltage`: voltage (en V). */

	int voltage;

	/* Propriétés tarifaires.
	 * `cost`: coût de l'unité. */

	double cost;
};

/* Méthode de mesure pour les compteurs à impulsions.
 * `ILS`: impulsions mécaniques.
 * `ELEC`: impulsions électroniques. */

enum wespplsmethod_t {
	WESPPLSMETHOD_ILS  = 0,
	WESPPLSMETHOD_ELEC = 1
};

/* Propriétés supplémentaires concernant un compteur d'impulsions. */

struct wespcfg_meter_pulse_t {
	/* Propriétés relatives aux compteurs de type impulsions.
	 * `method`: méthode de comptage d'impulsions.
	 * `group_size`: taille d'un groupe d'impulsions pour faire une unité. */

	wespplsmethod_t method;
	int group_size;

	/* Propriétés tarifaires.
	 * `cost`: coût de l'unité. */

	double cost;
};

/* Mode de consommation/production de chaque compteur.
 * `CONSO`: consommation.
 * `PROD`: production (panneaux solaires, …). */

enum wespmetermode_t {
	WESPMETERMODE_CONSO = 0,
	WESPMETERMODE_PROD  = 1
};

/* Grandeur mesurée par le compteur.
 * `UNKNOWN`: inconnu.
 * `WATER`: eau froide/chaude.
 * `GAS`: gaz.
 * `ELEC`: électricité.
 * `FUEL`: fioul. */

enum wespmeterwhat_t {
	WESPMETERWHAT_UNKNOWN = 0,
	WESPMETERWHAT_WATER   = 1,
	WESPMETERWHAT_GAS     = 2,
	WESPMETERWHAT_ELEC    = 3,
	WESPMETERWHAT_FUEL    = 4
};

/* Unité utilisée pour les valeurs du compteur.
 * `UNKNOWN`: Inconnu.
 * `WH`: Watts par heure (Wh).
 * `KWH`: Kilo-watts par heure (kWh).
 * `LITER`: Litres (L).
 * `CUBICMETER`: Mètres cube (m³). */

enum wespmeterunit_t {
	WESPMETERUNIT_UNKNOWN    = 0,
	WESPMETERUNIT_WH         = 1,
	WESPMETERUNIT_KWH        = 2,
	WESPMETERUNIT_LITER      = 3,
	WESPMETERUNIT_CUBICMETER = 4
};

/* Propriétés supplémentaires concernant un compteur en général, utilisé dans
 * la définition d'un objet. */

union wespcfg_metermore_t switch (wesptype_meter_t type) {
case WESPTYPE_METER_NONE:
	/* Paramètres génériques uniquement. */
case WESPTYPE_METER_TELEINFO:
	wespcfg_meter_teleinfo_t teleinfo;
case WESPTYPE_METER_AMPER:
	wespcfg_meter_amper_t amper;
case WESPTYPE_METER_PULSE:
	wespcfg_meter_pulse_t pulse;
};

struct wespcfg_meter_t {
	/* Propriétés de base de tout compteur.
	 * `rd`: le compteur est-il lu ou ignoré par le système ?
	 * `mode`: mode de consommation/production du compteur.
	 * `what`: grandeur mesurée. */

	bool rd;
	wespmetermode_t mode;
	wespmeterwhat_t what;

	/* Informations concernant le coût.
	 * `fixed_cost`: coût fixe de l'abonnement, à l'année.
	 * `prorata`: l'abonnement est-il au prorata du prix de l'unité ? */

	double fixed_cost;
	bool prorata;

	wespcfg_metermore_t more;
};

/* ---
 * Objets de type capteur.
 * --- */

/* Types de capteurs reconnus par weshd. */

enum wespsensorwhat_t {
	WESPSENSORTYPE_TEMP    = 1
};

/* Unité pouvant être utilisées par les capteurs. */

enum wespsensorunit_t {
	WESPSENSORUNIT_CELSIUS = 1
};

/* Propriétés supplémentaires pour un capteur relié via le bus 1-Wire. */

struct wespcfg_sensor_onewire_t {
	unsigned char id[8];
};

/* Propriétés supplémentaires pour un capteur en général. */

union wespcfg_sensormore_t switch (wesptype_sensor_t type) {
case WESPTYPE_SENSOR_NONE:
	/* Paramètres génériques uniquement. */
case WESPTYPE_SENSOR_ONEWIRE:
	wespcfg_sensor_onewire_t onewire;
};

struct wespcfg_sensor_t {
	/* Propriétés de base de tout capteur.
	 * `what`: ce qui est mesuré par le capteur.
	 * `more`: plus d'informations par rapport au type de capteur. */

	wespsensorwhat_t what;
	wespcfg_sensormore_t more;
};

/* ---
 * Définition générale d'un objet dans le protocole pour weshd.
 * --- */

/* Les types sont donnés en plus dans `type` et `subtype` puisque ce n'est pas
 * parce qu'on informe le client de ceux-ci que les données suivent
 * derrière. */

union wespcfgmore_t switch (wesptype_t type) {
case WESPTYPE_NONE:
	/* Propriétés génériques uniquement. */
case WESPTYPE_WES:
	wespcfg_wes_t wes;
case WESPTYPE_METER:
	wespcfg_meter_t meter;
case WESPTYPE_SENSOR:
	wespcfg_sensor_t sensor;
};

struct wespcfg_t {
	int type;
	int subtype;

	string name<>;
	wespcfgmore_t more;
};

/* ---
 * Drapeaux pour la configuration.
 * --- */

/* Propriétés générales. */

const WESPCFG_NAME                        =     1;

/* Propriétés d'un serveur WES connecté par IP. */

const WESPCFG_WES_IP_TIME                 =     2;
const WESPCFG_WES_IP_DHCP                 =     4;
const WESPCFG_WES_IP_ADDR                 =     8;
const WESPCFG_WES_IP_MASK                 =    16;
const WESPCFG_WES_IP_GW                   =    32;
const WESPCFG_WES_IP_DNS1                 =    64;
const WESPCFG_WES_IP_DNS2                 =   128;
const WESPCFG_WES_IP_MAC                  =   256;

/* Propriétés générales d'un compteur. */

const WESPCFG_METER_READ                  =     2;
const WESPCFG_METER_MODE                  =     4;
const WESPCFG_METER_WHAT                  =     8;
const WESPCFG_METER_FIXED_COSTS           =    16;
const WESPCFG_METER_PRORATA               =    32;

/* Propriétés concernant un compteur branché via téléinfo. */

const WESPCFG_METER_TELEINFO_COSTS_BASE   =    64;
const WESPCFG_METER_TELEINFO_COSTS_HCHP   =   128;
const WESPCFG_METER_TELEINFO_COSTS_TEMPO  =   256;
const WESPCFG_METER_TELEINFO_COSTS_EJP    =   512;
const WESPCFG_METER_TELEINFO_BDPV_ENABLED =  1024;
const WESPCFG_METER_TELEINFO_BDPV_IDS     =  2048;
const WESPCFG_METER_TELEINFO_BDPV_TIME    =  4096;

/* Propriétés concernant les pinces ampèremétriques. */

const WESPCFG_METER_AMPER_VOLTAGE         =    64;
const WESPCFG_METER_AMPER_COST            =   128;

/* Propriétés concernant un compteur d'impulsions. */

const WESPCFG_METER_PULSE_METHOD          =    64;
const WESPCFG_METER_PULSE_GROUP           =   128;
const WESPCFG_METER_PULSE_COST            =   256;

/* Propriétés concernant un capteur branché via 1-Wire. */

const WESPCFG_SENSOR_ONEWIRE_ID           =     2;

/* ---
 * Objets de données.
 * --- */

/* Propriétés concernant les compteurs branchés via téléinfo. */

#define bbr_hcjb tempo_hcjb
#define bbr_hpjb tempo_hpjb
#define bbr_hcjw tempo_hcjw
#define bbr_hpjw tempo_hpjw
#define bbr_hcjr tempo_hcjr
#define bbr_hpjr tempo_hpjr

struct wespdata_meter_teleinfo_t {
	/* Données relatives à un compteur branché via téléinfo.
	 * Si plusieurs compteurs ou plusieurs forfaits ont été connus
	 * sur la période donnée, l'erreur `WESPRET_LRG` sera renvoyée.
	 *
	 * `adco`: numéro ADCO du compteur (un chiffre direct par case).
	 * `phases`: nombre de phases (monophasé : 1, triphasé : 3).
	 * `isousc`: intensité souscrite.
	 * `pa`: puissance apparente (en VA).
	 * `iinst`: intensité instantanée pour chaque phase.
	 * `imax`: intensité maximale pour chaque phase. */

	unsigned char adco[12];
	int phases;

	unsigned int isousc;
	unsigned int pa;
	unsigned int iinst[3];
	unsigned int imax[3];

	/* `<forfait>_<période>`: nombre d'unités (partie commune) consommées
	 * par période tarifaire. */

	double base_th;
	double hchp_hc;
	double hchp_hp;
	double tempo_hcjb;
	double tempo_hpjb;
	double tempo_hcjw;
	double tempo_hpjw;
	double tempo_hcjr;
	double tempo_hpjr;
	double ejp_hn;
	double ejp_pm;
};

/* Propriétés concernant les compteurs. */

union wespdata_metermore_t switch (wesptype_meter_t type) {
case WESPTYPE_METER_AMPER:
	/* Aucune donnée supplémentaire à fournir. */
case WESPTYPE_METER_PULSE:
	/* Aucune donnée supplémentaire à fournir. */
case WESPTYPE_METER_TELEINFO:
	wespdata_meter_teleinfo_t teleinfo;
};

struct wespdata_meter_t {
	/* Un compteur est un objet qui compte le nombre d'unités consommées
	 * d'une énergie donnée, e.g. des litres d'eau ou des kVA d'électricité.
	 *
	 * `count`: nombre total d'unités consommées sur la période.
	 * `unit`: unité de la valeur précédente. */

	double count;
	wespmeterunit_t unit;

	wespdata_metermore_t more;
};

/* Propriétés concernant les capteurs. */

struct wespdata_sensor_t {
	/* Un capteur est un objet dont seules les valeurs instantanées nous
	 * intéressent, et pour lesquels nous produirons donc des statistiques.
	 *
	 * `min`: valeur minimale sur la période.
	 * `avg`: moyenne sur la période.
	 * `max`: valeur maximale sur la période.
	 * `unit`: unité des valeurs précédentes. */

	double min;
	double avg;
	double max;
	wespsensorunit_t unit;
};

/* Objet général de données. */

union wespdatamore_t switch (wesptype_t type) {
case WESPTYPE_METER:
	wespdata_meter_t meter;
case WESPTYPE_SENSOR:
	wespdata_sensor_t sensor;
};

struct wespdata_t {
	/* Valeurs avec unité consommée pendant la période.
	 * `active`: temps cumulé d'activité (« uptime ») de la ressource,
	 *           de 0 (jamais en activité) à 10000 (toujours en activité).
	 * `more`: plus d'informations selon le type de la ressource, compteur
	 *         ou capteur. */

	int active;
	wespdatamore_t more;
};

/* ---
 * Propriétés d'enregistrement pour `GATHER_WES()`.
 * --- */

struct wespgopts_ip_t {
	/* Propriétés d'instanciation spécifiques au serveur WES connecté par IP.
	 * `addr` : adresse IP.
	 * `http_user`: utilisateur HTTP.
	 * `http_pass`: mot de passe HTTP.
	 * `ftp_user`: utilisateur FTP.
	 * `ftp_pass`: mot de passe FTP. */

	wespaddr_t addr;
	string http_user<>;
	string http_pass<>;
	string ftp_user<>;
	string ftp_pass<>;
};

/* Type général. */

union wespgopts_t switch (westype_wes_t type) {
case WESPTYPE_WES_NONE:
	/* Ce type ne peut pas être instancié. */
case WESPTYPE_WES_DUMMY:
	/* Ce type ne prend aucun paramètre. */
case WESPTYPE_WES_IP:
	wespgopts_ip_t ip;
};

/* ---
 * Arguments.
 * --- */

struct wespid_with_flags_t {
	wespid_t id;
	wespflags_t flags;
};

struct wespid_with_flags_and_cfg_t {
	wespid_t id;
	wespflags_t flags;
	wespcfg_t cfg;
};

struct wespquery_t {
	wespid_t parent;
	int offset;
	int count; /* 30 max. */
};

struct wespid_and_span_t {
	wespid_t id;
	wespdt_t begin;
	wespdt_t end;
};

/* ---
 * Réponses.
 * --- */

/* Type de la réponse.
 * `NONE`: aucune donnée n'accompagne le code de retour.
 * `ID`: un identifiant accompagne le code de retour.
 * `CFG`: la configuration d'une ressource accompagne la réponse.
 * `DATA`: des données (compteurs, capteurs) accompagnent la réponse.
 * `LIST`: une liste de configurations avec identifiants accompagne
 *         la réponse. */

enum wespresp_type_t {
	WESPRESPTYPE_NONE = 0,
	WESPRESPTYPE_ID   = 1,
	WESPRESPTYPE_CFG  = 2,
	WESPRESPTYPE_DATA = 3,
	WESPRESPTYPE_LIST = 4
};

struct wesplistelt_t {
	wespid_t id;
	wespcfg_t cfg;
};

union wespresp_more_t switch (wespresp_type_t type) {
case WESPRESPTYPE_NONE:
	/* Code de retour uniquement. */
case WESPRESPTYPE_ID:
	wespid_t id;
case WESPRESPTYPE_CFG:
	wespcfg_t cfg;
case WESPRESPTYPE_DATA:
	wespdata_t data;
case WESPRESPTYPE_LIST:
	wesplistelt_t elements<30>;
};

struct wespresp_t {
	wespret_t ret;
	wespresp_more_t more;
};

/* ---
 * Définition du programme.
 * --- */

program WESHD_PROG {
	version WESHD_VERS1 {
		/* Fonction qui ne fait rien, pour pinger le service. */

		void WESHD_NULL(void) = 0;

		/* Récupération, suppression d'un ou plusieurs éléments. */

		wespresp_t GET(wespid_with_flags_t args) = 1;
		wespresp_t SET(wespid_with_flags_and_cfg_t args) = 2;

		wespresp_t QUERY(wespquery_t args) = 3;
		wespresp_t REMOVE(wespid_t id) = 4;

		/* Opérations spécifiques aux serveurs WES.
		 * `GATHER_WES()`: récupération (voire initialisation) d'un WES. */

		wespresp_t GATHER_WES(wespgatheroptions_t args) = 5;

		/* Opérations spécifiques aux compteurs et capteurs.
		 * `GET_DATA()`: récupération de données. */

		wespresp_t GET_DATA(wespid_and_span_t args) = 6;

		/* TODO: interactions avec le LCD */
	} = 1;
} = 0x20001234;

/* Puisque `rpcgen` ne définit pas, de base, dans le fichier d'en-tête,
 * la fonction correspondant au serveur, et que nous souhaitons nous en
 * servir, nous la définissons ici. */

#ifdef RPC_HDR
%extern void weshd_prog_1(struct svc_req *rqstp, SVCXPRT *transp);
#endif
