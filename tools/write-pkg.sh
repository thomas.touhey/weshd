#!/bin/sh
cd "$(dirname "$0")"

# ---
# Variables à utiliser.
# ---

name="wesh"
description="library for interacting with the WES handling daemon"
version="$(make -s -C .. getversion)"

libdir="$(make -s -C .. getlibdir)"
[ $? -eq 0 ] || exit 1
incdir="$(make -s -C .. getincdir)/lib${name}-${version}"
[ $? -eq 0 ] || exit 1

# ---
# Écriture du résultat.
# ---

cat <<EOF
includedir=${incdir}
libdir=${libdir}

Name: lib${name}
Description: ${description}
Version: ${version}
Libs: -L\${libdir} -l${name}
Cflags: -I\${includedir}
EOF

# End of file.
