#!/bin/sh
cd "$(dirname $0)"
cd ..

# Lancement du démon et récupération du PID.

${DAEMON} & WESHD_PID=$!

# Lancement du client en synchrone.

LD_PRELOAD=${LIB} ${CLT} ${CL}

# Arrêt du démon.

kill -9 $WESHD_PID

# End of file.
