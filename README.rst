Démon de gestion de serveurs WES
================================

Ce projet, nommé « weshd » pour « WES Handling Daemon », permet de gérer
un ou plusieurs `serveurs WES`_, superviseurs d'énergie produits par
l'entreprise française `Cartelectronic`_.

Il a été écrit en 2018 par `Thomas Touhey`_ à l'occasion d'un projet de
`BTS SNIR`_ organisé par le `GRETA de l'Essonne`_ et l'équipe pédagogique
du `lycée Michelet d'Arpajon`_. Ce projet vise à améliorer l'infrastructure
de gestion de la consommation de ressources telles que l'électricité ou
l'eau pour l'association `Monde en Marge, Monde en Marche`_, qui fournit
des logements sociaux.

Le projet est disponible sous les conditions de la licence de logiciel
libre `CeCILL`_ dans `sa version 2.1 <CeCILL 2.1_>`_. Cette licence a
été sélectionnée de par sa compatibilité avec le droit français.

Il contient trois composants :

- le `démon`_, qui gère les ressources en elles-mêmes et répond à
  des requêtes concernant celles-ci ;
- la `bibliothèque`_ `client`_ qui permet de faire des requêtes
  au démon ;
- un `client`_ utilisant la bibliothèque citée précédemment pour
  faire quelques opérations, comme servir de callback au serveur DHCP
  lorsque celui-ci a attribué ou retiré une adresse IP à un nouveau
  serveur WES sur le réseau.

La contruction de ces composants se fait en deux étapes :

- la *configuration*, soit la sélection de différentes options de
  construction et d'installation ;
- la *construction* en elle-même.

-----------
Dépendances
-----------

- un système `POSIX`_-compliant (sockets).
- la `libtirpc`_ (interface compatible SunRPC).
- la `libcurl`_ >= 7.0.

-------------
Configuration
-------------

Pour configurer le logiciel, il faut utiliser le script ``./configure``.
Au-delà des options d'affichage de messages spéciaux ``--help`` et
``--version``, pour générer une configuration qui convient à votre cas.

Les options existantes sont les suivantes :

``--nologo``

	Ne pas afficher le message de version/copyright/licence au démarrage
	du serveur.

``--loglevel=LOGLEVEL``

	N'afficher les messages qu'à partir d'un certain niveau de priorité.
	Sélectionner un niveau affichera les messages de ce niveau ainsi que
	ceux ayant une priorité plus importante.
	Les valeurs acceptées sont les suivantes :

	``info``, ``debug``
		Afficher les messages de développement (généralement utilisé par
		le mainteneur pour s'assurer que tout se déroule bien correctement).

	``warn``
		Afficher les messages d'avertissements.

	``error``
		Afficher les messages d'erreurs non fatales.

	``fatal``
		Afficher les messages d'erreurs fatales.

	``none``
		N'afficher que les messages non désactivables.

``--no-delete-scripts``

	Par défaut, ``weshd`` supprime les scripts qu'il téléverse et exécute
	sur les serveurs WES connectés par IP pour récupérer des variables.
	Pour du débogage, l'on peut préférer laisser les scripts sur l'appareil
	distant pour examiner s'ils sont bien formattés et quel résultat
	ils donnent : c'est ce que fait cette option.

``--libcurl-verbose``, ``--libcurl-http-verbose``, ``--libcurl-ftp-verbose``

	Rend la libcurl un peu plus bavarde lorsque weshd s'en sert,
	éventuellement pour certains protocoles uniquement.

------------
Construction
------------

Une fois le projet configuré, vous pouvez construire le projet avec la
commande ``make`` (ou ``make all``).

Trois parties du projet seront construites :

- le démon (construit sous ``build/weshd``) ;
- la bibliothèque client (construite sous ``build/libwesh.so{,.1}``) ;
- le client (construit sous ``build/wesh``).

Une fois construit, si vous souhaitez tester le client avec le démon (qui
sera lancé juste avant et éteint juste après), vous pouvez utiliser la
commande ``make test``.

Pour nettoyer le projet après construction, vous pouvez utiliser l'une des
deux commandes suivantes :

- ``make clean`` : supprime les fichiers de développement ainsi que les
  fichiers finaux ;
- ``make mrproper`` : supprime les fichiers de développement, les fichiers
  finaux, et la configuration de l'utilisateur (afin de retrouver l'état
  d'origine du dossier du projet).

-------------
Documentation
-------------

Je documente au maximum ce que je comprends des serveurs WES dans le dossier
``doc/``, notamment les interfaces qu'il faut, dans l'idéal, implémenter
pour pouvoir communiquer avec lui.

Pour ce qui concerne le code en lui-même, bien que je documente pas mal
comment le code est organisé dans les fichiers d'en-tête, il n'y a pas encore
de documentation indépendante ni de document écrit qui décrive ça en détail.
Ça sera dans notre rapport de stage.

.. _serveurs WES: https://www.cartelectronic.fr/content/8-serveur-wes
.. _Cartelectronic: https://www.cartelectronic.fr/
.. _Thomas Touhey: https://thomas.touhey.fr/
.. _BTS SNIR: http://www.onisep.fr/Ressources/Univers-Formation/Formations/Post-bac/BTS-Systemes-numeriques-option-A-informatique-et-reseaux
.. _GRETA de l'Essonne: http://www.greta-essonne.fr/
.. _lycée Michelet d'Arpajon: http://www.lyc-michelet-arpajon.ac-versailles.fr/
.. _Monde en Marge, Monde en Marche: https://metmmetm.fr/
.. _CeCILL: http://cecill.info/
.. _CeCILL 2.1: http://cecill.info/licences/Licence_CeCILL_V2.1-fr.html
.. _démon: https://fr.wikipedia.org/wiki/Daemon_%28informatique%29
.. _bibliothèque: https://fr.wikipedia.org/wiki/Biblioth%C3%A8que_logicielle
.. _client: https://fr.wikipedia.org/wiki/Client_(informatique)
.. _POSIX: https://fr.wikipedia.org/wiki/POSIX
.. _libcurl: https://curl.haxx.se/libcurl/
.. _libtirpc: https://sourceforge.net/projects/libtirpc/
.. _iniparser: http://ndevilla.free.fr/iniparser/
