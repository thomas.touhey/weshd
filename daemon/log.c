/* ****************************************************************************
 * log.c -- système de logging pour le démon de gestion de serveurs WES.
 * Copyright (C) 2018 Thomas Touhey <thomas@touhey.fr>
 *
 * This project is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the
 * CeCILL license as circulated by CEA, CNRS and INRIA at the
 * following URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the project's author, the holder of the
 * economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also therefore
 * means that it is reserved for developers and experienced professionals
 * having in-depth computer knowledge. Users are therefore encouraged to load
 * and test the software's suitability as regards their requirements in
 * conditions enabling the security of their systems and/or data to be
 * ensured and, more generally, to use and operate it in the same conditions
 * as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 * ************************************************************************* */
#include "internals.h"

static loglevel_t current_log_level = DEFAULT_LOGLEVEL;

/* `set_log_level()`: définir le niveau de logging minimum. */

void set_log_level(loglevel_t loglevel)
{
	current_log_level = loglevel;
}

/* `decode_log_level()`: décoder une chaîne. */

loglevel_t decode_log_level(const char *text)
{
	if (!strcmp(text, "debug") || !strcmp(text, "info"))
		return (WLDEBUG);
	if (!strcmp(text, "warn"))
		return (WLWARN);
	if (!strcmp(text, "error"))
		return (WLERROR);
	if (!strcmp(text, "fatal"))
		return (WLFATAL);
	return (WLNOTICE);
}

/* `islog()`: vérifier si l'on doit logger un message ou non. */

static int islog(loglevel_t loglevel)
{
	return (loglevel >= current_log_level);
}

/* ---
 * Affichage simple.
 * --- */

/* `loglevel_tostring()`: Récupération de la chaîne correspondant à un
 * niveau de log. */

static const char *loglevel_tostring(loglevel_t loglevel)
{
	if (loglevel <= WLDEBUG)
		return ("debug");
	else if (loglevel <= WLWARN)
		return ("warn");
	else if (loglevel <= WLERROR)
		return ("error");
	else if (loglevel <= WLFATAL)
		return ("fatal");
	else
		return ("notice");
}

/* `log_prefix()`: Affichage du préfixe pour une ligne. */

static void log_prefix(loglevel_t loglevel, const char *func)
{
	if (func && loglevel <= WLFATAL)
		fprintf(stderr, "\r[weshd %5s] %s: ",
			loglevel_tostring(loglevel), func);
	else
		fprintf(stderr, "\r[weshd %5s] ",
			loglevel_tostring(loglevel));
}

/* `log_msg()`: Affichage d'un message. */

void log_msg(loglevel_t loglevel, const char *func, const char *format, ...)
{
	va_list args;
	int shouldlog = islog(loglevel);

	/* put the prefix */
	if (shouldlog)
		log_prefix(loglevel, func);

	/* put the main part */
	va_start(args, format);
	if (shouldlog) {
		vfprintf(stderr, format, args);
		fputc('\n', stderr);
	}
	va_end(args);
}

/* ---
 * Affichage d'une zone de mémoire.
 * --- */

/* `log_putascii()`: mettre un nombre en ASCII-hex, dans un champ de
 * n caractères. */

static void log_putascii(unsigned char *p, unsigned long i, int n)
{
	p += (n - 1);
	while (n--) {
		int j = i % 16;
		*p-- = (unsigned char)(j >= 10 ? j - 10 + 'A' : j + '0');
		i /= 16;
	}
}

/* `log_mem_hex()`: affiche la représentation en hexadécimal de deux octets
 * maximum. */

static void log_mem_hex(char *s, const unsigned char *m, size_t n)
{
	size_t l = 0;
	while (l < 8) {
		/* Mise du nombre sous forme hexadécimale. */
		if (n) {
			log_putascii((unsigned char*)s, *m++, 2);
			s += 2;
		} else {
			*s++ = ' ';
			*s++ = ' ';
		}

		/* Décrémentation de la taille de la zone de mémoire à traiter. */
		n -= !!n;

		/* On va au prochain caractère si s est à la fin d'un groupe. */
		if (l++ % 2)
			s++;
	}
}

/* `log_mem_asc()`: afficher l'interprétation ASCII d'un maximum de
 * deux octets. */

static void log_mem_asc(char *s, const unsigned char *m, size_t n)
{
	size_t l = 0;

	while (n-- && l++ < 8) {
		/* Pour chaque octet, on met le caractère (ou un point si
		 * celui-ci n'est pas imprimable. */

		if (isprint(*m++))
			*s++ = *((const char*)m - 1);
		else
			*s++ = '.';
	}

	/* Et on n'oublie pas la fin de la ligne. */

	*s++ = '\n';
	*s   = '\0';
}

/* `log_mem()`: afficher une zone de mémoire. */

void log_mem(loglevel_t loglevel, const char *func,
	const void *m, size_t n)
{
	char linebuf[58];
	const unsigned char *p;

	/* On voit si on affiche bien la zone de mémoire par rapport au
	 * niveau de log actuel. */

	if (!islog(loglevel))
		return ;

	/* S'il n'y a rien, on affiche ça. */

	if (!n) {
		log_prefix(loglevel, func);
		fprintf(stderr, "(nothing)\n");
		return ;
	}

	/* Pour chaque groupe de huit octets maximum, on remplit la zone
	 * ASCII-hex et la zone ASCII, puis on affiche la ligne. */

	memcpy(linebuf, "0000 0000 0000 0000 ", 20);
	for (p = m; n > 0;) {
		log_mem_hex(&linebuf[0], p, n);
		log_mem_asc(&linebuf[20], p, n);

		log_prefix(loglevel, func);
		fputs(linebuf, stderr);

		p += 8;
		n -= n > 8 ? 8 : n;
	}
}
