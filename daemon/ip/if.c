/* ****************************************************************************
 * ip/base.c -- définition de l'interface.
 * Copyright (C) 2018 Thomas Touhey <thomas@touhey.fr>
 *
 * This project is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the
 * CeCILL license as circulated by CEA, CNRS and INRIA at the
 * following URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the project's author, the holder of the
 * economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also therefore
 * means that it is reserved for developers and experienced professionals
 * having in-depth computer knowledge. Users are therefore encouraged to load
 * and test the software's suitability as regards their requirements in
 * conditions enabling the security of their systems and/or data to be
 * ensured and, more generally, to use and operate it in the same conditions
 * as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 * ************************************************************************* */
#include "internals.h"
#define msgcgi(VAR, DESC) \
	msg((wlerror, "did CGI " VAR " (" DESC ") definition change?"));
#define GETCGI(NUM, BLK) \
	{ \
		int getcgi_err; \
		if ((getcgi_err = get_cgi(wes, (NUM), (BLK)))) { \
			msg((wlerror, "cgi error: %s", error_string(getcgi_err))); \
			return (getcgi_err); \
		} \
	}

static wesif_t ipif;

/* ---
 * Création et ajout d'une ressource, et suppression de cette même ressource.
 * --- */

/* `free_ip_wes()`: Supprimer une ressource de type WES. */

static int free_ip_wes(wes_t *wes)
{
	if (wes->http_handle) {
		curl_easy_cleanup(wes->http_handle);
		wes->http_handle = NULL;
	}
	if (wes->ftp_handle) {
		curl_easy_cleanup(wes->ftp_handle);
		wes->ftp_handle = NULL;
	}

	deinit_m2m(wes);

	clear_config(wes);
	/* FIXME: clear_data(wes); */

	set_http_ident(wes, NULL, NULL);
	set_ftp_ident(wes, NULL, NULL);

	deinit_wes_base(wes);
	free(wes);
	return (WROK);
}

/* `make_ip_wes()`: Création bête et méchante d'une ressource WES de la
 * classe courante. Ce n'est pas cette fonction qui sera publique */

static int make_ip_wes(wes_t **wesp, wesaddr_t const *addr,
	char *http_username, char *http_password,
	char *ftp_username, char *ftp_password)
{
	int err;
	wes_t *wes;

	/* Allocation dynamique de la ressource. */

	wes = malloc(sizeof(wes_t));
	if (!wes)
		return (WRALLOC);

	/* Initialisation de la base. */

	if ((err = init_wes_base(wes, &ipif))) {
		free(wes);
		return (err);
	}

	memcpy(&wes->addr, &addr, sizeof(wes->addr));

	wes->sections = NULL;
	wes->data = NULL;
	wes->http_handle = NULL;
	wes->ftp_handle = NULL;
	wes->http_name = NULL;
	wes->http_pass = NULL;
	wes->ftp_name = NULL;
	wes->ftp_pass = NULL;

	/* TODO: Il faudrait peut-être initialiser `data` ici… ? */

	init_m2m(wes);

	if (http_username && !*http_username)
		http_username = NULL;
	if (http_password && !*http_password)
		http_password = NULL;
	if (ftp_username && !*ftp_username)
		ftp_username = NULL;
	if (ftp_password && !*ftp_password)
		ftp_password = NULL;

	if ((err = set_http_ident(wes, http_username, http_password))
	 || (err =  set_ftp_ident(wes, ftp_username, ftp_password))) {
		free_ip_wes(wes);
		return (err);
	}

	/* Récupération du hostname. */

	{
		char hostname[33];
		wescgi_t blocks[] = {
			WESCGI("r n", "%s", hostname, 33)
		};

		if ((err = get_cgi(wes, 1, blocks))) {
			msg((wlerror, "cgi error: %s", error_string(err)));
			free_ip_wes(wes);
			return (err);
		}

		if ((err = set_name(wes, hostname))) {
			free_ip_wes(wes);
			return (err);
		}
	}

	*wesp = wes;
	return (WROK);
}

/* `add_ip_wes()`: créer une ressource de type serveur WES via IP (ou
 * rafraîchir une existante).
 *
 * Historiquement, cette fonction s'appelait `register_from_dhcpv4()`
 * et a d'abord été pensée comme un hook appelé indirectement par le
 * serveur DHCP qui détecterait une nouvelle machine sur un réseau
 * réservé à des serveurs WES. Ensuite, le paramètre correspondant à
 * l'adresse MAC a été retiré et la fonction ne servait plus que de helper
 * pour le code du serveur (puisque l'interface WES via IP était l'unique
 * à une époque, et le code n'était pas encore pensé en interfaces).
 *
 * Depuis, la façon dont est pensée cette fonction est devenue le
 * standard sur la manière de créer/d'ajouter un nouveau serveur WES
 * pour toute interface. */

int add_ip_wes(weslist_t *list, wes_t **wesp, wesaddr_t const *addr,
	char *http_username, char *http_password,
	char *ftp_username, char *ftp_password)
{
	wesiter_t iter;
	wes_t *wes;
	int err;

	/* On vérifie si les paramètres sont OK. */

	if (addr->type != WINET || ~addr->data.inet.has & HASIPv4
	 || addr->data.inet.has & ~HASIPv4)
		return (WROP);
	/* TODO: check if IPv4 address is valid, not broadcast, etc? */

	/* On vérifie si une entrée correspondante n'existe pas déjà. */

	iterate_on_list(&iter, list);
	while (!(err = get_next_iteration(&iter, &wes))) {
		/* On regarde s'il s'agit bien d'un WES de la même interface. */

		if (wes->base.iface != &ipif)
			continue;

		/* On regarde si l'adresse IP correspond.
		 * TODO: si on gère plus de familles d'adresses réseau un jour,
		 * il faudra contrôler que tout va bien. */

		if (memcmp(addr->data.inet.ipv4, wes->addr.data.inet.ipv4, 4))
			continue;

		/* L'adresse correspond, mince, ça veut dire qu'on a failli
		 * créer un duplicata ! Ne le créons pas, et renvoyons plutôt
		 * celui-ci en précisant qu'il existe déjà. */

		*wesp = wes;
		return (WREXISTS);
	}

	/* Aucune entrée n'existe avec cette IP, créons-la ! */

	err = make_ip_wes(&wes, addr, http_username, http_password,
		ftp_username, ftp_password);
	if (err)
		return (err);

	/* Et ajoutons l'entrée à la liste. */

	if ((err = add_to_list(list, wes))) {
		free_ip_wes(wes);
		return (err);
	}

	*wesp = wes;
	return (WROK);
}

/* ---
 * Gestion de la configuration du serveur WES.
 * --- */

static int getwescfg(wes_t *wes, wescfgflags_t flags, wescfg_t **cfgp)
{
	static wescfg_t cfg;

	(void)flags;

	/* Préparation de l'objet.
	 * L'élément n'est pas à supprimer puisqu'il est défini de façon statique,
	 * d'où `.free = NULL`.
	 * De plus, le nom est géré par le serveur (qui le stocke depuis qu'on
	 * l'a ajouté avec la fonction d'ajout/création). */

	cfg.free = NULL;
	cfg.name = NULL;

	/* Récupération des éléments. */

	{
		char datebuf[20], timebuf[20], gmtbuf[10], sumbuf[10],
			sembuf1[5], sembuf2[5], sembuf3[5], sembuf4[5],
			sembuf5[5], sembuf6[5];
		char dhcpbuf[5], ipbuf[20], msbuf[20], gwbuf[20], dpbuf[20],
			dsbuf[20], macbuf[30];

		wescgi_t blocks[] = {
			/* La date du jour, sous format numérique `D M Y`. */
			WESCGI("h d", "%d %d %d", datebuf, 20),

			/* L'heure courante, sous format numérique `H M S`. */
			WESCGI("h h", "%d %d %d", timebuf, 20),

			/* Le fuseau horaire (en nombre entier d'heures). */
			WESCGI("h G", "%d", gmtbuf, 10),

			/* Selon si l'on est en heure d'été (activée) ou en
			 * heure d'hiver (désactivée). */
			WESCGI("h e", "%.1s", sumbuf, 10),

			/* Le jour de la semaine qu'on est (où `g [1-7]` est "selected" si
			 * c'est le jour de la semaine actuel, et rien sinon.
			 * TODO: peut-être que `g j` fait ça mieux… ? */
			WESCGI("g 1", "%.1s", sembuf1, 5),
			WESCGI("g 2", "%.1s", sembuf2, 5),
			WESCGI("g 3", "%.1s", sembuf3, 5),
			WESCGI("g 4", "%.1s", sembuf4, 5),
			WESCGI("g 5", "%.1s", sembuf5, 5),
			WESCGI("g 6", "%.1s", sembuf6, 5),

			/* Selon si le DHCP est activé (checked) ou désactivé. */
			WESCGI("r d", "%.1s", dhcpbuf, 5),

			/* L'adresse IP actuelle. */
			WESCGI("r i", "%d %d %d %d", ipbuf, 20),

			/* Le masque de sous-réseau actuel. */
			WESCGI("r m", "%d %d %d %d", msbuf, 20),

			/* La passerelle par défaut actuelle. */
			WESCGI("r g", "%d %d %d %d", gwbuf, 20),

			/* Les serveurs DNS primaires (p) et secondaires (s). */
			WESCGI("r p", "%d %d %d %d", dpbuf, 20),
			WESCGI("r s", "%d %d %d %d", dsbuf, 20),

			/* L'adresse MAC du serveur. */
			WESCGI("r a", "%d %d %d %d %d %d", macbuf, 30)
		};

		GETCGI(17, blocks)

		/* ---
		 * Décodage des éléments.
		 * --- */

		/* Jour du mois, mois, année. */

		if (blocks[0].lines != 1 || sscanf(datebuf, "%d%d%d",
			&cfg.current_time.dom, &cfg.current_time.mon,
			&cfg.current_time.year) < 3) {
			msgcgi("h d", "date");
			return (WRUNKNOWN);
		}

		/* Heure, minute de l'heure, seconde de la minute. */

		if (blocks[1].lines != 1 || sscanf(timebuf, "%d%d%d",
			&cfg.current_time.hour, &cfg.current_time.min,
			&cfg.current_time.sec) < 3) {
			msgcgi("h h", "hour");
			return (WRUNKNOWN);
		}

		/* Fuseau horaire. */

		if (blocks[2].lines != 1 || sscanf(gmtbuf, "%d",
			&cfg.current_time.tzhour) < 1) {
			msgcgi("h G", "gmt");
			return (WRUNKNOWN);
		}
		cfg.current_time.tzmin = 0;

		/* Heure d'été (dst). */

		if (blocks[3].lines != 1) {
			msgcgi("h e", "dst");
			return (WRUNKNOWN);
		}
		cfg.current_time.sum = sumbuf[0] ? 1 : 0;

		/* Jour de la semaine. */

		if (blocks[4].lines != 1 || blocks[5].lines != 1
		 || blocks[6].lines != 1 || blocks[7].lines != 1
		 || blocks[8].lines != 1 || blocks[9].lines != 1) {
			msgcgi("g [1-7]", "day of week");
			return (WRUNKNOWN);
		}
		cfg.current_time.dow = sembuf1[0] ? 0 : sembuf2[0] ? 1 :
			sembuf3[0] ? 2 : sembuf4[0] ? 3 : sembuf5[0] ? 4 :
			sembuf6[0] ? 5 : 6;

		/* Mode DHCP activé ou non. */

		if (blocks[10].lines != 1) {
			msgcgi("r d", "dhcp enabled");
			return (WRUNKNOWN);
		}
		cfg.more.ip.dhcp_enabled = dhcpbuf[0] ? 1 : 0;

		/* IP fixe/courante. */

		if (blocks[11].lines != 1 || sscanf(ipbuf, "%hhu%hhu%hhu%hhu",
			&cfg.more.ip.ip[0], &cfg.more.ip.ip[1], &cfg.more.ip.ip[2],
			&cfg.more.ip.ip[3]) < 4) {
			msgcgi("r i", "current ip");
			return (WRUNKNOWN);
		}

		/* Masque de sous-réseau en configuration fixe. */

		if (blocks[12].lines != 1 || sscanf(msbuf, "%hhu%hhu%hhu%hhu",
			&cfg.more.ip.mask[0], &cfg.more.ip.mask[1], &cfg.more.ip.mask[2],
			&cfg.more.ip.mask[3]) < 4) {
			msgcgi("r m", "netmask");
			return (WRUNKNOWN);
		}

		/* Passerelle par défaut en configuration fixe. */

		if (blocks[13].lines != 1 || sscanf(gwbuf, "%hhu%hhu%hhu%hhu",
			&cfg.more.ip.gw[0], &cfg.more.ip.gw[1], &cfg.more.ip.gw[2],
			&cfg.more.ip.gw[3]) < 4) {
			msgcgi("r g", "gateway");
			return (WRUNKNOWN);
		}

		/* Serveur DNS primaire. */

		if (blocks[14].lines != 1 || sscanf(dpbuf, "%hhu%hhu%hhu%hhu",
			&cfg.more.ip.dns1[0], &cfg.more.ip.dns1[1], &cfg.more.ip.dns1[2],
			&cfg.more.ip.dns1[3]) < 4) {
			msgcgi("r p", "primary dns");
			return (WRUNKNOWN);
		}

		/* Serveur DNS secondaire. */

		if (blocks[15].lines != 1 || sscanf(dsbuf, "%hhu%hhu%hhu%hhu",
			&cfg.more.ip.dns2[0], &cfg.more.ip.dns2[1], &cfg.more.ip.dns2[2],
			&cfg.more.ip.dns2[3]) < 4) {
			msgcgi("r s", "secon. dns");
			return (WRUNKNOWN);
		}

		/* Adresse MAC. */

		if (blocks[16].lines != 1 || sscanf(macbuf, "%hhu%hhu%hhu%hhu%hhu%hhu",
			&cfg.more.ip.mac[0], &cfg.more.ip.mac[1], &cfg.more.ip.mac[2],
			&cfg.more.ip.mac[3], &cfg.more.ip.mac[4],
			&cfg.more.ip.mac[5]) < 6) {
			msgcgi("r a", "mac addr.");
			return (WRUNKNOWN);
		}
	}

	*cfgp = &cfg;
	return (WROK);
}

/* ---
 * Gestion des compteurs.
 * --- */

/* L'abstraction de serveur WES propose les compteurs suivants (avec
 * identifiants) :
 * [ 1] Compteur branché via télé-info 1 (TIC1).
 * [ 2] Compteur branché via télé-info 2 (TIC2).
 * [ 3] Pince ampèremétrique 1 (PCE1).
 * [ 4] Pince ampèremétrique 2 (PCE2).
 * [ 5] Pince ampèremétrique 3 (PCE3).
 * [ 6] Pince ampèremétrique 4 (PCE4).
 * [ 7] Compteur d'impulsions 1 (PLS1).
 * [ 8] Compteur d'impulsions 2 (PLS2).
 * [ 9] Compteur d'impulsions 3 (PLS3).
 * [10] Compteur d'impulsions 4 (PLS4). */

/* `getwestic()`: récupération de la configuration d'un compteur branché
 * via télé-information. */

static int getwestic(wes_t *wes, int tic_id, wesmeterflags_t flags,
	wesmetercfg_t **ticp)
{
	static wesmetercfg_t cfg;
	static char name[20], bdpvu[21], bdpvp[21];

	(void)flags;

	cfg.free = NULL;

	if (tic_id != 0 && tic_id != 1)
		return (WROP);

	cfg.type = WESMETERTYPE_TELEINFO;
	cfg.what = WESMETERWHAT_ELEC;

	{
		char isrd[5], prod2[5], abo[10], pror[5],
			tarif0[10], tarif1[10], tarif2[10], tarif3[10],
			tarif4[10], tarif5[10],
			bdpve[5], bdpvh[10], bdpvm[10];

		wescgi_t blocks[] = {
			/* Nom donné via l'interface au compteur.
			 * `e n`: TIC1, `e N`: TIC2 */
			WESCGI(tic_id ? "e N" : "e n", "%s", name, 20),

			/* Selon si le compteur est lu (checked) ou non.
			 * `G t`: TIC1, `G T`: TIC2 */
			WESCGI(tic_id ? "G T" : "G t", "%.1s", isrd, 5),

			/* Selon si l'on est en mode consommation ou production (pour
			 * le compteur 2 uniquement).
			 * `G C`: mode consommation (checked).
			 * `G p`: mode production (checked). */
			WESCGI("G p", "%.1s", prod2, 5),

			/* Coût fixe en euros du compteur. */
			WESCGI(tic_id ? "C A" : "C a", "%0.2f", abo, 10),

			/* Prorata abonnement au prix du kWh (checked) ou non ? */
			WESCGI(tic_id ? "C p2" : "C p1", "%.1s", pror, 5),

			/* Tarifs pour les différents abonnements. */
			WESCGI(tic_id ? "C T0" : "C t0", "%0.5f", tarif0, 10),
			WESCGI(tic_id ? "C T1" : "C t1", "%0.5f", tarif1, 10),
			WESCGI(tic_id ? "C T2" : "C t2", "%0.5f", tarif2, 10),
			WESCGI(tic_id ? "C T3" : "C t3", "%0.5f", tarif3, 10),
			WESCGI(tic_id ? "C T4" : "C t4", "%0.5f", tarif4, 10),
			WESCGI(tic_id ? "C T5" : "C t5", "%0.5f", tarif5, 10),

			/* Si mode production, paramètres BDPV (pour le compteur 2
			 * uniquement).
			 * `H a`: activé (checked).
			 * `H i`: identifiant.
			 * `H p`: mot de passe.
			 * `H h`: heure d'envoi.
			 * `H m`: minute d'envoi. */
			WESCGI("H a", "%.1s", bdpve, 5),
			WESCGI("H i", "%s", bdpvu, 21),
			WESCGI("H p", "%s", bdpvp, 21),
			WESCGI("H h", "%d", bdpvh, 10),
			WESCGI("H m", "%d", bdpvm, 10)
		};

		GETCGI(16, blocks)

		/* ---
		 * Décodage des éléments.
		 * --- */

		/* Nom du compteur. */

		if (blocks[0].lines != 1) {
			msgcgi("e [nN]", "teleinfo name");
			return (WRUNKNOWN);
		}

		cfg.name = name;

		/* Le compteur est-il lu ? */

		if (blocks[1].lines != 1) {
			msgcgi("G [tT]", "teleinfo is read");
			return (WRUNKNOWN);
		}
		cfg.is_read = isrd[0] ? 1 : 0;

		/* Mode de consommation/production. */

		if (blocks[2].lines != 1) {
			msgcgi("G p", "prod cpt. 2");
			return (WRUNKNOWN);
		}
		cfg.mode = tic_id && prod2[0] ? WESMETERMODE_PROD : WESMETERMODE_CONSO;

		/* Tarifs de base. */

		if (blocks[2].lines != 1
		 || sscanf(abo, "%lf", &cfg.fixed_cost) < 1) {
			msgcgi("C [aA]", "fixed costs");
			return (WRUNKNOWN);
		}

		if (blocks[3].lines != 1) {
			msgcgi("C p[12]", "prorata");
			return (WRUNKNOWN);
		}
		cfg.prorata = pror[0] ? 1 : 0;

		/* Tarifs détaillés. */

		{
			double tf0, tf1, tf2, tf3, tf4, tf5;

			if (blocks[4].lines != 1
			 || sscanf(tarif0, "%lf", &tf0) < 1) {
				msgcgi("C [tT]0", "tarif 0");
				return (WRUNKNOWN);
			}
			if (blocks[5].lines != 1
			 || sscanf(tarif1, "%lf", &tf1) < 1) {
				msgcgi("C [tT]1", "tarif 1");
				return (WRUNKNOWN);
			}
			if (blocks[6].lines != 1
			 || sscanf(tarif2, "%lf", &tf2) < 1) {
				msgcgi("C [tT]2", "tarif 2");
				return (WRUNKNOWN);
			}
			if (blocks[7].lines != 1
			 || sscanf(tarif3, "%lf", &tf3) < 1) {
				msgcgi("C [tT]3", "tarif 3");
				return (WRUNKNOWN);
			}
			if (blocks[8].lines != 1
			 || sscanf(tarif4, "%lf", &tf4) < 1) {
				msgcgi("C [tT]4", "tarif 4");
				return (WRUNKNOWN);
			}
			if (blocks[9].lines != 1
			 || sscanf(tarif5, "%lf", &tf5) < 1) {
				msgcgi("C [tT]5", "tarif 5");
				return (WRUNKNOWN);
			}

			cfg.more.teleinfo.cost_base_th = tf0;
			cfg.more.teleinfo.cost_hchp_hc = tf0;
			cfg.more.teleinfo.cost_hchp_hp = tf1;
			cfg.more.teleinfo.cost_tempo_hcjb = tf0;
			cfg.more.teleinfo.cost_tempo_hpjb = tf1;
			cfg.more.teleinfo.cost_tempo_hcjw = tf2;
			cfg.more.teleinfo.cost_tempo_hpjw = tf3;
			cfg.more.teleinfo.cost_tempo_hcjr = tf4;
			cfg.more.teleinfo.cost_tempo_hpjr = tf5;
			cfg.more.teleinfo.cost_ejp_hn = tf0;
			cfg.more.teleinfo.cost_ejp_pm = tf1;
		}

		/* Configuration BDPV (si compteur 2, hardcodé dans le serveur WES),
		 * i.e. détails de connexion au site BDPV. */

		if (tic_id == 0) {
			cfg.more.teleinfo.bdpv_enabled = 0;
			cfg.more.teleinfo.bdpv_hour = 0;
			cfg.more.teleinfo.bdpv_min = 0;
			cfg.more.teleinfo.bdpv_username = "";
			cfg.more.teleinfo.bdpv_password = "";
		} else {
			/* Selon si BDPV est activé ou non. */

			if (blocks[10].lines != 1) {
				msgcgi("H a", "bdpv enabled");
				return (WRUNKNOWN);
			}
			cfg.more.teleinfo.bdpv_enabled = bdpve[0] ? 1 : 0;

			/* Utilisateur et mot de passe du compte BDPV. */

			if (blocks[11].lines != 1) {
				msgcgi("H i", "bdpv username");
				return (WRUNKNOWN);
			}
			if (blocks[12].lines != 1) {
				msgcgi("H p", "bdpv password");
				return (WRUNKNOWN);
			}

			cfg.more.teleinfo.bdpv_username = bdpvu;
			cfg.more.teleinfo.bdpv_password = bdpvp;

			/* Heure et minute d'envoi. */

			if (blocks[13].lines != 1 || sscanf(bdpvh, "%d",
				&cfg.more.teleinfo.bdpv_hour)) {
				msgcgi("H h", "bdpv send hour");
				return (WRUNKNOWN);
			}
			if (blocks[14].lines != 1 || sscanf(bdpvm, "%d",
				&cfg.more.teleinfo.bdpv_min)) {
				msgcgi("H m", "bdpv send min.");
				return (WRUNKNOWN);
			}
		}
	}

	*ticp = &cfg;
	return (WROK);
}

/* `getwesmeter()`: récupération de la configuration d'un compteur. */

static int getwesmeter(wes_t *wes, int meter_id, wesmetertype_t expected,
	wesmeterflags_t flags, wesmetercfg_t **meterp)
{
	switch (meter_id) {
	case 1: case 2:
		/* Compteurs branchés via télé-information numéro 0 à 1. */

		if (expected != WESMETERTYPE_TELEINFO)
			return (WROP);
		return (getwestic(wes, meter_id - 1, flags, meterp));

#if 0 /* TODO: fonctions associées */
	case 3: case 4: case 5: case 6:
		/* Pinces ampèremétriques numéro 0 à 3. */

		if (expected != WESMETERTYPE_AMPER)
			return (WROP);
		return (getwespce(wes, meter_id - 3, flags, meterp));

	case 7: case 8: case 9: case 10:
		/* Compteurs d'impulsions numéro 0 à 3. */

		if (expected != WESMETERTYPE_PULSE)
			return (WROP);
		return (getwespls(wes, meter_id - 7, flags, meterp));
#endif

	default:
		/* Numéro inconnu ! */
		break;
	}

	return (WRVAL);
}

/* ---
 * Listage des compteurs.
 * --- */

struct wes_iter {
	int current;
	int left;
};

/* `getwesmeteriter()`: prepare the iterator. */

static int getwesmeteriter(wes_t *wes, wesmetertype_t type,
	struct wes_iter **iterp)
{
	struct wes_iter *iter;
	int cur, left;

	(void)wes; /* mêmes compteurs pour tous les WES de ce type */

	switch (type) {
	case WESMETERTYPE_NONE:
		cur = 1;
		left = 10;
		break;
	case WESMETERTYPE_AMPER:
		cur = 3;
		left = 4;
		break;
	case WESMETERTYPE_PULSE:
		cur = 7;
		left = 4;
		break;
	case WESMETERTYPE_TELEINFO:
		cur = 1;
		left = 2;
		break;
	default:
		cur = 0;
		left = 0;
	}

	if (!(iter = (*iterp = malloc(sizeof(*iter)))))
		return (WRALLOC);
	iter->current = cur;
	iter->left = left;

	return (WROK);
}

/* `getwesmeternext()`: get the next meter. */

static int getwesmeternext(struct wes_iter *iter, int *id,
	wesmetertype_t *type)
{
	if (!iter->left)
		return (WRITER);
	*id = iter->current;
	iter->current++;
	iter->left--;

	switch (*id) {
	case 1: case 2:
		*type = WESMETERTYPE_TELEINFO;
		break;
	case 3: case 4: case 5: case 6:
		*type = WESMETERTYPE_AMPER;
		break;
	case 7: case 8: case 9: case 10:
		*type = WESMETERTYPE_PULSE;
		break;
	default:
		*type = WESMETERTYPE_NONE;
		break;
	}

	return (WROK);
}

static int skipwesmeters(struct wes_iter *iter, int number)
{
	if (number > iter->left)
		number = iter->left;
	iter->current += number;
	iter->left -= number;

	return (WROK);
}

static int delwesmeteriter(struct wes_iter *iter)
{
	free(iter);
	return (WROK);
}

/* ---
 * Définition de l'interface.
 * --- */

static wesif_t ipif = {
	WESTYPE_IP, free_ip_wes,

	/* Interaction avec la configuration. */

	getwescfg, NULL,

	/* Interaction avec les compteurs. */

	getwesmeter, NULL, NULL,

	/* Listage des compteurs. */

	(wesif_getmeteriter_t *)getwesmeteriter,
	(wesif_getmeternext_t *)getwesmeternext,
	(wesif_skipmeters_t *)skipwesmeters,
	(wesif_delmeteriter_t *)delwesmeteriter
};
