/* ****************************************************************************
 * internals.h -- déclarations et définitions internes au démon.
 * Copyright (C) 2018 Thomas Touhey <thomas@touhey.fr>
 *
 * This project is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the
 * CeCILL license as circulated by CEA, CNRS and INRIA at the
 * following URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the project's author, the holder of the
 * economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also therefore
 * means that it is reserved for developers and experienced professionals
 * having in-depth computer knowledge. Users are therefore encouraged to load
 * and test the software's suitability as regards their requirements in
 * conditions enabling the security of their systems and/or data to be
 * ensured and, more generally, to use and operate it in the same conditions
 * as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 * ************************************************************************* */
#ifndef  LOCAL_INTERNALS_H
# define LOCAL_INTERNALS_H 2018030201
# include <stddef.h>
# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <ctype.h>
# include <errno.h>

/* ---
 * Codes de retour internes.
 * --- */

# define WROK        0 /* Tout va bien dans le meilleur des mondes. */
# define WRUNKNOWN   1 /* Une erreur inconnue s'est produite. */
# define WROP        2 /* Cette opération est interdite. */
# define WRNOSYS     3 /* Cette opération n'est pas encore implémentée. */
# define WRIMPL      3 /* (alias) */
# define WRALLOC     4 /* Une allocation mémoire a échoué. */
# define WRSPACE     5 /* Pas assez d'espace pour stocker quelque chose */
# define WRUNAVAIL   6 /* Ce medium est indisponible (éteint ?) */
# define WRVAL       7 /* L'un des arguments est invalide. */

# define WRNOHOST   10 /* Le serveur n'a pas pu être atteint. */
# define WRCONN     11 /* La connexion a été réinitialisée. */
# define WRNOTFOUND 12 /* Un élément n'a pas été trouvé. */
# define WREXISTS   13 /* Un élément existe déjà. */
# define WRNOITER   14 /* Aucun élément restant dans l'itérateur. */
# define WRITER     14 /* (alias) */
# define WRNOID     15 /* Il ne reste plus d'identifiants disponibles. */
# define WRTIMEOUT  16 /* Un délai a expiré. */
# define WRAUTH     17 /* Une authentification a échoué. */

/* ---
 * Niveaux de logging.
 * --- */

# define WLDEBUG   0 /* informations de débuggage. */
# define WLINFO    0 /* (idem) */
# define WLWARN   10 /* avertissements. */
# define WLERROR  20 /* erreurs non fatales. */
# define WLFATAL  30 /* erreurs fatales. */
# define WLNOTICE 40 /* messages type copyright etc. */
# define WLNONE   40 /* aucune erreur (pour la définition du seuil). */

/* Obtention du nom d'une fonction. */

# if defined(__STDC_VERSION__) && __STDC_VERSION__ >= 199901L
#  define LOGFUNC __func__
# elif !defined(__STRICT_ANSI__) && GNUC_PREREQ(2, 0)
#  define LOGFUNC __FUNCTION__
# else
#  define LOGFUNC NULL
# endif

/* Non seulement les versions en minuscules sont plus faciles à taper, mais
 * elles sont faites pour être utilisées avec les macros `msg` et `mem`.
 *
 * (et parce que les majuscules c'est un peu lourd à taper, je fais aussi
 * la version en minuscules. en fait depuis ça a pris un rôle différent,
 * mais bon) */

# define wldebug   0, LOGFUNC
# define wlinfo    0, LOGFUNC
# define wlwarn   10, LOGFUNC
# define wlerror  20, LOGFUNC
# define wlfatal  30, LOGFUNC
# define wlnotice 40, LOGFUNC

typedef int loglevel_t;

/* ---
 * Gestion des objets alloués.
 * --- */

/* Les callbacks du serveur correspondant aux différents appels doivent
 * retourner l'objet avec les chaînes allouées etc afin que la réponse
 * soit construite en XDR puis renvoyée. Seulement, puisqu'ils ont été
 * alloués pour cette requête, ils doivent être libérés après que la réponse
 * XDR aie été créée.
 *
 * Pour cela, on garde une trace du dernier objet alloué dans la ressource
 * de type WES de base. Cet objet peut être générique puisque le callback
 * de libération de l'objet est toujours à la même position quelle que soit
 * la ressource retournée : en première position. */

typedef int wesfreemem_t (void*);

/* ---
 * Définition d'un objet du démon.
 * --- */

/* Les objets du démon sont organisés en un arbre, avec parents et enfants,
 * en listes chaînées avec sentinelles.
 * Le dernier identifiant (ID_MAX + 1) doit pouvoir être atteint puisqu'il
 * s'agit de celui de la sentinelle. */

# define ID_MAX 32766

# define WESOBJTYPE_WES    1
# define WESOBJTYPE_METER  2
# define WESOBJTYPE_SENSOR 3

# ifndef WESIF_DEFINITIONS
#  define WESIFBASENAME wesif
#  define WESIFBASENAMT wesif_t
# else
#  define WESIFBASENAME wesifbase
#  define WESIFBASENAMT wesifbase_t
# endif

typedef int wesif_free_t (wes_t *);

typedef struct wesifacebase {
	wesif_free_t *free;
} wesifacebase_t;

typedef struct wesobjbase {
	/* Identifiant utilisé dans le protocole d'interaction avec le démon,
	 * si l'objet est valide, suivi du type et d'alignement. */

	int valid, id;
	int type, _reserved;

	/* Prochain WES dans la liste chaînée pour le même niveau.
	 * Aussi, pointeur vers le pointeur qui pointe sur cet élément dans
	 * l'élément précédent, pour pouvoir le mettre à jour en cas de
	 * changement. */

	struct wesobj  *next;
	struct wesobj **prev;
	struct wesobj  *parent;
	struct wesobj  *children;

	/* Interface. */

	wesif_t const *iface;

	/* Nom de la ressource (toutes les ressources en ont une). */

	char *name;
} wesobjbase_t;

/* ---
 * Définition des structures utilisées pour la liste.
 * --- */

/* Voici la structure de la liste.
 * En plus du lien vers le début des deux chaînes, cette structure contient
 * de quoi créer plus rapidement un élément avec un identifiant unique. */

typedef struct {
	/* Lien vers le premier élément. */

	wesobjbase_t *first;

	/* Dernier identifiant inséré.
	 * Ceci permet de ne pas réutiliser directement les identifiant
	 * supprimés avant d'avoir fait un tour. */

	int last_id;

	/* Nombre de nœuds actuellement gérés. */

	int count;

	/* Base */

	WESBASENAMT sentinel;
} weslist_t;

/* On peut vouloir itérer sur le contenu de la liste, pour chercher un
 * élément ou tout simplement lister les ressources disponibles. */

# define WIVALID 18988

typedef struct {
	/* Le prochain élément à renvoyer. */

	wes_t *current;

	/* Histoire de vérifier qu'on ne tombe pas sur un itérateur
	 * pas initialisé. */

	int status;

	int wesiter__align;
} wesiter_t;

/* ---
 * Utilitaires.
 * --- */

/* Horodate (date et heure).
 * Contient également les données concernant le fuseau horaire. */

typedef struct wesdt {
	int year; /* e.g. 2018 */
	int mon; /* 1 à 12 */
	int dom; /* 1 à 31 */
	int hour; /* 0 à 23 */
	int min; /* 0 à 59 */
	int sec; /* 0 à 60 (leap seconds) */
	int dow; /* 0 à 6 (lundi à dimanche) */

	int tzhour, tzmin; /* GMT+HH:MM, e.g. 1 puis 45 pour GMT+01:45 */
	int sum; /* « summer time » (heure d'été), 0 si heure d'hiver */
} wesdt_t;

/* Adresse réseau.
 * Pour le moment, seules les adresses IPv4 et IPv6 sont gérées.
 * Un appareil peut avoir à la fois une adresse IPv4 et une adresse IPv6. */

# define WNONET 0 /* entrée invalide */
# define WINET  1 /* ipv4 and ipv6 */

# define HASIPv4  1 /* option pour `data.inet.has`: a une adresse ipv4 */
# define HASIPv6  2 /* option pour `data.inet.has`: a une adresse ipv6 */

typedef struct {
	int type;
	union {
		struct {
			int has;
			unsigned char ipv4[4];
			unsigned char ipv6[16];
		} inet;
	} data;
} wesaddr_t;

/* ---
 * Définition des options concernant les serveurs WES.
 * --- */

/* Types de serveurs WES existants. */

typedef enum westype {
	WESTYPE_NONE,
	WESTYPE_DUMMY,
	WESTYPE_IP
} westype_t;

/* Configuration du serveur WES, avec type des paramètres. */

# define WESCFG_NAME    1 /* définir le nom */
# define WESCFG_TIME    2 /* définir l'horodate actuelle. */

# define WESCFG_DHCP    4 /* définir le mode DHCP ou non. */
# define WESCFG_IP      8 /* définir l'IP fixe actuelle */
# define WESCFG_MASK   16 /* définir le masque de sous-réseau actuel */
# define WESCFG_GW     32 /* définir la passerelle par défaut actuelle */
# define WESCFG_DNS1   64 /* définir le serveur de noms primaire */
# define WESCFG_DNS2  128 /* définir le serveur de noms secondaire */
# define WESCFG_MAC   256 /* définir l'adresse MAC */

typedef unsigned long wescfgflags_t;

typedef struct wescfg {
	wesfreemem_t *free;

	/* Nom de la ressource (géré par le serveur pour le stockage).
	 * N'est pas nécessairement unique. */

	char *name;

	/* Horodate actuelle. */

	wesdt_t current_time;

	/* Plus d'informations selon le type de la ressource. */

	union {
		/* Pour les ressources de type IP. */

		struct {
			/* Paramètres réseau.
			 * Ne gère que l'IPv4 pour le moment, puisque c'est tout ce que
			 * j'ai besoin de gérer. */

			int dhcp_enabled;
			unsigned char ip[4];
			unsigned char mask[4];
			unsigned char gw[4];
			unsigned char dns1[4];
			unsigned char dns2[4];

			unsigned char mac[6]; /* read-only! */

			/* TODO: NTP ? */
		} ip;
	} more;
} wescfg_t;

/* ---
 * Configuration des compteurs (télé-info, impulsions,
 * pinces ampèremétriques, …).
 * --- */

/* Type de compteur.
 * `NONE`: aucun, informations génériques uniquement.
 * `TELEINFO`: compteur branché via le protocole télé-informatique d'Enedis.
 * `AMPER`: pinces ampèremétriques.
 * `PULSE`: compteur d'impulsions. */

typedef enum wesmetertype {
	WESMETERTYPE_NONE,
	WESMETERTYPE_AMPER,
	WESMETERTYPE_PULSE,
	WESMETERTYPE_TELEINFO
} wesmetertype_t;

/* Mode du compteur électrique.
 * `CONSO`: consommation.
 * `PROD`: production (panneaux solaires, …). */

typedef enum wesmetermode {
	WESMETERMODE_CONSO,
	WESMETERMODE_PROD
} wesmetermode_t;

/* Grandeur mesurée par le compteur.
 * `UNKNOWN`: inconnu.
 * `WATER`: eau froide/chaude.
 * `GAS`: gaz.
 * `ELEC`: électricité.
 * `FUEL`: fioul. */

typedef enum wesmeterwhat {
	WESMETERWHAT_UNKNOWN,
	WESMETERWHAT_WATER,
	WESMETERWHAT_GAS,
	WESMETERWHAT_ELEC,
	WESMETERWHAT_FUEL
} wesmeterwhat_t;

/* Unité utilisée pour les valeurs du compteur.
 * `UNKNOWN`: inconnu.
 * `WH`: watts par heure (Wh).
 * `KWH`: kilo-watts par heure (kWh).
 * `LITER`: litres (L).
 * `CUBICMETER`: mètres cube (m³). */

typedef enum wesmeterunit {
	WESMETERUNIT_UNKNOWN,
	WESMETERUNIT_WH,
	WESMETERUNIT_KWH,
	WESMETERUNIT_LITER,
	WESMETERUNIT_CUBICMETER
} wesmeterunit_t;

/* Type de tarif pour le protocole télé-informatique d'ERDF.
 * `BASE`:  forfait Base (toutes heures), le prix du kWh ne varie pas en
 *          fonction de la période de consommation ;
 * `HCHP`:  forfait Heures Pleines Heures Creuses, le prix du kWh varie
 *          en fonction de l'heure de la journée ;
 * `TEMPO`: forfait Temporaire, le prix du kWh varie en fonction de l'heure
 *          de la journée, ainsi que de la couleur de la journée ;
 * `EJP`:   forfait Effacement de Jour de Pointe, le prix du kWh varie en
 *          fonction de la journée. */

typedef enum westictarif {
	WESTICTARIF_BASE,
	WESTICTARIF_HCHP,
	WESTICTARIF_EJP,
	WESTICTARIF_TEMPO
} westictarif_t;

/* Période tarifaire pour le protocole télé-informatique d'ERDF.
 * La signification des valeurs dans cette énumération dépend du forfait
 * souscrit pour ce compteur.
 * Cette structure est pensée comme un champ de bits puisque les
 * valeurs peuvent couvrir plusieurs périodes.
 *
 * Forfait BASE :
 * `TH`: toutes heures.
 *
 * Forfait HCHP :
 * `HC`: heures creuses.
 * `HP`: heures pleines.
 *
 * Forfait TEMPO :
 * `HC`: heures creuses.
 * `HP`: heures pleines.
 * `JB`: jours bleus.
 * `JW`: jours blancs.
 * `JR`: jours rouges.
 *
 * Forfait EJP :
 * `HN`: heures normales.
 * `PM`: pointes mobiles. */

typedef enum westicperiod {
	WESTICPERIOD_TH =  1,

	WESTICPERIOD_HC =  1,
	WESTICPERIOD_HP =  2,

	WESTICPERIOD_JB =  4,
	WESTICPERIOD_JW =  8,
	WESTICPERIOD_JR = 16,

	WESTICPERIOD_HN =  1,
	WESTICPERIOD_PM =  2
} westicperiod_t;

/* Méthode de mesure pour les compteurs à impulsions.
 * `ILS`: impulsions mécaniques.
 * `ELEC`: impulsions électroniques. */

typedef enum wesplsmethod {
	WESPLSMETHOD_ILS,
	WESPLSMETHOD_ELEC
} wesplsmethod_t;

/* Drapeaux d'éléments de la structure à récupérer/définir. */

# define WESMETER_NAME             1
# define WESMETER_READ             2
# define WESMETER_MODE             4
# define WESMETER_WHAT             8
# define WESMETER_TYPE            16
# define WESMETER_FIXED_COSTS     32
# define WESMETER_PRORATA         64

# define WESMETER_COST           128
# define WESMETER_VOLTAGE        256

# define WESMETER_METHOD         256
# define WESMETER_GROUP          512

# define WESMETER_COSTS_BASE     128
# define WESMETER_COSTS_HCHP     256
# define WESMETER_COSTS_TEMPO    512
# define WESMETER_COSTS_EJP     1024
# define WESMETER_BDPV_ENABLED  2048
# define WESMETER_BDPV_IDS      4096
# define WESMETER_BDPV_TIME     8192

typedef unsigned long wesmeterflags_t;

/* Configuration d'un compteur. */

# define cost_bbr_hcjb cost_tempo_hcjb
# define cost_bbr_hpjb cost_tempo_hpjb
# define cost_bbr_hcjw cost_tempo_hcjw
# define cost_bbr_hpjw cost_tempo_hpjw
# define cost_bbr_hcjr cost_tempo_hcjr
# define cost_bbr_hpjr cost_tempo_hpjr

typedef struct wesmetercfg {
	wesfreemem_t *free;

	/* Propriétés de base.
	 * `name`: nom du compteur.
	 * `type`: type du compteur.
	 * `is_read`: le compteur est-il lu ou ignoré par le système ?
	 * `mode`: mode de consommation/production du compteur.
	 * `what`: grandeur mesurée. */

	char *name;
	wesmetertype_t type;
	int is_read;
	wesmetermode_t mode;
	wesmeterwhat_t what;

	/* Informations concernant le coût.
	 * `fixed_cost`: coût fixe de l'abonnement, à l'année.
	 * `prorata`: l'abonement est-il au prorata du prix de l'unité ? */

	double fixed_cost;
	int prorata;

	/* Informations dépendant du type de compteur. */

	union {
		struct {
			/* Propriétés relatives aux compteurs de types pinces
			 * ampèremétriques.
			 * `voltage`: tension (en V).
			 * `cost`: coût de l'unité (en €). */

			int voltage;
			double cost;
		} amper;

		struct {
			/* Propriétés relatives aux compteurs d'impulsions.
			 * `method`: méthode de comptage d'impulsions.
			 * `group_size`: nombre d'impulsions par unité.
			 * `cost`: coût de l'unité. */

			wesplsmethod_t method;
			int group_size;
			double cost;
		} pulse;

		struct {
			/* Propriétés relatives aux compteurs branchés via le protocole
			 * de télé-information défini par Enedis.
			 * `cost_<abo>_<période>`: coût du kWh pendant une période
			 *                         lorsqu'un forfait est en vigueur.
			 * `bdpv_enabled`: envoyer (ou non) les données via BDPV.
			 * `bdpv_hour`, `bdpv_min`: heure d'envoi via BDPV. */

			double cost_base_th;
			double cost_hchp_hc;
			double cost_hchp_hp;
			double cost_tempo_hcjb;
			double cost_tempo_hpjb;
			double cost_tempo_hcjw;
			double cost_tempo_hpjw;
			double cost_tempo_hcjr;
			double cost_tempo_hpjr;
			double cost_ejp_hn;
			double cost_ejp_pm;

			int bdpv_enabled;
			int bdpv_hour, bdpv_min;
			char *bdpv_username;
			char *bdpv_password;
		} teleinfo;
	} more;
} wesmetercfg_t;

/* ---
 * Valeurs retournées par un compteur pour une période.
 * --- */

typedef struct wesmetervalues {
	wesfreemem_t *free;

	/* Valeurs de base.
	 * `type`: type du compteur (rappel).
	 * `uptime`: uptime de 0 à 1000 pour cette période.
	 * `count`: nombre d'unités détectées pendant cette période.
	 * `unit`: unité de mesure pour la valeur précédente. */

	wesmetertype_t type;
	int uptime;
	double count;
	wesmeterunit_t unit;

	/* Valeurs supplémentaires selon le type du compteur. */

	union {
		struct {
			/* Valeurs supplémentaires du compteur branché via
			 * télé-informatique.
			 *
			 * `adco`: numéro ADCO du compteur (un chiffre direct par case).
			 * `phases`: nombre de phases (monophasé: 1, triphasé: 3).
			 * `period`: périodes tarifaires sur la période.
			 * `isousc`: intensité souscrite.
			 * `pa`: puissance apparente (en VA).
			 * `iinst`: intensité instantanée pour chaque phase.
			 * `imax`: intensité maximale pour chaque phase. */

			unsigned char adco[12];
			int phases;
			westicperiod_t period;

			unsigned int isousc;
			unsigned int pa;
			unsigned int iinst[3];
			unsigned int imax[3];

			/* Index pour chacun des tarifs, avec le tarif en vigueur. */

			union {
				struct {
					unsigned long th;
				} base;
				struct {
					unsigned long hc;
					unsigned long hp;
				} hchp;
				struct {
					unsigned long hcjb;
					unsigned long hpjb;
					unsigned long hcjw;
					unsigned long hpjw;
					unsigned long hcjr;
					unsigned long hpjr;
				} tempo;
				struct {
					unsigned long hn;
					unsigned long pm;
				} ejp;
			} indexes;
		} teleinfo;
	} more;
} wesmetervalues_t;

/* ---
 * Configuration des capteurs (température, humidité, …).
 * --- */

/* Type de capteur.
 * `NONE`: aucun, informations génériques uniquement.
 * `1WIRE`: capteur branché via le bus 1-Wire. */

typedef enum wessensortype {
	WESSENSORTYPE_NONE,
	WESSENSORTYPE_1WIRE
} wessensortype_t;

/* Grandeur mesurée par le capteur.
 * `TEMPERATURE`: température.
 * `HUMIDITY`: humidité. */

typedef enum wessensorwhat {
	WESSENSORWHAT_TEMPERATURE = 1,
	WESSENSORWHAT_HUMIDITY
} wessensorwhat_t;

/* Unité utilisée pour les valeurs du capteur.
 * `CELSIUS`: degrés Celsius (°C).
 * `KELVIN`: degrés Kelvin (K).
 * `FARENHEIT`: degrés Farenheit (°F). */

typedef enum wessensorunit {
	WESSENSORUNIT_CELSIUS = 1,
	WESSENSORUNIT_KELVIN,
	WESSENSORUNIT_FARENHEIT
} wessensorunit_t;

/* Drapeaux d'éléments de la structure à récupérer/définir. */

# define WESSENSOR_NAME             1
# define WESSENSOR_WHAT             8
# define WESSENSOR_TYPE            16

# define WESSENSOR_1WIREID         128

typedef unsigned long wessensorflags_t;

/* Configuration d'un capteur. */

typedef struct wessensorcfg {
	wesfreemem_t *free;

	/* Propriétés de base.
	 * `name`: nom du capteur.
	 * `type`: type du capteur.
	 * `what`: grandeur mesurée. */

	char *name;
	wessensortype_t type;
	wessensorwhat_t what;

	union {
		struct {
			/* Propriétés relatives aux capteurs branchés via le bus 1-Wire.
			 * `id`: id 1-Wire du capteur. */

			unsigned char id[8];
		} onewire;
	} more;
} wessensorcfg_t;

/* ---
 * Valeurs retournées par un capteur pour une période.
 * --- */

typedef struct wessensorvalues {
	wesfreemem_t *free;

	/* Valeurs de base.
	 * `type`: type du compteur (rappel).
	 *
	 * `uptime`: uptime de 0 à 1000 pour cette période.
	 * `min`: minimum pour cette période.
	 * `avg`: moyenne pour cette période.
	 * `max`: maximum pour cette période.
	 * `unit`: unité de mesure pour les valeurs précédentes. */

	wessensortype_t type;
	int uptime;
	double count;
	wessensorunit_t unit;
} wessensorvalues_t;

/* ---
 * Définition des interfaces.
 * --- */

/* On peut interagir avec un serveur WES, selon où il est branché et d'autres
 * facteurs, de différentes façons pour exécuter la même opération.
 * L'interface représente cela.
 *
 * Le type `wes_t` est différent pour tout le monde, les headers de chaque
 * interface vont faire croire qu'ils ont tous la bonne définition.
 * Ils comportent tous une base commune, mais ont des besoins particuliers. */

struct wes;
typedef struct wes wes_t;

/* L'interface en elle-même est surtout constituée des différentes fonctions
 * à utiliser. Ces fonctions correspondent plus ou moins aux besoins du
 * serveur. */

typedef int wesif_getcfg_t (wes_t *, wescfgflags_t, wescfg_t **);
typedef int wesif_setcfg_t (wes_t *, wescfgflags_t, wescfg_t const *);

typedef int wesif_getmeter_t (wes_t *, int, wesmetertype_t, wesmeterflags_t,
	wesmetercfg_t **);
typedef int wesif_setmeter_t (wes_t *, int, wesmeterflags_t,
	wesmetercfg_t const *);
typedef int wesif_getmetervalues_t (wes_t *, wesmetervalues_t **);

typedef int wesif_getmeteriter_t (wes_t *, wesmetertype_t type, void **);
typedef int wesif_getmeternext_t (void *, int *, wesmetertype_t *type);
typedef int wesif_skipmeters_t (void *, int);
typedef void wesif_delmeteriter_t (void *);

typedef int wesif_getsensor_t (wes_t *, int, wessensortype_t, wessensorflags_t,
	wessensorcfg_t const *);
typedef int wesif_setsensor_t (wes_t *, int, wessensorflags_t,
	wessensorcfg_t const *);
typedef int wesif_getsensorvalues_t (wes_t *wes, wessensorvalues_t **);

typedef int wesif_getsensoriter_t (wes_t *wes, wessensortype_t type, void **);
typedef int wesif_getsensornext_t (void *, int *, wessensortype_t *type);
typedef int wesif_skipsensors_t (void *, int);
typedef void wesif_delsensoriter_t (void *);

typedef struct wesif {
	westype_t type;
	wesif_free_t *free;

	/* Interact with the WES configuration. */

	wesif_getcfg_t *getcfg;
	wesif_setcfg_t *setcfg;

	/* Interact with and list the meters. */

	wesif_getmeter_t *getmeter;
	wesif_setmeter_t *setmeter;
	wesif_getmetervalues_t *getmetervalues;

	wesif_getmeteriter_t *getmeteriter;
	wesif_getmeternext_t *getmeternext;
	wesif_skipmeters_t   *skipmeters;
	wesif_delmeteriter_t *delmeteriter;

	/* Interact with and list the sensors. */

	wesif_getsensor_t *getsensor;
	wesif_setsensor_t *setsensor;
	wesif_getsensorvalues_t *getsensorvalues;

	wesif_getsensoriter_t *getsensoriter;
	wesif_getsensornext_t *getsensornext;
	wesif_skipsensors_t   *skipsensors;
	wesif_delsensoriter_t *delsensoriter;
} wesif_t;

/* ---
 * Données de création.
 * --- */

/* Aux constructeurs de ressources de type WES, on passe toujours un set
 * de paramètres déterminé systématiquement à partir de ce que donne le client
 * (ne serait-ce que rempli avec les valeurs par défaut par le serveur).
 * Ce set de paramètres inclue des choses parfois spécifiques à certaines
 * interfaces. */

typedef struct wescreatedata {
	/* Les arguments correspondant à diverses interfaces.
	 * Peut-être que l'on changera pour un dictionnaire de chaînes un jour,
	 * qui sait ? */

	const char *http_username;
	const char *http_password;
	const char *ftp_username;
	const char *ftp_password;

	wesaddr_t addr;
} wescreatedata_t;

/* ---
 * Logging.
 * --- */

/* Fonctions principales.
 * Elles sont cachées derrière les macros en-dessous. */

extern void log_msg(loglevel_t loglevel, const char *func,
	const char *format, ...);
extern void log_mem(loglevel_t loglevel, const char *func,
	const void *m, size_t n);

/* Macros principales, qui redirigent vers les fonctions de log.
 * Utilisez les doubles parenthèses de la façon suivante :
 *
 *	msg((wldebug, "ma %schaîne de formattage %d", "merveilleuse ", 1));
 *	mem((wlwarn, ma_zone_de_memoire, la_taille_de_ma_zone_de_memoire));
 *
 * Il s'agit de macros variadiques, mais compatibles K&R/ANSI C. */

# define msg(ARGS) log_msg ARGS
# define mem(ARGS) log_mem ARGS

/* Définir les niveaux de logs, et les décoder à partir de chaînes. */

extern void set_log_level(loglevel_t log_level);
extern loglevel_t decode_log_level(const char *text);

/* ---
 * Gestion d'un serveur WES.
 * --- */

/* Création et ajout à la liste de serveurs WES de différents types. */

extern int add_ip_wes(weslist_t *list, wes_t **wesp, wesaddr_t const *addr,
	char *http_username, char *http_password,
	char *ftp_username, char *ftp_password);
extern int add_dummy_wes(weslist_t *list, wes_t **wesp);

/* Initialiser la base d'un serveur WES. */

extern int   init_wes_base(wes_t *wes, wesif_t const *iface);
extern int deinit_wes_base(wes_t *wes);

/* Définir le nom d'un serveur WES (dans sa base).
 * - `prep_name()`: préparation d'un nom en sanitizant (sans validation,
 *   utilisé pour récupérer des données depuis les WES surtout ;
 * - `prep_valid_name()`: préparation d'un nom en validant ou non,
 *   utilisé pour filtrer ce que donne le client ;
 * - `free_name()`: suppression d'un nom préparé par `prep_name()` ou
 *   `prep_valid_name()` ;
 * - `assign_name()`: assignation au nom de ce qu'on a fait juste avant ;
 * - `set_name()`: définir le nom avec `prep_name()` et `assign_name()`,
 *   en faisant `free_name()` sur l'ancien nom. */

extern int prep_name(char **dst, const char *src);
extern int prep_valid_name(char **dst, const char *src);
extern void free_name(char *name);

extern int assign_name(wes_t *wes, char *name);
extern int set_name(wes_t *wes, const char *name);

/* Initialisation des structures de données. */

extern int correct_cfg(wes_t *wes, wescfg_t *cfg, wescfgflags_t flags);
extern int correct_meter(wes_t *wes, wesmetercfg_t *meter,
	wesmetertype_t type, wesmeterflags_t flags);

/* ---
 * Gestion de la liste.
 * --- */

/* Initialiser et déinitialiser une liste.
 * Ces fonctions sont pensées pour que la liste soit déjà créée, en général
 * dans la stack. */

extern int   init_list(weslist_t *list);
extern int deinit_list(weslist_t *list);

/* Initialiser une tête. */

extern int init_obj_base(wesobjbase_t *obj, wesif_t const *iface);

/* Ces fonctions ajoutent et retirent un élément dans la liste chaînée.
 * `add_to_list()` ajoute un élément dans la liste.
 * `remove_in_list()` retire un élément de la liste et le supprime
 *                    ainsi que ses enfants s'il en a. */

extern int    add_to_list(weslist_t *list, wesobjbase_t *parent,
	wesobjbase_t *wes);
extern int remove_in_list(weslist_t *list, wesobj_t *wes);

extern int remove_in_list_using_id(weslist_t *list, int id);

/* Cette fonction vide la liste en supprimant les éléments. */

extern int  empty_list(weslist_t *list);

/* Cette fonction permet d'obtenir un élément en utilisant son
 * identifiant numérique. */

extern int get_in_list(weslist_t *list, wesobj_t **resp, int id);

/* Ces fonctions permettent d'itérer sur le contenu de la liste.
 * Il faut d'abord préparer l'itérateur avec `iterate_on_list()`,
 * puis itérer tant que la fonction ne renvoie pas `WRNOITER`,
 * ce qui veut dire qu'il n'y a plus d'éléments à dire. */

extern int iterate_on_list(wesiter_t *iter, weslist_t *list);
extern int get_next_iteration(wesiter_t *iter, wesobj_t **element);

/* ---
 * Gestion du démon en lui-même.
 * --- */

/* Cette fonction, utile pour le logging, donne l'énoncé d'une erreur,
 * ou NULL si on ne la connaît pas. */

extern const char *error_string(int err);

/* Cette fonction lance le serveur et écoute les requêtes.
 * Elle prend le contrôle du fil d'exécution.
 * Son code de retour est celui qui doit être utilisé pour le retour
 * du `main()`. */

extern int run_server(weslist_t *list);

#endif /* LOCAL_INTERNALS_H */
