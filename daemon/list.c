/* ****************************************************************************
 * list.c -- initialisation, gestion, utilisation de la liste de ressources.
 * Copyright (C) 2018 Thomas Touhey <thomas@touhey.fr>
 *
 * This project is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the
 * CeCILL license as circulated by CEA, CNRS and INRIA at the
 * following URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the project's author, the holder of the
 * economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also therefore
 * means that it is reserved for developers and experienced professionals
 * having in-depth computer knowledge. Users are therefore encouraged to load
 * and test the software's suitability as regards their requirements in
 * conditions enabling the security of their systems and/or data to be
 * ensured and, more generally, to use and operate it in the same conditions
 * as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 * ************************************************************************* */
#include "internals.h"

/* ---
 * Create and delete the list.
 * --- */

/* `init_list()`: initialiser une liste. */

int init_list(weslist_t *list)
{
	wes_t *sentinel;

	/* On fait toutes les affectations par défaut ici, avant les opérations
	 * à risque, pour que le `deinit_list()` puisse se faire même si
	 * cette fonction a renvoyé une erreur. */

	list->first = NULL;
	list->last_id = 0;
	list->count = 0;

	/* On utilise la sentinelle. */

	sentinel = &list->sentinel;
	list->first = sentinel;
	sentinel->valid = 0;
	sentinel->id = ID_MAX + 1;
	sentinel->next = NULL;
	sentinel->prev = &list->first;

	/* Notre liste est bonne ! */

	return (WROK);
}

/* `deinit_list()`: déinitialiser une liste. */

int deinit_list(weslist_t *list)
{
	int err;

	if (list->first) {
		/* On ne vide la liste que si la liste a été créée avec succès.
		 * `list->first` n'est à NULL que si `init_list()` a échoué. */

		err = empty_list(list);
		if (err)
			return (err);
	}

	return (WROK);
}

/* ---
 * Manage the list.
 * --- */

/* `add_to_list()`: ajouter un élément à la liste. */

int add_to_list(weslist_t *list, wes_t *element)
{
	wes_t **current;
	int id;

	/* On vérifie si tous les identifiants sont pris. */

	if (list->count == ID_MAX)
		return (WRNOID);

	/* On cherche où on en était.
	 * TODO: optimiser pour ne pas avoir à re-rechercher l'élément ?
	 * Seulement, n'est-ce pas dangereux si l'on n'utilise pas la
	 * technique de la copie comme précédemment ? (… et encore ?) */

	id = list->last_id;
	current = &list->first;

	while (*current && (*current)->id <= id)
		current = &(*current)->next;

	/* On cherche où insérer et avec quel identifiant. */

	while (1) {
		id++;
		if (id <= 0 || id > ID_MAX) {
			/* Overflow management */
			id = 1;
			current = &list->first;
		}

		if (!*current || id < (*current)->id)
			break;
		current = &(*current)->next;
	}

	/* On a trouvé l'identifiant et on sait où on doit remplacer.
	 * Alors remplaçons ! */

	msg((wlinfo, "setting id %d to new element", id));

	element->next = *current;
	element->prev = (*current)->prev;

	*element->prev = element;
	element->next->prev = &element->next;

	element->id = id;
	list->last_id = id;
	list->count++;

	return (WROK);
}

/* `remove_in_list()`: Supprimer un élément dans une liste. */

int remove_in_list(weslist_t *list, wes_t *element)
{
	if (!element->valid)
		return (WROP);

	*element->prev = element->next;
	element->next->prev = &(*element->prev)->next;

	(*element->iface->free)(element);
	list->count--;

	return (WROK);
}

/* `empty_list()`: Vider le contenu d'une liste. */

int empty_list(weslist_t *list)
{
	wes_t *wes, *next;
	int err;

	next = list->first;
	while ((wes = next)->valid) {
		next = wes->next;
		err = remove_in_list(list, wes);
		if (err != WROK)
			return (err);
	}

	return (WROK);
}

/* ---
 * Accès à la liste.
 * --- */

int get_in_list(weslist_t *list, wes_t **resp, int id)
{
	wes_t *current;

	current = list->first;
	for (; current->valid; current = current->next) {
		if (current->id >= id)
			break;
	}

	if (current->id > id)
		return (WRNOTFOUND);

	*resp = current;
	return (WROK);
}

/* `remove_in_list_using_id()`: Supprimer un élément dans une liste
 * en utilisant son identifiant numérique. */

int remove_in_list_using_id(weslist_t *list, int id)
{
	wes_t *element;
	int err;

	err = get_in_list(list, &element, id);
	if (err)
		return (err);

	err = remove_in_list(list, element);
	if (err)
		return (err);

	return (WROK);
}

/* ---
 * Itération sur la liste.
 * --- */

int iterate_on_list(wesiter_t *iter, weslist_t *list)
{
	iter->status = WIVALID;
	iter->current = list->first;
	return (WROK);
}

int get_next_iteration(wesiter_t *iter, wes_t **wesp)
{
	wes_t *current;

	if (iter->status != WIVALID)
		return (WROP);
	current = iter->current;
	if (!current->valid)
		return (WRNOITER);

	*wesp = current;
	iter->current = current->next;
	return (WROK);
}
