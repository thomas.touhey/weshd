/* ****************************************************************************
 * main.c -- fonction principale du démon de gestion de serveurs WES.
 * Copyright (C) 2018 Thomas Touhey <thomas@touhey.fr>
 *
 * This project is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the
 * CeCILL license as circulated by CEA, CNRS and INRIA at the
 * following URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the project's author, the holder of the
 * economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also therefore
 * means that it is reserved for developers and experienced professionals
 * having in-depth computer knowledge. Users are therefore encouraged to load
 * and test the software's suitability as regards their requirements in
 * conditions enabling the security of their systems and/or data to be
 * ensured and, more generally, to use and operate it in the same conditions
 * as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 * ************************************************************************* */
#include "internals.h"
#include <getopt.h>

static const char *version_message =
"weshd v" WESHD_VERSION " (licensed under CeCILL 2.1)\n"
"Maintained by " WESHD_MAINT ".\n"
"\n"
"This is free software; see the source for copying conditions.\n"
"There is NO warranty; not even for MERCHANTABILITY\n"
"or FITNESS FOR A PARTICULAR PURPOSE.\n";

static const char *help_message =
"syntax: weshd [-h] [-v]\n"
"\n"
"General options:\n"
"  -h                display this help message\n"
"  -v                display the version message\n"
"  -l <loglevel>     set the minimum log level to <loglevel>.\n"
"\n"
"Report bugs to " WESHD_MAINT ".\n";

/* `get_args()`: interface en ligne de commandes. */

static int get_args(int ac, char **av)
{
	int c;
	int help = 0, version = 0;
	const char *log_level = NULL;

	/* Récupération des options. */

	while ((c = getopt(ac, av, "hvl:")) != -1) switch (c) {
	case 'h':
		help = 1;
		break;
	case 'v':
		version = 1;
		break;
	case 'l':
		log_level = optarg;
		break;
	case '?':
		if (optopt == 'l')
			fprintf(stderr, "-l: expected argument\n");
		break;
	}

	/* Récupération des paramètres. */

	ac = ac - optind;
	/* av = &av[optind]; */
	if (ac)
		help = 1;

	/* Affichage des messages. */

	if (version) {
		fputs(version_message, stderr);
		return (-1);
	}
	if (help) {
		fputs(help_message, stderr);
		return (-1);
	}

	if (log_level)
		set_log_level(decode_log_level(log_level));

	return (0);
}

/* `main()`: fonction principale du démon. */

int main(int ac, char **av)
{
	weslist_t list;
	int ret;

	/* Décodage des arguments. */

	ret = get_args(ac, av);
	if (ret < 0)
		return (0);
	else if (ret > 0)
		return (ret);

	/* Initialisations globales. */

	curl_global_init(CURL_GLOBAL_DEFAULT);
	init_list(&list);

#if NOLOGO
#else
	/* Notice */

	msg((wlnotice, "weshd v" WESHD_VERSION
		" -- your WES server handling daemon."));
	msg((wlnotice, "Copyright (C) 2018 Thomas Touhey <thomas@touhey.fr>"));
	msg((wlnotice, ""));
	msg((wlnotice, "Maintained by " WESHD_MAINT "."));
	msg((wlnotice, "This is free software; see the source for "
		"copying conditions."));
	msg((wlnotice, "There is NO warranty; not even for MERCHANTABILITY"));
	msg((wlnotice, "or FITNESS FOR A PARTICULAR PURPOSE."));
	msg((wlnotice, ""));
#endif

	/* Lancement de l'application principale. */

	ret = run_server(&list);

	/* Déinitialisations globales. */

	deinit_list(&list);
	curl_global_cleanup();

	return (ret);
}
