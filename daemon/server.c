/* ****************************************************************************
 * server.c -- gestion du serveur, réception des requêtes.
 * Copyright (C) 2018 Thomas Touhey <thomas@touhey.fr>
 *
 * This project is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the
 * CeCILL license as circulated by CEA, CNRS and INRIA at the
 * following URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the project's author, the holder of the
 * economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also therefore
 * means that it is reserved for developers and experienced professionals
 * having in-depth computer knowledge. Users are therefore encouraged to load
 * and test the software's suitability as regards their requirements in
 * conditions enabling the security of their systems and/or data to be
 * ensured and, more generally, to use and operate it in the same conditions
 * as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 * ************************************************************************* */
#include "internals.h"
#include <signal.h>
#include <setjmp.h>

#include <arpa/inet.h>
#include <rpc/pmap_clnt.h>

#include <weshd.h> /* définitions du protocole depuis `weshd.x` */

/* ---
 * Utilitaires.
 * --- */

/* `addrstr()`: récupérer la chaîne correspondant à une adresse, pour
 * du logging. */

static const char *addrstr(struct in6_addr *src)
{
	static char str[INET6_ADDRSTRLEN + 1] = {0};

	return (inet_ntop(AF_INET6, src, str, sizeof(str)));
}

#define REQADDR addrstr(&svc_getcaller(req->rq_xprt)->sin6_addr)

/* Macro pour récupérer l'objet avec un ID. */

#define GETOBJ(ID, OBJ, RET, RESP) { \
	int getwes_err; \
	switch ((getwes_err = get_in_list(list, &OBJ, ID))) { \
	case WROK: \
		break; \
	case WRNOTFOUND: \
		msg((wlerror, "no server with id %d found", ID)); \
		RET = WESPRET_NOW; \
		return (RESP); \
	default: \
		msg((wlerror, "generic error while getting resource: %s", \
			error_string(getwes_err))); \
		RET = WESPRET_INT; \
		return (RESP); \
	}}

/* `set_obj_to_free()`: Définition du prochain objet à détruire.
 * Compense le fait que ce ne soient pas les callbacks ci-dessous qui
 * envoient la réponse et du coup ne puissent pas détruire les objets avant
 * de les envoyer au client.
 *
 * À ne surtout pas utiliser dans un contexte sur plusieurs fils d'exécution
 * (threads), évidemment… */

static void set_obj_to_free(void *vobj)
{
	static wesfreemem_t **lastobj = NULL;
	wesfreemem_t **obj = (void *)vobj;

	if (lastobj) {
		(**lastobj)(*((void **)lastobj));
		lastobj = NULL;
	}

	if (obj && *obj)
		lastobj = obj;
}

/* Ici, on stocke les ressources « globales » de sorte à ce que les
 * appels de services (svc) puissent les utiliser. */

static weslist_t *list;

/* On stocke également ce qu'il faut afin que les signaux du type
 * « arrête-toi » puissent être attrapés et que l'on quitte gracieusement. */

static jmp_buf endjmp;

/* ---
 * Requêtes de base.
 * --- */

/* `weshd_null_1_svc()`: Ne rien faire (sert à pinger le service). */

void *weshd_null_1_svc(void *argp, struct svc_req *req)
{
	static char val[4] = "ok!";

	(void)argp;
	(void)req;
	return ((void *)&val);
}

/* ---
 * Gestion des ressources WES existantes.
 * --- */

/* `gather_1_svc()`: Création de ressource, ou récupération de ressource
 * existante. */

wespret_with_id_t *gather_1_svc(wespoptions_t *args,
	struct svc_req *req)
{
	static wespret_with_id_t resp;
	const unsigned char *ip;
	wesaddr_t addr; int err;
	wespoptions_ip_t *ipopts;
	wes_t *wes;

	resp.ret = WESPRET_INT;

	msg((wlnotice, "(%s) register server", REQADDR));

	switch (args->type) {
	case WESPTYPE_DUMMY:
		/* On crée et ajoute directement la ressource de type dummy,
		 * sans faire de chichis. */

		err = add_dummy_wes(list, &wes);
		break;

	case WESPTYPE_IP:
		/* On prépare l'adresse IP. */

		ipopts = &args->wespoptions_t_u.ip;

		switch (ipopts->addr.type) {
		case WESPIPTYPE_4:
			ip = ipopts->addr.wespip_t_u.addr4;

			msg((wlnotice, "(%s) using IPv4 address %hhu.%hhu.%hhu.%hhu",
				REQADDR, ip[0], ip[1], ip[2], ip[3]));

			addr.type = WINET;
			addr.data.inet.has = HASIPv4;
			memcpy(addr.data.inet.ipv4, ip, 4);

			break;

		case WESPIPTYPE_6:
			ip = ipopts->addr.wespip_t_u.addr6;

			msg((wlnotice, "(%s) using IPv6 address "
				"%02hhX%02hhX:%02hhX%02hhX:%02hhX%02hhX:%02hhX%02hhX:"
				"%02hhX%02hhX:%02hhX%02hhX:%02hhX%02hhX:%02hhX%02hhX",
				REQADDR, ip[0], ip[1], ip[2], ip[3], ip[4], ip[5], ip[6],
				ip[7], ip[8], ip[9], ip[10], ip[11], ip[12], ip[13],
				ip[14], ip[15]));

			addr.type = WINET;
			addr.data.inet.has = HASIPv6;
			memcpy(addr.data.inet.ipv6, ip, 16);

			break;

		default:
			addr.type = WNONET;
			break;
		}

		/* On définit les identifiants. */

		err = add_ip_wes(list, &wes, &addr,
			ipopts->http_username, ipopts->http_password,
			ipopts->ftp_username, ipopts->ftp_password);
		break;

	default:
		/* Je ne connais pas ce type de WES. */

		resp.ret = WESPRET_VAL;
		return (&resp);
	}

	/* On regarde l'erreur retournée par la fonction dédiée de création et
	 * d'ajout à la liste. */

	switch (err) {
	case WROK:
		msg((wlinfo, "success in creating the resource!"));
		break;

	case WREXISTS:
		msg((wlinfo, "success in gathering existing resource!"));
		break;

	default:
		msg((wlerror, "error while adding: %s", error_string(err)));

		resp.ret = WESPRET_INT;
		return (&resp);
	}

	/* On a tout, on peut donner un petit sourire au client :) */

	resp.ret = WESPRET_OK;
	resp.id = wes->id;
	resp.name = wes->name;
	return (&resp);
}

/* `unregister_1_svc()`: Suppression de ressource. */

wespret_t *unregister_1_svc(wespid_t *id, struct svc_req *req)
{
	static wespret_t ret;

	msg((wlnotice, "(%s) delete server %d", REQADDR, *id));
	ret = WESPRET_INT;

	/* Suppression du WES en utilisant l'identifiant. */

	switch (remove_in_list_using_id(list, *id)) {
	case WROK:
		break;
	case WRNOTFOUND:
		msg((wlerror, "no server with id %d found", *id));
		ret = WESPRET_NOW;
		return (&ret);
	default:
		return (&ret);
	}

	/* Tout s'est bien passé ! */

	msg((wlerror, "deletion completed"));
	ret = WESPRET_OK;
	return (&ret);
}

/* `get_1_svc()`: Récupération de la configuration d'un serveur WES. */

wespret_with_wes_t *get_1_svc(wespid_with_wes_flags_t *args,
	struct svc_req *req)
{
	static wespret_with_wes_t resp;
	wescfg_t cfg, *cfgobj;
	wes_t *wes; int err;
	wescfgflags_t flags;
	wespdt_t *pdt;
	wespipmore_t *ipmoar;

	msg((wlnotice, "(%s) get server %d config", REQADDR, args->id));

	/* Récupération du WES. */

	GETWES(args->id, resp.ret, &resp)

	/* On convertit les flags du protocole au format interne (au cas où
	 * les deux diffèrent, ce qui n'est pas le cas au moment où j'écris
	 * ce bout de code). */

	flags = 0;
	if (args->to_get & WESP_NAME)
		flags |= WESCFG_NAME;
	if (args->to_get & WESP_TIME)
		flags |= WESCFG_TIME;

	switch (wes->iface->type) {
	case WESTYPE_IP:
		if (args->to_get & WESP_DHCP)
			flags |= WESCFG_DHCP;
		if (args->to_get & WESP_IP)
			flags |= WESCFG_IP;
		if (args->to_get & WESP_MASK)
			flags |= WESCFG_MASK;
		if (args->to_get & WESP_GW)
			flags |= WESCFG_GW;
		if (args->to_get & WESP_DNS1)
			flags |= WESCFG_DNS1;
		if (args->to_get & WESP_DNS2)
			flags |= WESCFG_DNS2;
		if (args->to_get & WESP_MAC)
			flags |= WESCFG_MAC;
		break;
	default:
		break;
	}

	/* Appel de la procédure correspondant à l'interface. */

	switch ((err = (*wes->iface->getcfg)(wes, flags, &cfgobj))) {
	case WROK:
		break;

	default:
		msg((wlerror, "getcfg error: %s", error_string(err)));
		resp.ret = WESPRET_INT;
		return (&resp);
	}

	/* Décodage. */

	memcpy(&cfg, cfgobj, sizeof(cfg));
	correct_cfg(wes, &cfg, flags);

	resp.wes.name = wes->name;
	pdt = &resp.wes.current_time;
	pdt->year = cfg.current_time.year;
	pdt->mon = cfg.current_time.mon;
	pdt->dom = cfg.current_time.dom;
	pdt->hour = cfg.current_time.hour;
	pdt->min = cfg.current_time.min;
	pdt->sec = cfg.current_time.sec;
	pdt->dow = cfg.current_time.dow;
	pdt->tzhour = cfg.current_time.tzhour;
	pdt->tzmin = cfg.current_time.tzmin;
	pdt->dst = cfg.current_time.sum;

	switch (wes->iface->type) {
	case WESTYPE_DUMMY:
		resp.wes.more.type = WESPTYPE_DUMMY;
		break;
	case WESTYPE_IP:
		resp.wes.more.type = WESPTYPE_IP;
		ipmoar = &resp.wes.more.wespmore_t_u.ip;
		ipmoar->dhcp_enabled = cfg.more.ip.dhcp_enabled;
		memcpy(ipmoar->ip, cfg.more.ip.ip, 4);
		memcpy(ipmoar->mask, cfg.more.ip.mask, 4);
		memcpy(ipmoar->gw, cfg.more.ip.gw, 4);
		memcpy(ipmoar->dns1, cfg.more.ip.dns1, 4);
		memcpy(ipmoar->dns2, cfg.more.ip.dns2, 4);
		memcpy(ipmoar->mac, cfg.more.ip.mac, 6);
		break;
	default:
		msg((wlerror, "invalid iftype: %d", wes->iface->type));
		resp.ret = WESPRET_INT;
		set_obj_to_free(cfgobj);
		return (&resp);
	}

	/* Tout est bien qui finit bien ! */

	resp.ret = WESPRET_OK;
	set_obj_to_free(cfgobj);
	return (&resp);
}

/* `set_1_svc()`: Définition de la configuration d'un serveur WES. */

wespret_t *set_1_svc(wespid_with_wes_t *args, struct svc_req *req)
{
	static wespret_t ret;
	wes_t *wes; wescfg_t cfg;
	wescfgflags_t setflags;
	wespipmore_t *ipmoar;
	int err;

	msg((wlnotice, "(%s) set server %d config", REQADDR, args->id));

	/* Récupération du WES. */

	GETWES(args->id, ret, &ret)

	/* Préparation des arguments,
	 * avec gestion des drapeaux. */

	ret = WESPRET_VAL;

	cfg.free = NULL;
	setflags = 0;

	if (args->wes.to_set & WESP_NAME)
		setflags |= WESCFG_NAME;
	if (args->wes.to_set & WESP_TIME)
		setflags |= WESCFG_TIME;

	switch (wes->iface->type) {
	case WESTYPE_DUMMY:
	case WESTYPE_NONE:
		if (args->wes.more.type != WESPTYPE_DUMMY)
			return (&ret);
		break;

	case WESTYPE_IP:
		if (args->wes.more.type != WESPTYPE_IP)
			return (&ret);

		if (args->wes.to_set & WESP_DHCP)
			setflags |= WESCFG_DHCP;
		if (args->wes.to_set & WESP_MASK)
			setflags |= WESCFG_MASK;
		if (args->wes.to_set & WESP_IP)
			setflags |= WESCFG_IP;
		if (args->wes.to_set & WESP_GW)
			setflags |= WESCFG_GW;
		if (args->wes.to_set & WESP_DNS1)
			setflags |= WESCFG_DNS1;
		if (args->wes.to_set & WESP_DNS2)
			setflags |= WESCFG_DNS2;

		ipmoar = &args->wes.more.wespmore_t_u.ip;
		cfg.more.ip.dhcp_enabled = ipmoar->dhcp_enabled;
		memcpy(cfg.more.ip.ip, ipmoar->ip, 4);
		memcpy(cfg.more.ip.mask, ipmoar->mask, 4);
		memcpy(cfg.more.ip.gw, ipmoar->gw, 4);
		memcpy(cfg.more.ip.dns1, ipmoar->dns1, 4);
		memcpy(cfg.more.ip.dns2, ipmoar->dns2, 4);
		break;
	}

	/* Appel à la fonction associée de l'interface. */

	if (!wes->iface->setcfg) {
		ret = WESPRET_IMP;
		return (&ret);
	}

	switch ((err = (*wes->iface->setcfg)(wes, setflags, &cfg))) {
	case WROK:
		break;
	default:
		msg((wlerror, "error while setting config: %s", error_string(err)));
		break;
	}

	/* On est bons ! */

	ret = WESPRET_OK;
	return (&ret);
}

/* `query_wes_1_svc()`: recherche et listage de serveurs WES. */

wespret_with_wes_list_t *query_wes_1_svc(wespquery_t *args,
	struct svc_req *req)
{
	static wespret_with_wes_list_t resp;

	msg((wlnotice, "(%s) query servers", REQADDR));

	/* TODO */
	(void)args;

	resp.ret = WESPRET_IMP;
	return (&resp);
}

/* ---
 * Gestion des compteurs.
 * --- */

/* `get_meter_1_svc()`: Récupération de la configuration d'un compteur. */

wespret_with_meter_t *get_meter_1_svc(wespid_with_meter_id_t *args,
	struct svc_req *req)
{
	static wespret_with_meter_t resp;
	wes_t *wes;
	wesmetertype_t expected_type;
	wesmeterflags_t flags;
	wesmetercfg_t *cfg;
	wesif_getmeter_t *getmeter;
	int ret;

	msg((wlnotice, "(%s) get meter %d config from server %d",
		REQADDR, args->meter_id, args->id));

	switch (args->expected_type) {
	case WESPMETERTYPE_NONE:
		expected_type = WESMETERTYPE_NONE;
		break;
	case WESPMETERTYPE_AMPER:
		expected_type = WESMETERTYPE_AMPER;
		break;
	case WESPMETERTYPE_PULSE:
		expected_type = WESMETERTYPE_PULSE;
		break;
	case WESPMETERTYPE_TELEINFO:
		expected_type = WESMETERTYPE_TELEINFO;
		break;
	default:
		resp.ret = WESPRET_IMP;
		return (&resp);
	}

	/* Récupération du WES. */

	GETWES(args->id, resp.ret, &resp)

	/* Préparation des arguments. */

	flags = 0;
	if (args->to_get & WESPMETER_NAME)
		flags |= WESMETER_NAME;
	if (args->to_get & WESPMETER_READ)
		flags |= WESMETER_READ;
	if (args->to_get & WESPMETER_MODE)
		flags |= WESMETER_MODE;
	if (args->to_get & WESPMETER_WHAT)
		flags |= WESMETER_WHAT;
	if (args->to_get & WESPMETER_TYPE)
		flags |= WESMETER_TYPE;
	if (args->to_get & WESPMETER_FIXED_COSTS)
		flags |= WESMETER_FIXED_COSTS;
	if (args->to_get & WESPMETER_PRORATA)
		flags |= WESMETER_PRORATA;

	switch (expected_type) {
	case WESMETERTYPE_NONE:
		break;

	case WESMETERTYPE_AMPER:
		if (args->to_get & WESPMETER_COST)
			flags |= WESMETER_COST;
		if (args->to_get & WESPMETER_VOLTAGE)
			flags |= WESMETER_VOLTAGE;
		break;

	case WESMETERTYPE_PULSE:
		if (args->to_get & WESPMETER_COST)
			flags |= WESMETER_COST;
		if (args->to_get & WESPMETER_METHOD)
			flags |= WESMETER_METHOD;
		if (args->to_get & WESPMETER_GROUP)
			flags |= WESMETER_GROUP;
		break;

	case WESMETERTYPE_TELEINFO:
		if (args->to_get & WESPMETER_COSTS_BASE)
			flags |= WESMETER_COSTS_BASE;
		if (args->to_get & WESPMETER_COSTS_HCHP)
			flags |= WESMETER_COSTS_HCHP;
		if (args->to_get & WESPMETER_COSTS_TEMPO)
			flags |= WESMETER_COSTS_TEMPO;
		if (args->to_get & WESPMETER_COSTS_EJP)
			flags |= WESMETER_COSTS_EJP;
		if (args->to_get & WESPMETER_BDPV_ENABLED)
			flags |= WESMETER_BDPV_ENABLED;
		if (args->to_get & WESPMETER_BDPV_IDS)
			flags |= WESMETER_BDPV_IDS;
		if (args->to_get & WESPMETER_BDPV_TIME)
			flags |= WESMETER_BDPV_TIME;
		break;
	}

	getmeter = wes->iface->getmeter;
	if (!getmeter) {
		resp.ret = WESPRET_IMP;
		return (&resp);
	}

	ret = (*getmeter)(wes, args->meter_id, expected_type, flags, &cfg);
	switch (ret) {
	case WROK:
		break;
	default:
		msg((wlerror, "could not get meter cfg: %s", error_string(ret)));
		resp.ret = WESPRET_INT;
		return (&resp);
	}

	correct_meter(wes, cfg, expected_type, flags);

	/* Traduction en le format du protocole. */

	{
		wespmeter_t *r;
		wespmetermore_amper_t *amp;
		wespmetermore_pulse_t *pul;
		wespmetermore_teleinfo_t *tic;

		r = &resp.meter;
		r->name = cfg->name;
		r->rd = cfg->is_read;
		r->fixed_cost = cfg->fixed_cost;
		r->prorata = cfg->prorata;

		switch (cfg->mode) {
		case WESMETERMODE_CONSO:
			r->mode = WESPMETERMODE_CONSO;
			break;
		case WESMETERMODE_PROD:
			r->mode = WESPMETERMODE_PROD;
			break;
		}

		switch (cfg->what) {
		case WESMETERWHAT_UNKNOWN:
			r->what = WESPMETERWHAT_UNKNOWN;
			break;
		case WESMETERWHAT_WATER:
			r->what = WESPMETERWHAT_WATER;
			break;
		case WESMETERWHAT_GAS:
			r->what = WESPMETERWHAT_GAS;
			break;
		case WESMETERWHAT_ELEC:
			r->what = WESPMETERWHAT_ELEC;
			break;
		case WESMETERWHAT_FUEL:
			r->what = WESPMETERWHAT_FUEL;
			break;
		}

		switch (cfg->type) {
		case WESMETERTYPE_NONE:
			r->more.type = WESPMETERTYPE_NONE;
			break;
		case WESMETERTYPE_AMPER:
			r->more.type = WESPMETERTYPE_AMPER;
			amp = &r->more.wespmetermore_t_u.amper;

			amp->voltage = cfg->more.amper.voltage;
			amp->cost = cfg->more.amper.cost;
			break;
		case WESMETERTYPE_PULSE:
			r->more.type = WESPMETERTYPE_PULSE;
			pul = &r->more.wespmetermore_t_u.pulse;

			pul->group_size = cfg->more.pulse.group_size;
			pul->cost = cfg->more.pulse.cost;

			switch (cfg->more.pulse.method) {
			case WESPLSMETHOD_ILS:
				pul->method = WESPPLSMETHOD_ILS;
				break;
			case WESPLSMETHOD_ELEC:
				pul->method = WESPPLSMETHOD_ELEC;
				break;
			}

			break;
		case WESMETERTYPE_TELEINFO:
			r->more.type = WESPMETERTYPE_TELEINFO;
			tic = &r->more.wespmetermore_t_u.teleinfo;

			tic->cost_base_th = cfg->more.teleinfo.cost_base_th;
			tic->cost_hchp_hc = cfg->more.teleinfo.cost_hchp_hc;
			tic->cost_hchp_hp = cfg->more.teleinfo.cost_hchp_hp;
			tic->cost_tempo_hcjb = cfg->more.teleinfo.cost_tempo_hcjb;
			tic->cost_tempo_hpjb = cfg->more.teleinfo.cost_tempo_hpjb;
			tic->cost_tempo_hcjw = cfg->more.teleinfo.cost_tempo_hcjw;
			tic->cost_tempo_hpjw = cfg->more.teleinfo.cost_tempo_hpjw;
			tic->cost_tempo_hcjr = cfg->more.teleinfo.cost_tempo_hcjr;
			tic->cost_tempo_hpjr = cfg->more.teleinfo.cost_tempo_hpjr;
			tic->cost_ejp_hn = cfg->more.teleinfo.cost_ejp_hn;
			tic->cost_ejp_pm = cfg->more.teleinfo.cost_ejp_pm;

			tic->bdpv_enabled = cfg->more.teleinfo.bdpv_enabled;
			tic->bdpv_hour = cfg->more.teleinfo.bdpv_hour;
			tic->bdpv_min = cfg->more.teleinfo.bdpv_min;
			tic->bdpv_username = cfg->more.teleinfo.bdpv_username;
			tic->bdpv_password = cfg->more.teleinfo.bdpv_password;
			break;
		}
	}

	/* Tout va bien ! */

	resp.ret = WESPRET_OK;
	set_obj_to_free(cfg);
	return (&resp);
}

/* `set_meter_1_svc()`: Définition de la configuration d'un compteur. */

wespret_t *set_meter_1_svc(wespid_with_meter_t *args, struct svc_req *req)
{
	static wespret_t ret;

	msg((wlnotice, "(%s) set meter %d config on server %d",
		REQADDR, args->meter_id, args->id));

	/* TODO */

	ret = WESPRET_IMP;
	return (&ret);
}

/* `get_meter_data_1_svc()`: Récupération des valeurs d'un compteur. */

wespret_with_meter_data_t *get_meter_data_1_svc(
	wespid_with_meter_id_and_span_t *args, struct svc_req *req)
{
	static wespret_with_meter_data_t resp;

	msg((wlnotice, "(%s) get meter %d values from server %d",
		REQADDR, args->meter_id, args->id));

	/* TODO */

	resp.ret = WESPRET_IMP;
	return (&resp);
}

/* `query_meters_1_svc()`: Recherche de compteurs. */

wespret_with_meter_list_t *query_meters_1_svc(wespid_with_meter_query_t *args,
	struct svc_req *req)
{
	static wespret_with_meter_list_t resp;
	wespmeter_list_element_t results[10];
	int ret, meter_id;
	wesmetertype_t meter_type;
	wespmetertype_t meter_ptype;
	wes_t *wes;
	void *iter;

	msg((wlnotice, "(%s) query meters on server %d",
		REQADDR, args->id));

	/* Vérification des paramètres */

	resp.ret = WESPRET_VAL;
	resp.meters.meters_val = results;
	resp.meters.meters_len = 0;

	if (args->offset < 0 || args->offset > 32766
	 || args->count < 1 || args->count > 10)
		return (&resp);
	switch (args->of_type) {
	case WESPMETERTYPE_NONE:
		meter_type = WESMETERTYPE_NONE;
		break;
	case WESPMETERTYPE_AMPER:
		meter_type = WESMETERTYPE_AMPER;
		break;
	case WESPMETERTYPE_PULSE:
		meter_type = WESMETERTYPE_PULSE;
		break;
	case WESPMETERTYPE_TELEINFO:
		meter_type = WESMETERTYPE_TELEINFO;
		break;
	default:
		return (&resp);
	}

	/* Récupération du WES. */

	GETWES(args->id, resp.ret, &resp)

	/* Création de l'itérateur. */

	if (!wes->iface->getmeteriter || !wes->iface->getmeternext
	 || !wes->iface->skipmeters) {
		resp.ret = WESPRET_IMP;
		return (&resp);
	}

	switch ((ret = (*wes->iface->getmeteriter)(wes, meter_type, &iter))) {
	case 0:
		break;

	default:
		msg((wlerror, "could not get meter iterator: %s", error_string(ret)));
		resp.ret = WESPRET_INT;
		return (&resp);
	}

	/* Passer les premiers compteurs. */

	if (args->offset
	&& (ret = (*wes->iface->skipmeters)(iter, args->offset))) {
		if (wes->iface->delmeteriter)
			(*wes->iface->delmeteriter)(iter);
		switch (ret) {
		default:
			msg((wlerror, "could not skip meters: %s", error_string(ret)));
			resp.ret = WESPRET_INT;
			return (&resp);
		}
	}

	/* Lire les compteurs. */

	while (resp.meters.meters_len < (u_int)args->count) {
		ret = (*wes->iface->getmeternext)(iter, &meter_id, &meter_type);
		if (ret == WRITER)
			break;
		else if (ret) {
			if (wes->iface->delmeteriter)
				(*wes->iface->delmeteriter)(iter);

			resp.meters.meters_len = 0;
			switch (ret) {
			default:
				msg((wlerror, "could not get next meter: %s",
					error_string(ret)));
				resp.ret = WESPRET_INT;
				return (&resp);
			}
		}

		/* Traitement dans le tableau. */

		switch (meter_type) {
		case WESMETERTYPE_NONE:
			meter_ptype = WESPMETERTYPE_NONE;
			break;
		case WESMETERTYPE_AMPER:
			meter_ptype = WESPMETERTYPE_AMPER;
			break;
		case WESMETERTYPE_PULSE:
			meter_ptype = WESPMETERTYPE_PULSE;
			break;
		case WESMETERTYPE_TELEINFO:
			meter_ptype = WESPMETERTYPE_TELEINFO;
			break;
		default:
			msg((wlerror, "unknown meter type: %d", meter_type));

			if (wes->iface->delmeteriter)
				(*wes->iface->delmeteriter)(iter);

			resp.ret = WESPRET_INT;
			resp.meters.meters_len = 0;
			return (&resp);
		}

		resp.meters.meters_val[resp.meters.meters_len].id = meter_id;
		resp.meters.meters_val[resp.meters.meters_len].type = meter_ptype;
		++resp.meters.meters_len;
	}

	/* Destruction planifiée de l'itérateur. */

	if (wes->iface->delmeteriter)
		(*wes->iface->delmeteriter)(iter);

	resp.ret = WESPRET_OK;
	return (&resp);
}

/* ---
 * Gestion des capteurs.
 * --- */

/* `get_sensor_1_svc()`: récupération de la configuration d'un capteur. */

wespret_with_sensor_t *get_sensor_1_svc(wespid_with_sensor_id_t *args,
	struct svc_req *req)
{
	static wespret_with_sensor_t resp;

	/* TODO */

	resp.ret = WESPRET_IMP;
	return (&resp);
}

/* `set_sensor_1_svc()`: définition de la configuration d'un capteur. */

wespret_t *set_sensor_1_svc(wespid_with_sensor_t *args,
	struct svc_req *req)
{
	static wespret_t ret;

	/* TODO */

	resp.ret = WESPRET_IMP;
	return (&resp);
}

/* ---
 * Fonction de lancement du serveur, avec gestions de signaux.
 * --- */

/* `its_the_signal()`: fonction de réception du signal d'arrêt. */

static __attribute__((noreturn)) void its_the_signal(int sig)
{
	(void)sig;
	longjmp(endjmp, 1);
}

/* `run_server()`: lancer le serveur. */

int run_server(weslist_t *llist)
{
	SVCXPRT *tcp = NULL;

	/* On stocke les données des appels de service (svc) pour que
	 * ceux-ci puissent les utiliser. C'est du global parce que l'interface
	 * legacy de SunRPC ne permet pas de transmettre des cookies. */

	list = llist;

	/* On libère le port sur le service portmap. */

	pmap_unset(WESHD_PROG, WESHD_VERS1);

	/* Création du transport via TCP, création de l'application sur
	 * ce transport. */

	if (!(tcp = svctcp_create(RPC_ANYSOCK, 0, 0))) {
		msg((wlfatal, "could not create the transport"));
		return (WRUNKNOWN);
	} else if (!svc_register(tcp, WESHD_PROG, WESHD_VERS1, weshd_prog_1,
	 IPPROTO_TCP)) {
		msg((wlfatal, "could not register the service"));
		svc_destroy(tcp);
		return (WRUNKNOWN);
	}

	/* Gestion du signal d'arrêt. */

	signal(SIGINT, its_the_signal);

	/* Lancement de l'application.
	 * Remarquez la subtilité : on annonce que le serveur tourne alors qu'en
	 * réalité, pas encore. Mais chut, il ne faut surtout pas que
	 * l'administrateur apprenne ça, il dirait encore que les développeurs
	 * c'est vraiment que des bidouilleurs ;) */

	if (setjmp(endjmp) == 0) {
		msg((wlnotice, "The server is now running."));
		svc_run();
	}

	/* Libération de mémoire.
	 * Malheureusement, il n'y a pas vraiment de moyen de libérer les
	 * ressources allouées en interne par `svc_run()` à ce stade… :/ */

	msg((wlnotice, "Stopping the server."));
	svc_destroy(tcp);
	set_obj_to_free(NULL);
	return (0);
}
