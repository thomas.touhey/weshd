/* ****************************************************************************
 * dummy/internals.h -- déclarations et définitions pour les serveurs WES
 *                      factices.
 * Copyright (C) 2018 Thomas Touhey <thomas@touhey.fr>
 *
 * This project is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the
 * CeCILL license as circulated by CEA, CNRS and INRIA at the
 * following URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the project's author, the holder of the
 * economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also therefore
 * means that it is reserved for developers and experienced professionals
 * having in-depth computer knowledge. Users are therefore encouraged to load
 * and test the software's suitability as regards their requirements in
 * conditions enabling the security of their systems and/or data to be
 * ensured and, more generally, to use and operate it in the same conditions
 * as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 * ************************************************************************* */
#include "internals.h"

static wesif_t dumif;

/* ---
 * Création et ajout d'une ressource, et suppression de cette même
 * ressource.
 * --- */

/* `free_dummy_wes()`: Supprimer une ressource factice. */

static int free_dummy_wes(wes_t *wes)
{
	deinit_wes_base(wes);
	free(wes);
	return (WROK);
}

/* `add_dummy_wes()`: créer une ressource factice. */

int add_dummy_wes(weslist_t *list, wes_t **wesp)
{
	wes_t *wes;
	char dummyname[20];
	int err;

	/* Allocation de la ressource. */

	wes = malloc(sizeof(wes_t));
	if (!wes)
		return (WRALLOC);

	/* Initialisons la base, définissons le nom du WES et
	 * ajoutons la ressource à la liste. */

	sprintf(dummyname, "dummy-%04X", (unsigned int)wes);

	if ((err = init_wes_base(wes, &dumif))
	 || (err = set_name(wes, dummyname))
	 || (err = add_to_list(list, wes))) {
		free_dummy_wes(wes);
		return (err);
	}

	/* On est bons. (oui, c'est aussi simple que ça quand c'est factice) */

	*wesp = wes;
	return (WROK);
}

/* ---
 * Interactions avec la configuration du faux serveur WES (heure locale).
 * --- */

/* `getfakecfg()`: Récupération de la configuration depuis le serveur
 * WES factice. */

static int getfakecfg(wes_t *wes, wescfgflags_t flags, wescfg_t **cfgp)
{
	static wescfg_t cfg;
	wesdt_t *dt;
	time_t tp; struct tm *tm;

	(void)wes;
	(void)flags;

	/* Récupération de l'heure. */

	time(&tp);
	tm = gmtime(&tp);

	/* Remplissage de la configuration.
	 * Le nom est rempli par le serveur. */

	cfg.free = NULL;
	dt = &cfg.current_time;
	dt->year = tm->tm_year + 1900;
	dt->mon = tm->tm_mon + 1;
	dt->dom = tm->tm_mday;
	dt->hour = tm->tm_hour;
	dt->min = tm->tm_min;
	dt->sec = tm->tm_sec;
	dt->dow = tm->tm_wday ? tm->tm_wday - 1 : 6;
	dt->sum = tm->tm_isdst;
	dt->tzhour = 0;
	dt->tzmin = 0;

	/* On est bons ! */

	*cfgp = &cfg;
	return (WROK);
}

/* `setfakecfg()`: application de la configuration sur le serveur
 * WES factice. */

static int setfakecfg(wes_t *wes, wescfgflags_t flags, wescfg_t const *cfg)
{
	/* On accepte direct, histoire que le nom soit accepté et que
	 * le reste de la configuration aussi, bien que rien de plus que
	 * le nom ne sera modifié. */

	(void)wes;
	(void)flags;
	(void)cfg;

	return (WROK);
}

/* ---
 * Interactions avec les (faux) compteurs du WES factice.
 * --- */

/* Les compteurs simulés pour le WES factices sont les suivants :
 * [1] Compteur branché via télé-information. */

/* `getfakemeter()`: Récupération des données d'un compteur du serveur
 * WES factice. */

static int getfakemeter(wes_t *wes, int id, wesmetertype_t expected,
	wesmeterflags_t flags, wesmetercfg_t **cfgp)
{
	static wesmetercfg_t cfg;
	static char name[20];

	(void)wes;
	(void)flags;

	if (id != 1 || expected != WESMETERTYPE_TELEINFO)
		return (WROP);

	cfg.free = NULL;

	strcpy(name, "Faux compteur TIC");
	cfg.name = name;
	cfg.type = WESMETERTYPE_TELEINFO;
	cfg.is_read = 1;
	cfg.mode = WESMETERMODE_CONSO;
	cfg.what = WESMETERWHAT_ELEC;

	cfg.fixed_cost = 0.0;
	cfg.prorata = 0;

	cfg.more.teleinfo.cost_base_th = 0;
	cfg.more.teleinfo.cost_hchp_hc = 0;
	cfg.more.teleinfo.cost_hchp_hp = 0;
	cfg.more.teleinfo.cost_tempo_hcjb = 0;
	cfg.more.teleinfo.cost_tempo_hpjb = 0;
	cfg.more.teleinfo.cost_tempo_hcjw = 0;
	cfg.more.teleinfo.cost_tempo_hpjw = 0;
	cfg.more.teleinfo.cost_tempo_hcjr = 0;
	cfg.more.teleinfo.cost_tempo_hpjr = 0;
	cfg.more.teleinfo.cost_ejp_hn = 0;
	cfg.more.teleinfo.cost_ejp_pm = 0;

	cfg.more.teleinfo.bdpv_enabled = 0;
	cfg.more.teleinfo.bdpv_hour = 23;
	cfg.more.teleinfo.bdpv_min = 45;
	cfg.more.teleinfo.bdpv_username = "user.bdpv";
	cfg.more.teleinfo.bdpv_password = "pw.bdpv";

	*cfgp = &cfg;
	return (WROK);
}

/* ---
 * Définition de l'interface.
 * --- */

static wesif_t dumif = {
	WESTYPE_DUMMY, free_dummy_wes,

	/* Interaction avec la configuration. */

	getfakecfg, setfakecfg,

	/* Interaction avec les compteurs. */

	getfakemeter, NULL, NULL,

	/* Listage des compteurs. */

	NULL, NULL, NULL, NULL
};
