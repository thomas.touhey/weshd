:title: Envoi de variables par requête HTTP et mail via des triggers
:author: Thomas Touhey

Il est possible d'envoyer des variables via l'aspect programmation, dans des
requêtes HTTP et des mails. Ceci est documenté dans l'annexe 4 de la
notice d'utilisation des serveurs WES (V1 & V2).

**TODO**: le documenter ici ?
