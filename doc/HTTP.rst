:title: Définition de variables via l'interface web du serveur WES
:author: Thomas Touhey

Pour les requêtes HTTP avec certains arguments GET ou POST, l'interface web du
serveur WES modifie sa configuration courante. Ces requêtes marchent quel que
soit le chemin (même si celui-ci pointe vers un fichier ou un dossier qui
n'existe pas).

Les requêtes GET peuvent ne contenir qu'un seul argument, mais les requêtes
POST doivent contenir tous les éléments du formulaire correspondant.
Par exemple, pour l'afficheur, pour définir ``LAL`` (la durée d'impulsion
du mode alarme sous forme flottante), il faut aussi définir ``LBL`` dans
la même requête.

Les arguments passés via GET ne sont **pas** les mêmes que ceux passés
via POST, les deux sont donc bel et bien à distinguer.

Quelques fois, les requêtes comportent des éléments non éditables, comme
l'adresse MAC : tenter de les éditer de force provoquera possiblement un
timeout.

-------------
Arguments GET
-------------

``nm``
	Nom d'hôte.

``dhcp``
	Statut du mode DHCP.

	``ON`` ou ``OFF``.

``ip``
	IPv4 fixe (lorsque le mode DHCP est désactivé).

	Sous le format ``XXX.XXX.XXX.XXX``.

``msk``
	Masque de sous-réseau en IPv4 (lorsque le mode DHCP est désactivé).

	Sous le format ``XXX.XXX.XXX.XXX``.

``gw``
	Passerelle par défaut en IPv4 (lorsque le mode DHCP est désactivé).

	Sous le format ``XXX.XXX.XXX.XXX``.

``pdns``
	IPv4 du serveur DNS primaire (lorsque le mode DHCP est désactivé).

	Sous le format ``XXX.XXX.XXX.XXX``.

``sdns``
	IPv4 du serveur DNS secondaire (lorsque le mode DHCP est désactivé).

	Sous le format ``XXX.XXX.XXX.XXX``.

``mac``
	Adresse MAC du serveur WES.

	Sous le format ``XX:XX:XX:XX:XX:XX``. Non éditable en réalité : une
	valeur différente de celle par défaut est ignorée.

``pnb``
	Port HTTP.

	Sous format numérique. Non éditable en réalité : une valeur de celle
	par défaut provoque un timeout de la requête.

``TIC[1-2]``
	Selon s'il faut lire le compteur branché via TÉLÉINFO.

	« ON » si oui, « OFF » sinon.

``cp2m``
	Mode du compteur 2.

	« CONS » s'il est en mode consommation, « PROD » s'il
	est en mode production.

``TCSV``
	Selon s'il faut activer l'écriture d'enregistrements dans les fichiers
	CSV ou non pour les compteurs branchés via TÉLÉINFO.

	« ON » si oui, « OFF » sinon.

``ncpt[1-2]``
	Nom du compteur branché via TÉLÉINFO.

	Maximum 16 caractères.

``ABO[1-2]``
	Coût fixe de l'abonnement du compteur branché via TÉLÉINFO.

	Sous format flottant.

``ABP[1-2]``
	Selon si l'abonnement du compteur est au prorata du prix du kWh.

	Rien si non, « 1 » si oui.

``TR[AB][0-5]``
	Prix du kWh dans une période tarifaire donnée.

	``A`` pour le compteur 1, ``B`` pour le compteur 2.
	La période est la suivante selon le chiffre après la lettre du compteur :

	- ``0`` : BASE/HCHP/EJPHN/HCJB ;
	- ``1`` : HCHC/EJPPM/HPJB ;
	- ``2`` : HCJW ;
	- ``3`` : HPJW ;
	- ``4`` : HCJR ;
	- ``5`` : HPJR.

``rl[n]``
	Allumer ou éteindre le relai ``n``.

	« ON » pour allumer le relai, « OFF » pour éteindre le relai.
	Exemples de relais :

	- ``rl1`` : relai 1 ;
	- ``rl119`` : relai virtuel numéro 9 de chaque carte ;

``trl[n]``
	Provoquer une impulsion sur un relai donné.

	L'argument est le nombre de secondes que dure l'impulsion.
	Par exemple, ``trl105=5`` crée une impulsion de 5 secondes sur
	le relai 105.

``vs[n]``
	Allumer ou éteindre le switch virtuel ``n``.

	« ON » pour allumer le switch virtuel, « OFF » pour l'éteindre.
	Par exemple, ``vs2=ON`` allume le switch virtuel numéro 2.

``frl``
	Inverser l'état d'un relai donné.

	Par exemple, ``frl=101`` pour inverser l'état du relai 101.

``fvs``
	Inverser l'état d'un switch virtuel.

	Par exemple, ``fvs=1`` pour inverser l'état du switch virtuel 1.

``alarme``
	Selon si l'alarme est désactivée ou non.

	« ON » si elle l'est, « OFF » sinon.
