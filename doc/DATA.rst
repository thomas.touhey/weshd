:title: Données avec historique sous format CSV
:author: Thomas Touhey

Le serveur WES est capable de stocker, par minute, trois types de données
qu'il reçoit depuis des entrées diverses :

- pinces ampèremétriques (``PCE``) ;
- impulsions (``PLS``) ;
- compteurs reliés via Téléinfo (``teleinfo``).

Cette fonctionnalité est optionnelle et doit être activée sur le serveur WES,
manuellement ou via l'une des autres interfaces (configuration, requêtes
HTTP).

Ces données sont stockées dans des fichiers CSV, regroupées par jour avec une
entrée par minute, où la première colonne représente la minute de la journée,
et les autres les données (sous des formats divers). Ces fichiers sont
accessibles via différents chemins.

- Pour accéder aux données concernant les pinces :

  - ``PCE/YYYY/PC-MM-DD.csv`` (nouveau format) ;
  - ``PCE/PCYYYY-MM-DD.csv`` (ancien format) ;

- Pour accéder aux données concernant les impulsions :

  - ``PLS/YYYY/PL-MM-DD.csv`` (nouveau format) ;
  - ``PLS/PCYYYY-MM-DD.csv`` (ancien format ?) ;
  - ``PLS/PLYYYY-MM-DD.csv`` (ancien format bis ?) ;

- Pour accéder aux données récupérées via téléinfo :

  - ``teleinfo/YYYY/TI-MM-DD.csv`` (nouveau format) ;
  - ``teleinfo/TIYYYY-MM-DD.csv`` (ancien format).

Où ``YYYY`` représente l'année sur quatre chiffres, ``MM`` représente
le mois de l'année et ``DD`` représente le jour du mois.

Les fichiers sont donc au format CSV avec des ``,``, où la première colonne
est toujours sous le format ``HH:MM``, sauf la première ligne qui contient
le nom des champs. Les retours à la ligne sont des ``<CR><LF>``, où ``<CR>``
représente le retour chariot (``\r``, code ASCII 13) et ``<LF>`` représente le
retour à la ligne (``\n``, code ASCII 10).

Pour les pinces ampèremétriques, les colonnes sont les suivantes :

- heure au format ``HH:MM`` ;
- valeur sous forme de flottant à deux décimales ou ``nan`` (« not a number »)
  si indisponible pour la pince 1 ;
- idem pour la pince 2 ;
- idem pour la pince 3 ;
- idem pour la pince 4.

Pour les compteurs d'impulsions, les colonnes sont les suivantes :

- heure au format ``HH:MM`` ;
- valeur sous forme de flottant à deux décimales ou ``nan`` (« not a number »)
  si indisponible pour le compteur d'impulsions 1 ;
- idem pour le compteur d'impulsions 2 ;
- idem pour le compteur d'impulsions 3 ;
- idem pour le compteur d'impulsions 4.

Pour l'entrée Téléinfo, les colonnes sont les suivantes :

- heure au format ``HH:MM`` ;
- énergie consommée lors des heures pleines (cumul), en Wh ;
- énergie consommée lors des heures creuses (cumul), en Wh ;
- Intensité instantanée, en ampères ;
- Puissance apparente, en VA.

À noter que s'il n'y a pas de compteur de branché, il n'y aura que deux
colonnes et la deuxième colonne ne contiendra que « PAS de teleinfo ». Il
peut n'y avoir que deux colonnes dans une ligne et cinq sur une autre.
