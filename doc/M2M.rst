:title: Interface machine à machine du serveur WES
:author: Thomas Touhey

Le serveur WES propose une interface faite pour être utilisée par d'autres
machines. Cette interface est basée sur le modèle client/serveur, où le
client, après connexion, soumet une ligne contenant la commande, puis le
serveur répond par une ligne.

Cette fonctionnalité est optionnelle et doit être activée sur le serveur WES,
manuellement ou via l'une des autres interfaces (configuration, requêtes
HTTP).

La connexion peut être en mode connecté (via TCP) ou en mode non connecté
(via UDP). En TCP, la ligne de réponse est renvoyée sur le même canal.
En UDP, la réponse est envoyée dans un autre paquet après que la commande
aie été traitée par le serveur.

Les lignes se terminent obligatoirement, dans un sens comme dans l'autre,
par un retour à la ligne (``\n``, code ASCII 10). Ces caractères de
terminaison de ligne (et donc de message) ne sont pas représentés dans la
suite de ce document.

Les commandes et leur réponse n'ont pas de format régulier (ou du moins, ont
trop d'exceptions pour que la connaissance de ce format est une réelle
valeur opérationnelle). On peut toutefois supputer que l'idée de base est
la suivante :

- ``<clé>`` est une demande de la valeur de la clé, où la réponse est soit du
  format ``<valeur>`` soit du format ``<clé>=<valeur>`` ;
- ``<clé>=<valeur>`` est une affectation d'une valeur à une option, où la
  réponse est du même format que pour la demande.

Lorsque la commande est inconnue ou que la syntaxe est incorrecte, la ligne
envoyée par le serveur est ``???`` (où ``?`` est le point d'interrogation,
code ASCII 63).

Quelques récupérations de logs pour analyse future sont dans le fichier
``NOTES.txt`` (tests avec l'utilitaire ``ncat``, en TCP).

Options connues
---------------

``gfw``

	Version du firmware, e.g. ``V0.7A``.

``gmac``

	Adresse MAC, e.g. ``001EC09497DB``.

``lcd_a`` (``lcd_alm`` en pré-V0.7A)

	Alarme visuelle (l'écran clignotera en rouge si activée).
	Renverra une réponse du type `LCD_ALM=<valeur donnée>` lors d'une
	affectation, et `LCD_ALM=<valeur inconnue>` lors d'une simple
	récupération (la valeur inconnue ressemble à un nombre d'itérations,
	mais ne donne pas d'information *a priori* sur le statut de l'alarme).

	**TODO** Peut-être en récupérant plusieurs fois la valeur et en regardant
	si elle a évolué ? ;

``lcd_b``

	Backlight (de couleur verte).

D'autres variables existent mais je n'en ai pas l'utilité pour le moment.
