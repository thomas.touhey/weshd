:title: Scripting pour l'interface web du serveur WES
:author: Thomas Touhey

L'interface web du serveur WES utilise un langage de scripting personnalisé
qui est interprété pour tout fichier portant l'extension ``CGI`` ou ``CGX``.
Ces fichiers sont encodés en UTF-8 normalisés en NFC.

Ce langage permet d'avoir une sortie conditionnelle selon certaines variables.
Il est possible de téléverser un script au serveur via FTP, puis de l'exécuter
via HTTP pour obtenir l'état de certaines variables non seulement
difficilement accessibles mais aussi de façon trop instable par d'autres
moyens (scraping), d'où l'intérêt de ce document pour le projet.

Dans ce langage, une ligne représente une instruction, commence par un
caractère désignant le type d'instruction suivi par un contenu optionnel
séparé du caractère de l'instruction par un espace, et se termine par un
retour à la ligne type MS-DOS, ``<CR><LF>``, où ``<CR>`` représente le retour
chariot (``\r``, code ASCII 13) et ``<LF>`` représente le retour à la ligne
(``\n``, code ASCII 10). Les types d'instruction ne sont jamais envoyés au
serveur, « la ligne » ici désigne ce qui est entre l'espace suivant le
caractère désignant l'instruction et la séquence de retour à la ligne décrite
précédemment. Les types d'instruction connus sont :

``# `` (U+0023 puis U+0020)
	Commentaire.

	La ligne ne sera pas envoyée au client HTTP.

``t `` (U+0074 puis U+0020)
	Texte.

	La ligne sera envoyée sans modification au client HTTP, suivie d'un CRLF.

``i `` (U+0069 puis U+0020)
	Inclusion.

	Le fichier dont la ligne contient le nom, e.g. ``BANDEAU.INC``, sera inclus
	sans modification. Le fichier inclus ne doit pas contenir d'instructions,
	mais bel et bien le contenu directement.

``c `` (U+0063 puis U+0020)
	Commande.

	Cette ligne est une commande (voir ci-dessous).

``. `` (U+002E puis U+0020)
	Fin de fichier.

	Cette instruction doit se trouver en dernière dans le fichier, et désigne
	la fin de ce dernier. Le contenu de cette ligne est ignoré, et peut sans
	problème être vide (i.e. la ligne complète peut ne contenir que
	``.<CR><LF>``).

Une commande désigne une variable, qui est composée de quatre caractères,
dont les deux premiers désignent souvent la catégorie/sous-catégorie et
les deux derniers la variable dans cette catégorie. Lorsque la catégorie
fait un caractère, le deuxième est un espace, de même pour la variable dans
la catégorie. Les espaces et tabulations sont assimilés. Le nom de la variable
est sensible à la casse.

Selon la variable, la commande peut requérir un format ou non. Si requis,
ce format est un format très similaire à `celui de printf en C <pf_>`_.
Par exemple, ``r i``, qui désigne l'IP fixe lors d'une configuration fixe,
donne quatre entiers pour le format donné, un exemple de format est donc
``%d.%d.%d.%d``.

Les types pouvant être utilisés avec ce format ici sont les suivants :

- entiers, affichables par les placeholders de type ``%d``, ``%x``, ``%o`` ;
- flottants, affichables par le placeholder de type ``%f`` ;
- chaînes, affichables par le placeholder de type ``%s``.

Les variables décrites dans ce document peuvent être documentées ou non dans
le document ``CGI_WES.pdf`` fourni par Cartelectronic.
Je les ajoute et détaille au gré de mes besoins, de ma curiosité et de ce
que supporte les versions du firmware sur lesquelles je travaille.

Parmi les schémas récurrents dans les noms de variables, on note :

- les compteurs 1 et 2 sont souvent différenciés par la casse, où la
  minuscule désigne le compteur 1 et la majuscule désigne le compteur 2 ;
- on trouve parfois la distinction année/mois/semaine/jour, cela va de
  pair avec leurs lettres associées en français, `amsj` ;
- lorsqu'il il y a distinction entre forme numérique et forme textuelle,
  souvent, la majuscule représente la forme numérique et la minuscule l'état
  sous forme de chaîne, e.g. ``l E`` renvoie ``0`` ou ``1`` et ``l e`` renvoie
  ``ON`` ou ``OFF``.

Quelques valeurs selon le format :

- les éléments cachés ou non (selon s'ils ont été activés ou non dans
  l'interface associée, et souvent utilisés sur des éléments de type
  ``<li>``) ont ``style="display:none;"`` précédés d'un espace si cachés,
  et rien sinon ;
- les éléments cochés ou non ont ``checked`` précédés d'un espace s'ils
  ont été cochés, et rien sinon ;
- les éléments sélectionnés ou non ont ``selected`` précédés d'un espace
  s'ils le sont, et rien sinon ;
- les éléments désactivés ou non ont ``disabled`` précédés d'un espace
  s'ils ont été désactivés, et rien sinon ;
- il y a également les tableaux générés avec un ``<tr>`` par ligne
  dans le fichier HTML produit (la structure des tableaux est posée dans
  les scripts avec des ``t ``).

Lorsque la syntaxe d'une instruction est inconnue, ou lorsque la commande
n'existe pas ou plus, la ligne est purement et simplement ignorée (au même
titre qu'un commentaire).

-------------------------------------------------------
``[AaMmSsJj]`` : consommation par période des compteurs
-------------------------------------------------------

``a 1``, ``m 1``, ``s 1``, ``j 1``

	::

		Entier (kWh consommés), flottant (coût total)

	Consommation sur l'année, le mois, la semaine ou la journée pour
	les abonnements suivants :

	- BASE
	- H. Creuse H.P.
	- EJP Heure normale
	- TEMPO jour Bleu Heure Creuse

``a 2``, ``m 2``, ``s 2``, ``j 2``

	::

		Entier (kWh consommés), flottant (coût total)

	Consommation sur l'année, le mois, la semaine ou la journée pour
	les abonnements suivants :

	- H. Creuse H.C.
	- EJP Heure pointe mobile
	- TEMPO jour Bleu Heure Pleine

``a 3``, ``m 3``, ``s 3``, ``j 3``

	::

		Entier (kWh consommés), flottant (coût total)

	Consommation sur l'année, le mois, la semaine ou la journée pour
	les abonnements suivants :

	- TEMPO jour Blanc Heure Creuse

``a 4``, ``m 4``, ``s 4``, ``j 4``

	::

		Entier (kWh consommés), flottant (coût total)

	Consommation sur l'année, le mois, la semaine ou la journée pour
	les abonnements suivants :

	- TEMPO jour Blanc Heure Pleine

``a 5``, ``m 5``, ``s 5``, ``j 5``

	::

		Entier (kWh consommés), flottant (coût total)

	Consommation sur l'année, le mois, la semaine ou la journée pour
	les abonnements suivants :

	- TEMPO jour Rouge Heure Creuse

``a 6``, ``m 6``, ``s 6``, ``j 6``

	::

		Entier (kWh consommés), flottant (coût total)

	Consommation sur l'année, le mois, la semaine ou la journée pour
	les abonnements suivants :

	- TEMPO jour Rouge Heure Pleine

``a c``, ``m c``, ``s c``, ``j c``
	Consommation + coût sur l'année, le mois, la semaine ou la journée
	pour chaque période tarifaire sous la forme d'un tableau à 3 colonnes.

``a E``, ``m E``, ``s E``, ``j E``

	::

		Flottant (coût total)

	Prix total de la consommation sur l'année, le mois, la semaine ou la
	journée, en euros.

``a e``, ``m e``, ``s e``, ``j e``
	Coût sur l'année, le mois, la semaine ou la journée à chaque
	période tarifaire, sous la forme d'un tableau à 2 colonnes.

``a M``
	?

``a T``, ``m T``, ``s T``, ``j T``

	::

		Entier (kWh consommés)

	Consommation totale sur l'année, le mois, la semaine ou la journée,
	en kWh.

``a t``, ``m t``, ``s t``, ``j t``
	Consommation sur l'année, le mois, la semaine ou la journée en euros
	sous la forme d'un tableau à 2 colonnes.

``ac[1-2]``, ``mc[1-2]``, ``jc[1-2]``
	?

``am[1-2]``, ``mm[1-2]``, ``jm[1-2]``
	?

``m[1-2]M``
	?

``mE[1-2]``

	::

		Flottant (?)

	Prix en euros ?

``mT[1-2]``

	::

		Flottant (?)

	Conso° totale en kWh ?

-----------------------------------------
``b`` : gestion des bargraphes et courbes
-----------------------------------------

``b d``

	::

		Chaîne (?)

	Date du graphique affiché.

``b p``

	::

		Chaîne (?)

	vAxis title?

``b w``
	?

-------------------------------------------
``C`` : coût des kW, abonnements, gaz, eau…
-------------------------------------------

``C a``, ``C A``

	::

		Flottant (coût)

	Coût fixe de l'abonnement en euros pour le compteur 1 branché via TELEINFO.
	``A`` pour le compteur 2.

``C p[1-2]``

	::

		Chaîne (checked ON)

	Prorata abonnement au prix du kWh ou non pour le compteur 1.
	``p2`` pour le compteur 2.

``C t[0-5]``, ``C T[0-5]``

	::

		Flottant (?)

	c.f. prix de la consommation pour les compteurs

	TRA[0-5]? (`t`)
	TRB[0-5]? (`T`)

---------------------------------------
``c`` : réglage des index des compteurs
---------------------------------------

``c [1-3]``

	::

		Chaîne (?)

	?

``c i[1-6]``, ``c I[1-6]``

	::

		Entier (?)

	IDXA[0-5]? (`i`)
	IDXB[0-5]? (`I`)
	``%09d``

------------------------
``d`` : debug temporaire
------------------------

Cette lettre est réservée à des usages de déboggage.

-----------------------------------
``E`` : configuration des courriels
-----------------------------------

``E d[1-4]``

	::

		Chaîne (adresse e-mail)

	Destinataire n° 1 à 4 des courriels.

``E e``

	::

		Entier (?)

	Statut du test du serveur SMTP :

	- 0 : envoyé avec succès ;
	- 1 : délai expiré ;
	- 2 : erreur de configuration ;
	- 3 : non testé ;
	- 4 : envoi en cours.

``E m``

	::

		Chaîne (adresse e-mail)

	Adresse de courriel de l'émetteur.

``E n``

	::

		Chaîne (nom d'hôte)

	Nom d'hôte du serveur SMTP.

``E p``

	::

		Entier (numéro de port)

	Numéro de port du service SMTP.

``E u``

	::

		Chaîne (utilisateur)

	Nom de l'utilisateur SMTP.

``E w``

	::

		Chaîne (mot de passe)

	Mot de passe de l'utilisateur SMTP.

-------------------------------------
``e`` : valeur en-têtes des compteurs
-------------------------------------

En-têtes envoyées par les compteurs via Téléinfo.

``e a``, ``e A``

	::

		Chaîne (identifiant)

	Numéro ADCO (adresse du concentrateur de téléport, soit le numéro de
	série) du compteur 1. ``A`` pour le compteur 2.

``e b``, ``e B``

	::

		Chaîne (nom)

	Nom de l'abonnement pour le compteur 1. ``B`` pour le compteur 2.
	Exemple : « H Pleines/Creuses »

``e c``, ``e C``

	::

		Entier (valeur)

	Intensité souscrite pour le compteur 1. ``C`` pour le compteur 2.

``e d``, ``e D``

	::

		Entier (numéro)

	Numéro de l'abonnement pour le compteur 1. ``D`` pour le compteur 2.
	Valeurs possibles :

	- 0 : BASE
	- 1 : Heure Creuse
	- 2 : EJP
	- 3 : TEMPO

``e E``

	::

		Chaîne (?)

	« … en € » ?

``e n``, ``e N``

	::

		Chaîne (nom)

	Nom donné via l'interface au compteur 1. ``N`` pour le compteur 2.

``e P``

	::

		Chaîne (?)

	kWh? mode? CPT2_prodcons?

``eo[1-2]``

	::

		Entier (?)

	OPTARIF? 1/2 pour compteur ?

----------------------------
``f`` : gestion des fichiers
----------------------------

``fb``
	Liste des sauvegardes, avec nom, taille en Ko et date de création.

``fcP``, ``fcp``
	?

``fcw``
	?

``fh``
	?

``ff``

	::

		Flottant (capacité)

	Espace libre sur la carte micro-SD, en Go.

---------------------------------------
``G`` : configuration TIC - EAU - PINCE
---------------------------------------

``G a``

	::
		Chaîne (checked ON), chaîne (checked OFF)

	Activer la lecture des pinces.

``G A``

	::

		Chaîne (checked ON), chaîne (checked OFF)

	Activer l'enregistrement dans les fichiers CSV pour les pinces
	ampèremétriques.

``G C``

	::

		Chaîne (checked ON)

	Mode consommation activé pour le compteur 2.

``G I``

	::

		Chaîne (checked ON), chaîne (checked OFF)

	Activer l'enregistrement dans des fichiers CSV pour les impulsions.

``G i``

	::

		Chaîne (checked ON), chaîne (checked OFF)

	Activer le comptage.

``G l``

	::

		Chaîne (checked ON), chaîne (checked OFF)

	Activer les entrées des capteurs.

``G p``

	::

		Chaîne (checked ON)

	Mode production activé pour le compteur 2.

``G t``, ``G T``

	::

		Chaîne (checked ON), chaîne (checked OFF)

	Statut de la lecture du TIC1 (``t``) ou du TIC2 (``T``).

``G v``

	::

		Chaîne (checked ON), chaîne (checked OFF)

	Afficher ou non le module sur les variables en page d'accueil.

``G W``

	::

		Chaîne (checked ON), chaîne (checked OFF)

	Enregistrer dans des fichiers CSV.

``G w``

	::

		Chaîne (checked ON), chaîne (checked OFF)

	Activer la lecture des sondes 1WIRE.

``GAa``

	::

		Chaîne (checked ON), chaîne (checked OFF)

	Statut des entrées analogiques (activé ou désactivé).

``GAA``

	::

		Chaîne (checked ON), chaîne (checked OFF)

	Afficher ou non le module sur les entrées analogiques sur la page
	d'accueil.

``GCT``

	::

		Chaîne (checked ON), chaîne (checked OFF)

	TCSV ON/OFF?

``GP[1-3][1-4]``

	::

		Chaîne (checked ON)[, chaîne (nom)]

	Lier les tarifs de la pince ampèremétrique numéro 1 à 4 à ceux d'un
	compteur. Trois choix :

	- Choix numéro 1 : lier la pince aux tarifs du compteur 1 (avec nom
	  du compteur 1 en second argument) ;
	- Choix numéro 2 : lier la pince aux tarifs du compteur 2 (avec nom
	  du compteur 2 en second argument) ;
	- Choix numéro 3 : ne pas lier la pince aux tarifs d'un compteur, et
	  définir le tarif manuellement (voir ``GPE[1-4]``).

``GPA``

	::

		Chaîne (caché OFF)

	Selon si les pinces ampèremétriques 3 et 4 sont activées ou non.
	Si elles sont désactivées toutes deux, la chaîne devient
	``style="display:none;"`` précédé d'un espace.

``GPA[1-4]``

	::

		Chaîne (caché OFF)

	Compteur PLS[1-4] activé ou non.

``GPa[1-4]``

	::

		Chaîne (checked ON), chaîne (checked OFF)

	Activer la pince numéro 1 à 4.

``GPC[1-4]``

	::

		Chaîne (checked ON)

	Mode consommation pour la pince ampèremétrique numéro 1 à 4, par
	opposition à ``GPP[1-4]``.

``GPE[1-4]``

	::

		Flottant (coût)

	Coût du kWh en euros pour la pince ampèremétrique numéro 1 à 4.

``GPe2``

	::

		Flottant (?, 2 décimales ?)

	?

``GPP[1-4]``

	::

		Chaîne (checked ON)

	Mode production pour la pince ampèremétrique numéro 1 à 4, par
	opposition à ``GPC[1-4]``.

``GPV``

	::

		Chaîne (caché OFF)

	Selon si la tension secteur doit être considérée comme une pince
	ampèremétrique ou pas. Si ce n'est pas le cas, la chaîne est
	l'attribut ``style="display:none;"`` précédé d'un espace.

``GPv``

	::

		Chaîne (checked ON), chaîne (checked OFF)

	Activer la mesure de la tension secteur.

``Gp[1-3][1-4]``

	::

		Chaîne (checked ON)[, chaîne (nom du compteur)]

	Calcul du coût en fonction d'un compteur TÉLÉINFO (choix 1 et 2 pour les
	compteurs 1 à 2) ou manuellement (choix 3) pour le compteur d'impulsions
	numéro 1 à 4. Pour les choix 1 et 2, le nom du compteur est rajouté
	aux arguments.

``GpA[1-4]``

	::

		Chaîne (style)

	Selon si le compteur d'impulsions est activé ou non. S'il est désactivé,
	la chaîne est un attribut HTML qui s'applique par exemple à des éléments
	``<li>`` et qui donne ``style="display:none;"`` précédé d'un espace.

``Gpa[1-4]``

	::

		Chaîne (checked ON)

	Activer le compteur numéro 1 à 4.

``GpC[1-4]``

	::

		Chaîne (checked ON)

	Mode consommation pour le compteur numéro 1 à 4 (par opposition à
	``GpP[1-4]``).

``GpE[1-4]``

	::

		Flottant (coût)

	Coût par unité défini manuellement pour le compteur d'impulsions
	numéro 1 à 4, en euros.

``Gpe[1-4]``

	::

		Flottant (coût)

	Coût fixe par an pour l'abonnement défini manuellement pour le compteur
	d'impulsions numéro 1 à 4, en euros.

``Gpi[1-4]``

	::

		Entier (?)

	PLSact[1-4]?

``GpP[1-4]``

	::

		Chaîne (checked ON)

	Mode production pour le compteur numéro 1 à 4 (par opposition à
	``GpP[1-4]``).

------------------------------
``g`` : configuration générale
------------------------------

``g [1-7]``

	::

		Chaîne (selected ON)

	Jour de la semaine actuel :

	- 1 : lundi
	- …
	- 7 : dimanche

``g d``

	::

		Entier (jour), entier (mois), entier (année)

	"Date système" ?

``g j``

	::

		Entier (?)

	jour_cgx?

---------------------
``H`` : requêtes HTTP
---------------------

``H a``

	::

		Chaîne (checked ON), chaîne (checked OFF)

	Envoi au service `BDPV`_ activé ou non.

``H h``

	::

		Entier (heure)

	Heure d'envoi.

``H i``

	::

		Chaîne (identifiant)

	Identifiant `BDPV`_.

``H m``

	::

		Entier (minute)

	Minute d'envoi.

``H P[1-5]``

	::

		Entier (numéro de port)

	Port pour la requête n°1 à 5 utilisée en programmation.

``H p``

	::

		Chaîne (mot de passe)

	Mot de passe `BDPV`_.

``H r``

	::

		Chaîne (message)

	Dernière réponse du service `BDPV`_.

``H S``

	::

		Entier (message)

	Dernière réponse au test de requête HTTP.

``H s``

	::

		Chaîne (message)

	Statut de la connexion au service `BDPV`_.

``H U[1-5]``

	::

		Chaîne (URL)

	URL pour la requête n°1 à 5 utilisée en programmation.

``HBa``

	::

		Entier (?)

	BDPVAC? bssw?

``HBH``

	::

		Entier (?), entier (?)

	pvH?

--------------------
``h`` : horloge date
--------------------

``h d``

	::

		Entier (jour), entier (mois), entier (année)

	Date du serveur.

``h e``

	::

		Chaîne (checked ON), chaîne (checked OFF)

	Selon si l'on est en heure d'été (activée) ou en heure
	d'hiver (désactivée).

``h G``

	::

		Entier (fuseau horaire)

	Décalage horaire par rapport à GMT (en nombre d'heures).

``h h``

	::

		Entier (heures), entier (minutes), entier (secondes)

	Heure du serveur.

``h I``

	::

		Chaîne (nom d'hôte)

	Nom d'hôte du serveur NTP. (**DEPRECATED** ?)

``h j``

	::

		Chaîne (jour de la semaine), entier (jour du mois),
		chaîne (mois), entier (année)

	Date du serveur en français.

``h m``

	::

		Entier (statut)

	Statut de la mise à jour de la date/l'heure via NTP :

	- 0 : non mis à jour ;
	- 1 : mise à jour effectuée aujourd'hui ;
	- 2 : configuration invalide.

``h N``

	::

		Chaîne (checked ON), chaîne (checked OFF)

	Mise à l'heure automatique par NTP.

-------------------------------------------
``[iI]`` : tableau intensités des compteurs
-------------------------------------------

``i [1-3]``

	::

		Entier (valeur)

	Intensité instantanée de la phase n°1 à 3 (ou 1 si mono) en A.

``i [4-6]``

	::

		Entier (valeur)

	Intensité maximale de la phase n°1 à 3 (ou 1 si mono) en A.

``i M``, ``I M``

	::

		Entier (?)

	Numéro du mode mono ou triphasé :

	- 0 : pas de détection ;
	- 1 : monophasé ;
	- 3 : triphasé.

``i m``, ``I m``

	::

		Chaîne (valeur)

	Mode mono ou triphasé du compteur 1. ``I`` pour le compteur 2.

``i p``, ``I p``

	::

		Entier (valeur)

	Puissance apparente du compteur 1 en VA. ``I`` pour le compteur 2.

``i T``
	Tableau HTML des I maximales selon le mode mono ou triphasé.

``i t``
	Tableau HTML des I instantanées selon le mode mono ou triphasé.

``Ic[01-06]``, ``Ic[08-11]``

	::

		Entier (?)

	sicon<num>?

``ii[0-3]``, ``Ii[0-3]``
	Intensité instantanée pour la phase n° 1 à 3 (en A).

	::

		Entier (intensité)

	En mode mono (non triphasé), seul ``ii0``/``Ii0`` présente un intérêt.
	``I`` pour le compteur 2.

``im[0-3]``, ``Im[0-3]``
	Intensité maximale pour la phase n° 1 à 3 (en A).

	::

		Entier (?)

	En mode mono (non triphasé), seul ``im0``/``Im0`` présente un intérêt.
	``I`` pour le compteur 2.

-------------------------------------------------
``k`` : programmation des actions (relais, mails)
-------------------------------------------------

``k E``
	?

``k H``
	?

``k h``
	?

``k R``
	?

``k S``
	?

``k s``
	?

``k t``
	?

``k v``
	?

``k w``
	?

------------------------------
``L`` : gestion de l'écran LCD
------------------------------

``L A``
	Statut de l'alarme.

	::

		Chaîne (statut)

	"true" si allumée, "false" sinon.

``L a``
	Durée d'une impulsion pour la backlight rouge lorsque l'alarme est
	activée.

	::

		Flottant (durée)

	En secondes.

``L D``
	Selon s'il y a un afficheur LCD ou pas.

	::

		Chaîne (description)

	S'il n'y a pas d'afficheur LCD : "Pas de LCD".

``L v``
	Durée avant l'extinction automatique de la backlight verte.

	::

		Entier (durée)

	En secondes.

------------------------
``l`` : état des entrées
------------------------

``l A[1-4]``

	::

		Entier (valeur)

	Valeur de l'entrée analogique.

``l a[1-4]``

	::

		Chaîne (nom)

	Nom de l'entrée analogique.

``l E[1-2]``

	::

		Entier (booléen)

	État de l'entrée :

	- 0 : entrée OFF
	- 1 : entrée ON

``l e[1-2]``

	::

		Chaîne (état)

	Chaîne représentant l'état de l'entrée : "ON" ou "OFF".

``l N[1-8]``

	::

		Chaîne (nom)

	Nom du switch virtuel donné par l'utilisateur.

``l n[1-2]``

	::

		Chaîne (nom)

	Nom de l'entrée donné par l'utilisateur.

``l r[1-8]``

	::

		Chaîne (checked ON)

	État du switch virtuel.

``l V[1-8]``

	::

		Entier (booléen)

	État du switch virtuel :

	- 0 : entrée OFF
	- 1 : entrée ON

``l v[1-8]``

	::

		Chaîne (état)

	Chaîne représentant l'état du switch virtuel : "ON" ou "OFF".

-------------------------
``M[tu]`` : protocole M2M
-------------------------

``Mta``

	::

		Chaîne (checked ON)

	Statut du protocole M2M en TCP.

``Mtp``

	::

		Entier (numéro de port)

	Port utilisé pour le protocole M2M en TCP.

``Mua``

	::

		Chaîne (checked ON)

	Statut du protocole M2M en UDP.

``Mup``

	::

		Entier (numéro de port)

	Port utilisé pour le protocole M2M en UDP.

---------------------------
``o`` : contrôle des relais
---------------------------

``o a``

	::

		Chaîne (checked ON), chaîne (checked OFF)

	Afficher les relais sur la page d'accueil de l'interface.

``o E[1-2]``

	::

		Entier (booléen)

	État du relai :

	- 0 : relai OFF
	- 1 : relai ON

``o n[0-1]``

	::

		Chaîne (nom)

	Nom du relai n°0 ou 1 donné par l'utilisateur.

``o R[1-2]``

	::

		Chaîne (état)

	État du relai : "ON" ou "OFF".

``o r[1-2]``

	::

		Chaîne (checked ON)

	État du relai.

``o s``

	::

		Chaîne (checked ON), chaîne (checked OFF)

	État de la sauvegarde de l'état.

``o w``
	?

------------------------------
``P`` : pinces ampèremétriques
------------------------------

``P A[1-4]``

	::

		Flottant (valeur)

	Intensité mesurée de la pince ampèremétrique (lrms, en A).

``P C``, ``P c``

	::

		Chaîne (?)

	coul_PCE puis coul_PCEV?

``P i``

	::

		Chaîne (checked ON)

	Mode graphique : intensité instantanée en ampères (contrairement à
	``P w``).

``P P[1-4]``

	::

		Entier (valeur)

	Puissance apparente mesurée par la pince (lrms x tension, en Wh).

``P R``

	::

		Entier (valeur)

	MGPCE ? (en A)

``P W[1-4]``

	::

		Entier (index)

	Index de début de journée concernant l'énergie consommée mesurée par
	la pince ampèremétrique numéro 1 à 4 (lrms x tension, en VA).

``P i``

	::

		Chaîne (checked ON)

	Mode graphique : puissance en watts (contrairement à ``P i``).

``PCa[1-4]``, ``PCm[1-4]``, ``PCj[1-4]``

	::

		Flottant (valeur)

	Consommation sur la journée, le mois ou l'année mesurée par la
	pince X (en kWh).

``Pea[1-4]``, ``Pem[1-4]``, ``Pej[1-4]``

	::

		Flottant (valeur)

	Coût sur la journée, le mois ou l'année mesurée par la pince X (en euros).

``PG[1-4]``

	::

		Chaîne (couleur), chaîne (?)

	PCEC[1-4] ?

``PHM[1-4]``

	::

		Chaîne (?), flottant (consommation), flottant (prix en euros)

	aff_cons ?

``Pia[1-4]``, ``Pim[1-4]``, ``Pij[1-4]``

	::

		Chaîne (index)

	Index du début de l'année, du mois ou de la journée, pour la pince
	ampèremétrique numéro 1 à 4.

``PJ[1-4]``

	::

		Entier (?)

	MPCE[1-4]? PC[1-4]_gmax? (en A)
	Donne "100" sur ``peval.cgi``, sauf pour la prise secteur (ne correspond
	pas à une variable CGI) où ça devient "280".

``PMa[1-4]``, ``PMm[1-4]``, ``PMj[1-4]``

	::

		Flottant (valeur)

	"Maxi journée" ? (en A)

``Pm [1-4]``

	::

		Chaîne (?)

	"%s en €" ?

``Pn[1-4]``

	::

		Chaîne (nom de la pince)

	Nom de la pince ampèremétrique numéro 1 à 4.

``Pt[1-4]``

	::

		Entier (tension)

	Tension du circuit parcourant la pince ampèremétrique numéro 1 à 4,
	en volts (V).

``PVV``

	::

		Entier (tension)

	Tension secteur (en volts).

-----------------------------
``p`` : compteur d'impulsions
-----------------------------

``p A``, ``p A``

	::

		Entier (valeur)

	Consommation en m³/kWh sur l'année pour le compteur 1.
	``A`` pour le compteur 2.

``p C``, ``p c``

	::

		Chaîne (?)

	coul_PLS puis coul_PLSV?

``p d``, ``p D``

	::

		Entier (nombre)

	Nombre de pulses/L sur le compteur 1. ``D`` pour le compteur 2.

``p j``, ``p J``

	::

		Entier (valeur)

	Consommation en L/kWh sur la journée pour le compteur 1.
	``J`` pour le compteur 2.

``p m``, ``p M``

	::

		Entier (valeur)

	Consommation en m³/kWh sur le mois pour le compteur 1.
	``M`` pour le compteur 2.

``p n``, ``p N``

	::

		Chaîne (nom)

	Nom du compteur 1 (``n``) ou 2 (``N``).

``pCa[1-4]``, ``pCm[1-4]``, ``pCj[1-4]``, ``pCh[1-4]``

	::

		[Flottant (?), ] chaîne (?)

	cons[AVJM][1-4]_val?
	``PLS[1-4]_conso_(day|month)?``

``pd[1-4]``

	::

		Flottant (?)

	debig[1-4]_val ?

``pea[1-4]``, ``pem[1-4]``, ``pej[1-4]``, ``peh[1-4]``

	::

		Flottant (?)

	prix en euros ?

``pG[1-4]``

	::

		Chaîne (couleur), chaîne (?)

	PLSC[1-4] ?

``pg[1-4]``

	::

		Chaîne (couleur), chaîne (?)

	PLSG[1-4] ?

``pHM[1-4]``

	::

		Chaîne (?), flottant (consommation), flottant (prix en euros)

	aff_cons ?

``pIU[1-4]``, ``pIJ[1-4]``, ``pIM[1-4]`` ``pIA[1-4]``

	::

		Flottant (index)

	Index actuel, en début de journée, en début de mois et en début d'année.

``pJ[1-4]``

	::

		Entier (?)

	PLSj[1-4]? PLS[1-4]_gmax? débit max?

``pK[1-4]``

	::

		Chaîne (?), chaîne (?)

	Couleurs ?

``pMg[1-4]``

	::

		Chaîne (?)

	"%s en €" ?

``pMp[1-4]``

	::

		Chaîne (?)

	"%s de l' Année"/"%s de la Veille" ?

``pn[1-4]``

	::

		Chaîne (nom)

	Nom du compteur d'impulsions numéro 1 à 4 (max. 18 caractères).

``po[1-4]``

	::

		Chaîne (?)

	unitcpt[1-4]?

``pp[1-4]``

	::

		Entier (?)

	Nombre d'impulsions aujourd'hui pour le compteur d'impulsions numéro 1 à 4.

``pR[1-4][0-1]``

	::

		Chaîne (selected ON)

	Type d'impulsion pour le compteur d'impulsions numéro 1 à 4.
	Types :

	- choix 0 : ILS (mécanique) ;
	- choix 1 : électronique.

``ps[1-4]``

	::

		Chaîne (?)

	?

``pT[1-4][1-4]``

	::

		Chaîne (checked ON)

	Type de compteur d'impulsions pour le numéro 1 à 4 (premier groupe).
	Types existants :

	- choix 1 : eau froide/chaude ;
	- choix 2 : gaz ;
	- choix 3 : électrique ;
	- choix 4 : fioul.

``pU[1-4][1-4]``

	::

		Chaîne (checked ON)

	Unité de mesure pour le compteur d'impulsions 1 à 4 (premier groupe).
	Unités gérées :

	- choix 1 : litres ;
	- choix 2 : mètres cube ;
	- choix 3 : Wh ;
	- choix 4 : kWh.

``pu[1-4]``

	::

		Entier (nombre)

	Nombre d'impulsions par unité.

------------------------------------------
``R`` : accès au serveur (identifiants, …)
------------------------------------------

``R a``, ``R A``

	::

		Chaîne (nom d'utilisateur)

	Nom d'utilisateur pour se connecter via HTTP (``a``) ou FTP (``A``).

``R b``, ``R B``

	::

		Chaîne ("actif" ou "inactif")

	Connexion activée ("Actif") ou désactivée via HTTP (``b``) ou FTP (``B``).

``R C``

	::

		Chaîne (mot de passe)

	Mot de passe du compte utilisateur.

``R c``

	::

		Chaîne (nom d'utilisateur)

	Nom d'utilisateur du compte utilisateur.

``R g``

	::

		Chaîne (checked ON), chaîne (checked OFF)

	Activer le compte invité (nom d'utilisateur "invite" ou "guest",
	sans mot de passe).

``R p``, ``R P``

	::

		Chaîne (mot de passe)

	Mot de passe pour se connecter via HTTP (``p``) ou via FTP (``P``).

``RM0``

	::

		Chaîne (checked ON), chaîne (checked OFF)

	Autoriser la programmation à l'utilisateur.

``RM1``

	::

		Chaîne (checked ON), chaîne (checked OFF)

	Autoriser le réglage de la date/de l'heure à l'utilisateur.

``RM2``

	::

		Chaîne (checked ON), chaîne (checked OFF)

	Autoriser le réglage des mails à l'utilisateur.

``RM3``

	::

		Chaîne (checked ON), chaîne (checked OFF)

	Autoriser le réglage des index à l'utilisateur.

``RM4``

	::

		Chaîne (checked ON), chaîne (checked OFF)

	Autoriser le réglage des coûts à l'utilisateur.

``RM5``

	::

		Chaîne (checked ON), chaîne (checked OFF)

	Autoriser le contrôle des switch et relais à l'utilisateur.

-------------------------
``r`` : paramètres réseau
-------------------------

``r a``

	::

		6 entiers (octets de l'adresse)

	Adresse MAC du serveur.

``r D``

	::

		Chaîne (checked OFF)

	Option DHCP.

``r d``

	::

		Chaîne (checked ON)

	Option DHCP (bis).

``r g``

	::

		4 entiers (octets de l'adresse), chaîne (DHCP disabled ON)

	Passerelle par défaut.

``r h``

	::

		Entier (numéro de port)

	Port du service HTTP du serveur WES.

``r i``

	::

		4 entiers (octets de l'adresse), chaîne (DHCP disabled ON)

	Adresse IPv4 actuelle.

``r m``

	::

		4 entiers (octets du masque), chaîne (DHCP disabled ON)

	Masque de sous-réseau.

``r n``

	::

		Chaîne (nom)

	Nom d'hôte du serveur WES.

``r o``
	Tableau HTML avec un ``<tr>`` par ligne, et dans l'ordre :

	- le numéro de socket (1 à 20) ;
	- l'état de la socket ;
	- l'IP distante ;
	- le numéro de port distant ;
	- le numéro de port local ;
	- le timer (120 ?).

``r p``

	::

		4 entiers (octets de l'adresse), chaîne (DHCP disabled ON)

	Serveur DNS primaire.

``r s``

	::

		4 entiers (octets de l'adresse), chaîne (DHCP disabled ON)

	Serveur DNS secondaire.

------------------------------
``T`` : compteurs via TELEINFO
------------------------------

``T c``, ``T C``

	::

		Chaîne (couleurs)

	Couleurs en CSS pour la légende des graphes concernant le compteur 1.
	``C`` pour le compteur 2.

``T n``, ``T N``

	::

		Chaîne (nom)

	Nom de la période tarifaire en cours sur le compteur 1.
	``N`` pour le compteur 2.

``T p``, ``T P``

	::

		Chaîne (période)

	Période tarifaire en cours sur le compteur 1.
	``P`` pour le compteur 2.

``T R``

	::

		Entier (?)

	PMAX ? MGTIC ? (en VA)

``T t``, ``T T``

	Tableau des index suivant abonnement sur le compteur 1.
	``T`` pour le compteur 2.

``T x``, ``T X``

	Tableau des index suivant abonnement avec mise à jour automatique chaque
	seconde par fichiers CGX pour le compteur 1.
	``X`` pour le compteur 2.

``Tb1``, ``TB1``
	Index interprété pour l'abonnement BASE.

	::

		Chaîne (index)

	``B`` pour le compteur 2.

``Tc[1-2]``, ``TC[1-2]``
	Index interprétés pour l'abonnement HCHP (Heures Creuses, Heures Pleines).

	::

		Chaîne (index)

	L'index n°1 est pour les heures creuses, l'index n°2 est pour les heures
	pleines. ``C`` pour le compteur 2.

``Td[1-2]``

	::

		Chaîne (?)

	DEMAIN?

``Te[1-2]``

	::

		Entier (?)

	PEJP? 1/2 pour compteur ?

``Tg[12]?[0-5]``, ``TG[0-5]``

	::

		Chaîne (couleur de fond), chaîne (?)

	``TICCxx`` ? ``COULTxx`` ? (max 20 car. ?)

``Ti[1-6]``, ``TI[1-6]``
	Index quasi non interprétés des compteurs.

	::

		Chaîne (index)

	Si disponible, le compteur est donné sur neuf chiffres décimaux.
	Autrement, il s'agit de la chaîne « Pas Dispo ».

``Tj[1-2]``, ``TJ[1-2]``
	Index interprétés pour l'abonnement d'Effacement de Jour de Pointe.

	::

		Chaîne (index)

	L'index n°1 est pour les heures normales, l'index n°2 est pour les
	heures de pointe mobile. ``J`` pour le compteur 2.

``Tr[1-6]``, ``TR[1-6]``
	Index interprétés pour l'abonnement TEMPO.

	::

		Chaîne (index)

	Les index sont les suivants :

	- n°1 : jours bleus, heures creuses ;
	- n°2 : jours bleus, heures pleines ;
	- n°3 : jours blancs, heures creuses ;
	- n°4 : jours blancs, heures pleines ;
	- n°5 : jours rouges, heures creuses ;
	- n°6 : jours rouges, heures pleines.

	``R`` pour le compteur 2.

-----------------
``V`` : variables
-----------------

``Vn[1-8]``

	::

		Chaîne (nom)

	Nom de la variable n° 1 à 8.

``Vv[1-8]``

	::

		Flottant (valeur)

	Valeur de la variable n° 1 à 8.

--------------------------------
``v`` : informations de versions
--------------------------------

``v h``

	::

		Chaîne (version)

	Version du matériel, e.g. "F417 V2".

``v n``

	::

		Chaîne (disabled ON)

	?

``v v``

	::

		Chaîne (version)

	Version du micrologiciel, e.g. "V0.7G3".

-----------------
``W`` : bus 1WIRE
-----------------

``W A[1-3]``, ``W M[1-3]``, ``W J[0-2]``

	::

		Flottant (valeur)

	Température maximum (°C) sur l'année, le mois (sonde 1) ou le
	jour (sonde n+1).

``W a[1-3]``, ``W m[1-3]``, ``W j[0-2]``

	::

		Flottant (valeur)

	Température minimum (°C) sur l'année, le mois (sonde 1) ou le
	jour (sonde n+1).

``W H``

	::

		Entier (nombre)

	Nombre de sondes d'humidité.

``W Q``

	::

		Entier (nombre)

	Nombre de sondes de température.

``W[0-2]C[0-9]``

	::

		Flottant (?), chaîne (?)

	tmp1_val? WSV[00-29]?

``W[0-2]D[0-9]``

	::

		Chaîne (?)

	? (image?)

``W[0-2]G[0-9]``

	::

		Entier (?)

	tmp10_cal? WG[0-2][0-9]?
	Numéro de graphe (1 à 4) ?

``W[0-2]A[0-9]``, ``W[0-2]M[0-9]``, ``W[0-2]J[0-9]``

	::

		Flottant (valeur)

	Valeur maximale de la température sur l'année, le mois ou
	la journée (en °C).

``W[0-2]a[0-9]``, ``W[0-2]m[0-9]``, ``W[0-2]j[0-9]``

	::

		Flottant (valeur)

	Valeur minimale de la température sur l'année, le mois ou
	la journée (en °C).

``W[0-2]N[0-9]``

	::

		Chaîne (nom)

	Nom de la sonde, donné par l'utilisateur.

``W[0-2]S[0-9]``

	::

		8 entiers (octets de l'identifiant)

	Identifiant de la sonde, à partir de 0 (e.g. ``W1S8`` -> 19ème sonde).

``W[0-2]T[0-9]``

	::

		Flottant (valeur), chaîne (?)

	Valeur de la température de la sonde (en °C).

``W[0-2]t[0-9]``

	::

		Chaîne (?)

	?

``WPD[0-5]``

	::

		Chaîne (?)

	Image ?

``WPS[0-5]``

	::

		8 entiers (octets de l'identifiant)

	?

``W[0-2]t[0-9]``

	::

		Chaîne (?)

	?

``WTA``, ``WTM``, ``WTJ``

	Tableau HTML à deux entrées où la première entrée est le nom de la
	sonde/catégorie et la deuxième est la température maximum calculée
	pour un jour, un mois ou une année.

	Exemple (indenté) où la seule sonde de température s'appelle ``temp``,
	et où sont appelés dans l'ordre ``WTJ``, ``WTM`` et ``WTA`` :

	::

		<tr>
			<td>temp MAXI Jour</td>
			<td align=right>21.7 &ordm;C</td>
		</tr>
		<tr>
			<td>temp MAXI Mois</td>
			<td align=right>23.0 &ordm;C</td>
		</tr>
		<tr>
			<td>temp MAXI Ann&eacute;e</td>
			<td align=right>59.5 &ordm;C</td>
		</tr>

``WTa``, ``WTm``, ``WTj``

	Tableau HTML à deux entrées, où la première entrée est le nom de la
	sonde/catégorie et la deuxième est la température minimum calculée
	pour un jour, un mois ou une année.

	Exemple (indenté) où la seule sonde de température s'appelle ``temp``,
	et où sont appelés dans l'ordre ``WTj``, ``WTm`` et ``WTa`` :

	::

		<tr>
			<td>temp mini Jour</td>
			<td align=right>19.6 &ordm;C</td>
		</tr>
		<tr>
			<td>temp mini Mois</td>
			<td align=right>15.9 &ordm;C</td>
		</tr>
		<tr>
			<td>temp mini Ann&eacute;e</td>
			<td align=right>14.0 &ordm;C</td>
		</tr>

``WTs``

	Tableau XML pour les sondes 1Wire.

	Exemple (indenté) :

	::

		<java>
			<var>WS0</var>
			<value><![CDATA[<td></td>
				<td align=right id="WSC0">21.6 &ordm;C</td>
				<td align=center>19.6 / 21.7</td>
				<td align=center>15.9 / 23.0</td>
				<td align=center>14.0 / 59.5</td>
			]]></value>
		</java>

``WTS``

	Tableau HTML à deux colonnes où la première est le nom de la sonde
	et la seconde représente la température actuelle avec l'unité de
	température (°C).

``WTv``
	?

``WR[0-9][1-8]``

	::

		Chaîne (nom)

	Nom de relai n° 1 à 8 de la carte relai n° 1 à 10.

``WRD[0-9]``

	::

		Chaîne (nom d'image)

	Statut de la détection de la carte relai n° 1 à 10, sous la forme
	d'image : ``led_OFF.gif`` si désactivée.

``WRd``
	?

``WRN[0-9]``

	::

		Chaîne (nom)

	Nom de la carte relai.

``WRn``

	::

		Entier (nombre)

	Nombre de cartes relais détectées sur le bus 1WIRE.

``WRo``
	?

``WRS[0-9]``

	::

		8 entiers (identifiant)

	Identifiant de la carte relai n° 1 à 10.

``WRT[0-9]``

	::

		Chaîne (checked 3), chaîne (checked 8)

	Nombre de relais sur la carte.


.. _pf: https://en.wikipedia.org/wiki/Printf_format_string
.. _BDPV: https://www.bdpv.fr/
