:title: Introduction aux serveurs WES
:author: Thomas Touhey

Les serveurs WES, pour « Web energie superviseur », sont des serveurs de
surveillance et de gestion énergétique produits par Cartelectronic.
Ils permettent de :

- récupérer les données Teleinfo (TIC compteur ERDF) depuis un compteur
  électrique type Linky ;
- récupérer ce que renvoient jusqu'à quatre pinces ampèremétriques (avec
  tension secteur par transformateur ?) ;
- récupérer ce que renvoient des détecteurs d'impulsions ;
- interagir avec un réseau 1WIRE (?) ;
- deux relais (?) ;
- I1 à I4 entrées tout ou rien ou analogiques (?) ;
- capteurs (?).

Cette petite documentation comporte les points du serveur WES que j'ai
utilisés dans ce projet, notamment la façon d'interagir avec lui.

Le serveur WES se connecte en Ethernet et communique avec le reste du
réseau par IP (sa configuration peut être dynamique via DHCP, ou fixe).
Pour interagir avec lui, différentes méthodes sont possibles et doivent
être utilisées de façon complémentaire pour interagir avec la plus grande
gamme possible d'éléments :

- récupération et remplacement des fichiers de configuration généraux,
  présents dans le dossier ``CFG/`` via FTP (documenté dans
  `CONFIG.rst <CONFIG.rst>`_) ;
- récupération et remplacement des triggers (« programmation ») dans
  le dossier ``CFG/`` (documenté dans `TRIG.rst <TRIG.rst>`_) ;
- utilisation des triggers pour faire envoyer au serveur WES des mails et
  des requêtes HTTP contenant des variables (documenté dans
  `REQS.rst <REQS.rst>`_) ;
- exécution de scripts CGI (upload et suppression via FTP, exécution via
  HTTP) pour récupérer la valeur de certaines variables (documenté
  dans `CGI.rst <CGI.rst>`_) ;
- exécution de commandes via le protocole machine à machine (documenté
  dans `M2M.rst <M2M.rst>`_) ;
- récupération de fichiers XML avec l'extension ``CGX`` (documenté dans
  `CGX.rst <CGX.rst>`_) ;
- récupération de fichiers CSV contenant l'historique des données dans
  les dossiers correspondant aux types de données (documenté dans
  `DATA.rst <DATA.rst>`_) ;
- exécution de requêtes HTTP avec arguments pour définir des variables
  et autres éléments de configuration (documenté dans `HTTP.rst <HTTP.rst>`_).

Découvrez-en plus sur chacun des éléments par les documents qui y sont liés.
