:title: Récupération et décodage de fichiers CGX
:author: Thomas Touhey

Les fichiers CGX sont codés avec le langage de scripting spécifique du serveur
WES (documenté dans `CGI.md`), cependant il est important de noter que puisque
les deux ne sont pas standardisés, l'un peut évoluer sans l'autre, du moment
que l'autre est compensé (ce qui ne se voit pas forcément côté utilisateur).
Implémenter la lecture des fichiers CGX ajoute donc de la redondance et rend
tout logiciel implémentant les deux plus robuste face à l'avenir.

Les fichiers CGX sont des fichiers XML.

**TODO: format?**
