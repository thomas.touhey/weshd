:title: Configuration du serveur WES
:author: Thomas Touhey

Le serveur WES stocke sa configuration dans le dossier ``CFG/`` accessible
via FTP. Les fichiers de configuration ont l'extension ``.CFG``. À noter que
toute modification manuelle de ces fichiers doit être suivie par un reboot.

**TODO**: ou simplement un rechargement de la configuration via requêtes
HTTP par exemple ?

Dans ce document, ``<CR>`` représente le retour chariot (``\r``, code ASCII 13)
et ``<LF>`` représente le retour à la ligne (``\n``, code ASCII 10).

Les fichiers de configuration partent d'un format simple où chaque option,
constituée d'une clé et d'une valeur (les « sections » utilisées dans ce
projet correspondent en réalité aux divers fichiers, comme ``CFG/M2M.CFG``
correspondant à la section ``M2M``), est représentée sous la forme suivante :

.. code::

	<clé>=<valeur><CR><LF>

Les options sont mises à la suite (``<CR><LF>`` représentant un changement de
ligne, on peut parler d'« une option par ligne »). Seulement, certaines lignes
ne sont pas des correspondances (clé, valeur) et ne contiennent que des
données à lire indépendamment, comme le fichier de configuration ``S1WIRE.CFG``
où les données sont contenues sous la forme d'un CSV avec tirets et nombres.

Dans ce document, je vais essayer de lister les clés connues.

------------------------------------------------------------
``M2M.CFG`` : configuration de l'interface machine à machine
------------------------------------------------------------

``UDPA``

	Activer l'interface et la faire écouter sur un port UDP.

``UDPP``

	Port UDP sur lequel l'interface écoute.

``TCPA``

	Activer l'interfce et la faire écouter sur un port TCP.

``TCPP``

	Port TCP sur lequel l'interface écoute.

-------------------------------------
``RESEAU.CFG`` : configuration réseau
-------------------------------------

``NAME``

	Nom d'hôte du serveur WES.

``DHCP``

	Activer (1) ou désactiver (0) le fait de récupérer sa configuration
	réseau IPv4 via DHCP. Activer cette option fait que le serveur ignore
	la configuration statique définie via les options ``ADIP``, ``MASQ``,
	``GATE``, ``1DNS`` et ``2DNS`` présentées ci-dessous.

``ADIP``

	Adresse IPv4 à utiliser dans une configuration fixe.

``MASQ``

	Masque de sous-réseau à utiliser dans une configuration fixe.

``GATE``

	Adresse IPv4 de la passerelle par défaut à utiliser dans une
	configuration fixe.

``1DNS``

	Adresse IPv4 du serveur DNS primaire.

``2DNS``

	Adresse IPv4 du serveur DNS secondaire.

``HTPT``

	Port à utiliser pour le serveur HTTP présentant l'interface graphique
	de configuration.

------------------------------------------
``LCD.CFG`` : configuration de l'afficheur
------------------------------------------

``BACKL``

	Backlight (?).

``ALARM``

	Seuil d'alarme (?).
